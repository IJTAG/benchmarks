/*
* Author: Aleksa Damljanovic, Politecnico di Torino
*
* Uses modules from files: 
* - Instruments.icl
* - NetworkStructs.icl
*/

Module N32D6 {
	Attribute lic = 'h a9c3f5b9;
	ScanInPort SI;
	CaptureEnPort CE;
	ShiftEnPort SE;
	UpdateEnPort UE;
	SelectPort SEL;
	ResetPort RST;
	TCKPort TCK;
	ScanOutPort SO {
		Source SIB1.SO;
	}
	LogicSignal R31_Sel {
		SEL;
	}
	Instance R31 Of WrappedInstr  {
		InputPort SI = SI;
		InputPort SEL = R31_Sel;
		Parameter Size = 1667;
	}
	LogicSignal R30A_Sel {
		SEL & ~SCB30scb.toSEL;
	}
	Instance R30A Of WrappedInstr  {
		InputPort SI = R31;
		InputPort SEL = R30A_Sel;
		Parameter Size = 3232;
	}
	LogicSignal R30B_Sel {
		SEL & SCB30scb.toSEL;
	}
	Instance R30B Of WrappedInstr  {
		InputPort SI = R31;
		InputPort SEL = R30B_Sel;
		Parameter Size = 2520;
	}
	ScanMux SCB30 SelectedBy SCB30scb.toSEL {
		1'b0 : R30A.SO;
		1'b1 : R30B.SO;
	}
	Instance SCB30scb Of SCB {
		InputPort SI = SCB30;
		InputPort SEL = SEL;
	}
	LogicSignal R29A_Sel {
		SEL & ~SCB29scb.toSEL;
	}
	Instance R29A Of WrappedInstr  {
		InputPort SI = SCB30scb.SO;
		InputPort SEL = R29A_Sel;
		Parameter Size = 480;
	}
	LogicSignal R29B_Sel {
		SEL & SCB29scb.toSEL;
	}
	Instance R29B Of WrappedInstr  {
		InputPort SI = SCB30scb.SO;
		InputPort SEL = R29B_Sel;
		Parameter Size = 1882;
	}
	ScanMux SCB29 SelectedBy SCB29scb.toSEL {
		1'b0 : R29A.SO;
		1'b1 : R29B.SO;
	}
	Instance SCB29scb Of SCB {
		InputPort SI = SCB29;
		InputPort SEL = SEL;
	}
	LogicSignal R28_Sel {
		SEL;
	}
	Instance R28 Of WrappedInstr  {
		InputPort SI = SCB29scb.SO;
		InputPort SEL = R28_Sel;
		Parameter Size = 3307;
	}
	LogicSignal R27t_Sel {
		SIB27.toSEL;
	}
	Instance R27t Of WrappedInstr  {
		InputPort SI = SIB27.toSI;
		InputPort SEL = R27t_Sel;
		Parameter Size = 3933;
	}
	LogicSignal R26t_Sel {
		SIB26.toSEL;
	}
	Instance R26t Of WrappedInstr  {
		InputPort SI = SIB26.toSI;
		InputPort SEL = R26t_Sel;
		Parameter Size = 270;
	}
	LogicSignal R25A_Sel {
		SIB26.toSEL & ~SCB25scb.toSEL;
	}
	Instance R25A Of WrappedInstr  {
		InputPort SI = R26t;
		InputPort SEL = R25A_Sel;
		Parameter Size = 1097;
	}
	LogicSignal R25B_Sel {
		SIB26.toSEL & SCB25scb.toSEL;
	}
	Instance R25B Of WrappedInstr  {
		InputPort SI = R26t;
		InputPort SEL = R25B_Sel;
		Parameter Size = 3454;
	}
	ScanMux SCB25 SelectedBy SCB25scb.toSEL {
		1'b0 : R25A.SO;
		1'b1 : R25B.SO;
	}
	Instance SCB25scb Of SCB {
		InputPort SI = SCB25;
		InputPort SEL = SIB26.toSEL;
	}
	LogicSignal R24t_Sel {
		SIB24.toSEL;
	}
	Instance R24t Of WrappedInstr  {
		InputPort SI = SIB24.toSI;
		InputPort SEL = R24t_Sel;
		Parameter Size = 1412;
	}
	LogicSignal R23_Sel {
		SIB24.toSEL;
	}
	Instance R23 Of WrappedInstr  {
		InputPort SI = R24t;
		InputPort SEL = R23_Sel;
		Parameter Size = 4139;
	}
	LogicSignal R22A_Sel {
		SIB24.toSEL & ~SCB22scb.toSEL;
	}
	Instance R22A Of WrappedInstr  {
		InputPort SI = R23;
		InputPort SEL = R22A_Sel;
		Parameter Size = 2012;
	}
	LogicSignal R22B_Sel {
		SIB24.toSEL & SCB22scb.toSEL;
	}
	Instance R22B Of WrappedInstr  {
		InputPort SI = R23;
		InputPort SEL = R22B_Sel;
		Parameter Size = 2403;
	}
	ScanMux SCB22 SelectedBy SCB22scb.toSEL {
		1'b0 : R22A.SO;
		1'b1 : R22B.SO;
	}
	Instance SCB22scb Of SCB {
		InputPort SI = SCB22;
		InputPort SEL = SIB24.toSEL;
	}
	Instance SIB24 Of SIB_mux_pre {
		InputPort SI = SCB25scb.SO;
		InputPort SEL = SIB26.toSEL;
		InputPort fromSO = SCB22scb.SO;
	}
	LogicSignal R21A_Sel {
		SIB26.toSEL & ~SCB21scb.toSEL;
	}
	Instance R21A Of WrappedInstr  {
		InputPort SI = SIB24.SO;
		InputPort SEL = R21A_Sel;
		Parameter Size = 2035;
	}
	LogicSignal R21B_Sel {
		SIB26.toSEL & SCB21scb.toSEL;
	}
	Instance R21B Of WrappedInstr  {
		InputPort SI = SIB24.SO;
		InputPort SEL = R21B_Sel;
		Parameter Size = 800;
	}
	ScanMux SCB21 SelectedBy SCB21scb.toSEL {
		1'b0 : R21A.SO;
		1'b1 : R21B.SO;
	}
	Instance SCB21scb Of SCB {
		InputPort SI = SCB21;
		InputPort SEL = SIB26.toSEL;
	}
	Instance SIB26 Of SIB_mux_pre {
		InputPort SI = R27t.SO;
		InputPort SEL = SIB27.toSEL;
		InputPort fromSO = SCB21scb.SO;
	}
	LogicSignal R20_Sel {
		SIB27.toSEL;
	}
	Instance R20 Of WrappedInstr  {
		InputPort SI = SIB26.SO;
		InputPort SEL = R20_Sel;
		Parameter Size = 1785;
	}
	Instance SIB27 Of SIB_mux_pre {
		InputPort SI = R28.SO;
		InputPort SEL = SEL;
		InputPort fromSO = R20.SO;
	}
	LogicSignal R19A_Sel {
		SEL & ~SCB19scb.toSEL;
	}
	Instance R19A Of WrappedInstr  {
		InputPort SI = SIB27.SO;
		InputPort SEL = R19A_Sel;
		Parameter Size = 3601;
	}
	LogicSignal R19B_Sel {
		SEL & SCB19scb.toSEL;
	}
	Instance R19B Of WrappedInstr  {
		InputPort SI = SIB27.SO;
		InputPort SEL = R19B_Sel;
		Parameter Size = 506;
	}
	ScanMux SCB19 SelectedBy SCB19scb.toSEL {
		1'b0 : R19A.SO;
		1'b1 : R19B.SO;
	}
	Instance SCB19scb Of SCB {
		InputPort SI = SCB19;
		InputPort SEL = SEL;
	}
	LogicSignal R18t_Sel {
		SIB18.toSEL;
	}
	Instance R18t Of WrappedInstr  {
		InputPort SI = SIB18.toSI;
		InputPort SEL = R18t_Sel;
		Parameter Size = 2671;
	}
	LogicSignal R17t_Sel {
		SIB17.toSEL;
	}
	Instance R17t Of WrappedInstr  {
		InputPort SI = SIB17.toSI;
		InputPort SEL = R17t_Sel;
		Parameter Size = 2227;
	}
	LogicSignal R16t_Sel {
		SIB16.toSEL;
	}
	Instance R16t Of WrappedInstr  {
		InputPort SI = SIB16.toSI;
		InputPort SEL = R16t_Sel;
		Parameter Size = 308;
	}
	LogicSignal R15A_Sel {
		SIB16.toSEL & ~SCB15scb.toSEL;
	}
	Instance R15A Of WrappedInstr  {
		InputPort SI = R16t;
		InputPort SEL = R15A_Sel;
		Parameter Size = 1293;
	}
	LogicSignal R15B_Sel {
		SIB16.toSEL & SCB15scb.toSEL;
	}
	Instance R15B Of WrappedInstr  {
		InputPort SI = R16t;
		InputPort SEL = R15B_Sel;
		Parameter Size = 4202;
	}
	ScanMux SCB15 SelectedBy SCB15scb.toSEL {
		1'b0 : R15A.SO;
		1'b1 : R15B.SO;
	}
	Instance SCB15scb Of SCB {
		InputPort SI = SCB15;
		InputPort SEL = SIB16.toSEL;
	}
	LogicSignal R14t_Sel {
		SIB14.toSEL;
	}
	Instance R14t Of WrappedInstr  {
		InputPort SI = SIB14.toSI;
		InputPort SEL = R14t_Sel;
		Parameter Size = 2442;
	}
	Instance SIB14 Of SIB_mux_pre {
		InputPort SI = SCB15scb.SO;
		InputPort SEL = SIB16.toSEL;
		InputPort fromSO = R14t.SO;
	}
	LogicSignal R13A_Sel {
		SIB16.toSEL & ~SCB13scb.toSEL;
	}
	Instance R13A Of WrappedInstr  {
		InputPort SI = SIB14.SO;
		InputPort SEL = R13A_Sel;
		Parameter Size = 4210;
	}
	LogicSignal R13B_Sel {
		SIB16.toSEL & SCB13scb.toSEL;
	}
	Instance R13B Of WrappedInstr  {
		InputPort SI = SIB14.SO;
		InputPort SEL = R13B_Sel;
		Parameter Size = 2967;
	}
	ScanMux SCB13 SelectedBy SCB13scb.toSEL {
		1'b0 : R13A.SO;
		1'b1 : R13B.SO;
	}
	Instance SCB13scb Of SCB {
		InputPort SI = SCB13;
		InputPort SEL = SIB16.toSEL;
	}
	LogicSignal R12_Sel {
		SIB16.toSEL;
	}
	Instance R12 Of WrappedInstr  {
		InputPort SI = SCB13scb.SO;
		InputPort SEL = R12_Sel;
		Parameter Size = 4125;
	}
	Instance SIB16 Of SIB_mux_pre {
		InputPort SI = R17t.SO;
		InputPort SEL = SIB17.toSEL;
		InputPort fromSO = R12.SO;
	}
	LogicSignal R11_Sel {
		SIB17.toSEL;
	}
	Instance R11 Of WrappedInstr  {
		InputPort SI = SIB16.SO;
		InputPort SEL = R11_Sel;
		Parameter Size = 1122;
	}
	Instance SIB17 Of SIB_mux_pre {
		InputPort SI = R18t.SO;
		InputPort SEL = SIB18.toSEL;
		InputPort fromSO = R11.SO;
	}
	Instance SIB18 Of SIB_mux_pre {
		InputPort SI = SCB19scb.SO;
		InputPort SEL = SEL;
		InputPort fromSO = SIB17.SO;
	}
	LogicSignal R10_Sel {
		SEL;
	}
	Instance R10 Of WrappedInstr  {
		InputPort SI = SIB18.SO;
		InputPort SEL = R10_Sel;
		Parameter Size = 3473;
	}
	LogicSignal R9A_Sel {
		SEL & ~SCB9scb.toSEL;
	}
	Instance R9A Of WrappedInstr  {
		InputPort SI = R10;
		InputPort SEL = R9A_Sel;
		Parameter Size = 406;
	}
	LogicSignal R9B_Sel {
		SEL & SCB9scb.toSEL;
	}
	Instance R9B Of WrappedInstr  {
		InputPort SI = R10;
		InputPort SEL = R9B_Sel;
		Parameter Size = 343;
	}
	ScanMux SCB9 SelectedBy SCB9scb.toSEL {
		1'b0 : R9A.SO;
		1'b1 : R9B.SO;
	}
	Instance SCB9scb Of SCB {
		InputPort SI = SCB9;
		InputPort SEL = SEL;
	}
	LogicSignal R8t_Sel {
		SIB8.toSEL;
	}
	Instance R8t Of WrappedInstr  {
		InputPort SI = SIB8.toSI;
		InputPort SEL = R8t_Sel;
		Parameter Size = 1153;
	}
	LogicSignal R7_Sel {
		SIB8.toSEL;
	}
	Instance R7 Of WrappedInstr  {
		InputPort SI = R8t;
		InputPort SEL = R7_Sel;
		Parameter Size = 1297;
	}
	Instance SIB8 Of SIB_mux_pre {
		InputPort SI = SCB9scb.SO;
		InputPort SEL = SEL;
		InputPort fromSO = R7.SO;
	}
	LogicSignal R6t_Sel {
		SIB6.toSEL;
	}
	Instance R6t Of WrappedInstr  {
		InputPort SI = SIB6.toSI;
		InputPort SEL = R6t_Sel;
		Parameter Size = 2195;
	}
	Instance SIB6 Of SIB_mux_pre {
		InputPort SI = SIB8.SO;
		InputPort SEL = SEL;
		InputPort fromSO = R6t.SO;
	}
	LogicSignal R5t_Sel {
		SIB5.toSEL;
	}
	Instance R5t Of WrappedInstr  {
		InputPort SI = SIB5.toSI;
		InputPort SEL = R5t_Sel;
		Parameter Size = 2632;
	}
	LogicSignal R4_Sel {
		SIB5.toSEL;
	}
	Instance R4 Of WrappedInstr  {
		InputPort SI = R5t;
		InputPort SEL = R4_Sel;
		Parameter Size = 3314;
	}
	Instance SIB5 Of SIB_mux_pre {
		InputPort SI = SIB6.SO;
		InputPort SEL = SEL;
		InputPort fromSO = R4.SO;
	}
	LogicSignal R3_Sel {
		SEL;
	}
	Instance R3 Of WrappedInstr  {
		InputPort SI = SIB5.SO;
		InputPort SEL = R3_Sel;
		Parameter Size = 2692;
	}
	LogicSignal R2_Sel {
		SEL;
	}
	Instance R2 Of WrappedInstr  {
		InputPort SI = R3;
		InputPort SEL = R2_Sel;
		Parameter Size = 139;
	}
	LogicSignal R1t_Sel {
		SIB1.toSEL;
	}
	Instance R1t Of WrappedInstr  {
		InputPort SI = SIB1.toSI;
		InputPort SEL = R1t_Sel;
		Parameter Size = 1028;
	}
	LogicSignal R0A_Sel {
		SIB1.toSEL & ~SCB0scb.toSEL;
	}
	Instance R0A Of WrappedInstr  {
		InputPort SI = R1t;
		InputPort SEL = R0A_Sel;
		Parameter Size = 101;
	}
	LogicSignal R0B_Sel {
		SIB1.toSEL & SCB0scb.toSEL;
	}
	Instance R0B Of WrappedInstr  {
		InputPort SI = R1t;
		InputPort SEL = R0B_Sel;
		Parameter Size = 3773;
	}
	ScanMux SCB0 SelectedBy SCB0scb.toSEL {
		1'b0 : R0A.SO;
		1'b1 : R0B.SO;
	}
	Instance SCB0scb Of SCB {
		InputPort SI = SCB0;
		InputPort SEL = SIB1.toSEL;
	}
	LogicSignal Rs1t_Sel {
		SIBs1.toSEL;
	}
	Instance Rs1t Of WrappedInstr  {
		InputPort SI = SIBs1.toSI;
		InputPort SEL = Rs1t_Sel;
		Parameter Size = 3091;
	}
	Instance SIBs1 Of SIB_mux_pre {
		InputPort SI = SCB0scb.SO;
		InputPort SEL = SIB1.toSEL;
		InputPort fromSO = Rs1t.SO;
	}
	LogicSignal Rs2t_Sel {
		SIBs2.toSEL;
	}
	Instance Rs2t Of WrappedInstr  {
		InputPort SI = SIBs2.toSI;
		InputPort SEL = Rs2t_Sel;
		Parameter Size = 4396;
	}
	Instance SIBs2 Of SIB_mux_pre {
		InputPort SI = SIBs1.SO;
		InputPort SEL = SIB1.toSEL;
		InputPort fromSO = Rs2t.SO;
	}
	Instance SIB1 Of SIB_mux_pre {
		InputPort SI = R2.SO;
		InputPort SEL = SEL;
		InputPort fromSO = SIBs2.SO;
	}

}