/*
* Author: Aleksa Damljanovic, Politecnico di Torino
*
* Uses modules from files: 
* - Instruments.icl
* - NetworkStructs.icl
*/

Module N73D14 {
	Attribute lic = 'h dc4ac4c1;
	ScanInPort SI;
	CaptureEnPort CE;
	ShiftEnPort SE;
	UpdateEnPort UE;
	SelectPort SEL;
	ResetPort RST;
	TCKPort TCK;
	ScanOutPort SO {
		Source SIB0.SO;
	}
	LogicSignal R72t_Sel {
		SIB72.toSEL;
	}
	Instance R72t Of WrappedInstr  {
		InputPort SI = SIB72.toSI;
		InputPort SEL = R72t_Sel;
		Parameter Size = 1824;
	}
	LogicSignal R71_Sel {
		SIB72.toSEL;
	}
	Instance R71 Of WrappedInstr  {
		InputPort SI = R72t;
		InputPort SEL = R71_Sel;
		Parameter Size = 728;
	}
	LogicSignal R70_Sel {
		SIB72.toSEL;
	}
	Instance R70 Of WrappedInstr  {
		InputPort SI = R71;
		InputPort SEL = R70_Sel;
		Parameter Size = 2629;
	}
	LogicSignal R69t_Sel {
		SIB69.toSEL;
	}
	Instance R69t Of WrappedInstr  {
		InputPort SI = SIB69.toSI;
		InputPort SEL = R69t_Sel;
		Parameter Size = 3458;
	}
	LogicSignal R68t_Sel {
		SIB68.toSEL;
	}
	Instance R68t Of WrappedInstr  {
		InputPort SI = SIB68.toSI;
		InputPort SEL = R68t_Sel;
		Parameter Size = 1987;
	}
	LogicSignal R67_Sel {
		SIB68.toSEL;
	}
	Instance R67 Of WrappedInstr  {
		InputPort SI = R68t;
		InputPort SEL = R67_Sel;
		Parameter Size = 3780;
	}
	Instance SIB68 Of SIB_mux_pre {
		InputPort SI = R69t.SO;
		InputPort SEL = SIB69.toSEL;
		InputPort fromSO = R67.SO;
	}
	LogicSignal R66t_Sel {
		SIB66.toSEL;
	}
	Instance R66t Of WrappedInstr  {
		InputPort SI = SIB66.toSI;
		InputPort SEL = R66t_Sel;
		Parameter Size = 4195;
	}
	Instance SIB66 Of SIB_mux_pre {
		InputPort SI = SIB68.SO;
		InputPort SEL = SIB69.toSEL;
		InputPort fromSO = R66t.SO;
	}
	Instance SIB69 Of SIB_mux_pre {
		InputPort SI = R70.SO;
		InputPort SEL = SIB72.toSEL;
		InputPort fromSO = SIB66.SO;
	}
	Instance SIB72 Of SIB_mux_pre {
		InputPort SI = SI;
		InputPort SEL = SEL;
		InputPort fromSO = SIB69.SO;
	}
	LogicSignal R65A_Sel {
		SEL & ~SCB65scb.toSEL;
	}
	Instance R65A Of WrappedInstr  {
		InputPort SI = SIB72.SO;
		InputPort SEL = R65A_Sel;
		Parameter Size = 3358;
	}
	LogicSignal R65B_Sel {
		SEL & SCB65scb.toSEL;
	}
	Instance R65B Of WrappedInstr  {
		InputPort SI = SIB72.SO;
		InputPort SEL = R65B_Sel;
		Parameter Size = 3567;
	}
	ScanMux SCB65 SelectedBy SCB65scb.toSEL {
		1'b0 : R65A.SO;
		1'b1 : R65B.SO;
	}
	Instance SCB65scb Of SCB {
		InputPort SI = SCB65;
		InputPort SEL = SEL;
	}
	LogicSignal R64_Sel {
		SEL;
	}
	Instance R64 Of WrappedInstr  {
		InputPort SI = SCB65scb.SO;
		InputPort SEL = R64_Sel;
		Parameter Size = 2761;
	}
	LogicSignal R63_Sel {
		SEL;
	}
	Instance R63 Of WrappedInstr  {
		InputPort SI = R64;
		InputPort SEL = R63_Sel;
		Parameter Size = 2725;
	}
	LogicSignal R62A_Sel {
		SEL & ~SCB62scb.toSEL;
	}
	Instance R62A Of WrappedInstr  {
		InputPort SI = R63;
		InputPort SEL = R62A_Sel;
		Parameter Size = 4339;
	}
	LogicSignal R62B_Sel {
		SEL & SCB62scb.toSEL;
	}
	Instance R62B Of WrappedInstr  {
		InputPort SI = R63;
		InputPort SEL = R62B_Sel;
		Parameter Size = 896;
	}
	ScanMux SCB62 SelectedBy SCB62scb.toSEL {
		1'b0 : R62A.SO;
		1'b1 : R62B.SO;
	}
	Instance SCB62scb Of SCB {
		InputPort SI = SCB62;
		InputPort SEL = SEL;
	}
	LogicSignal R61A_Sel {
		SEL & ~SCB61scb.toSEL;
	}
	Instance R61A Of WrappedInstr  {
		InputPort SI = SCB62scb.SO;
		InputPort SEL = R61A_Sel;
		Parameter Size = 4034;
	}
	LogicSignal R61B_Sel {
		SEL & SCB61scb.toSEL;
	}
	Instance R61B Of WrappedInstr  {
		InputPort SI = SCB62scb.SO;
		InputPort SEL = R61B_Sel;
		Parameter Size = 408;
	}
	ScanMux SCB61 SelectedBy SCB61scb.toSEL {
		1'b0 : R61A.SO;
		1'b1 : R61B.SO;
	}
	Instance SCB61scb Of SCB {
		InputPort SI = SCB61;
		InputPort SEL = SEL;
	}
	LogicSignal R60_Sel {
		SEL;
	}
	Instance R60 Of WrappedInstr  {
		InputPort SI = SCB61scb.SO;
		InputPort SEL = R60_Sel;
		Parameter Size = 2304;
	}
	LogicSignal R59_Sel {
		SEL;
	}
	Instance R59 Of WrappedInstr  {
		InputPort SI = R60;
		InputPort SEL = R59_Sel;
		Parameter Size = 884;
	}
	LogicSignal R58t_Sel {
		SIB58.toSEL;
	}
	Instance R58t Of WrappedInstr  {
		InputPort SI = SIB58.toSI;
		InputPort SEL = R58t_Sel;
		Parameter Size = 753;
	}
	LogicSignal R57t_Sel {
		SIB57.toSEL;
	}
	Instance R57t Of WrappedInstr  {
		InputPort SI = SIB57.toSI;
		InputPort SEL = R57t_Sel;
		Parameter Size = 155;
	}
	Instance SIB57 Of SIB_mux_pre {
		InputPort SI = R58t.SO;
		InputPort SEL = SIB58.toSEL;
		InputPort fromSO = R57t.SO;
	}
	LogicSignal R56A_Sel {
		SIB58.toSEL & ~SCB56scb.toSEL;
	}
	Instance R56A Of WrappedInstr  {
		InputPort SI = SIB57.SO;
		InputPort SEL = R56A_Sel;
		Parameter Size = 3072;
	}
	LogicSignal R56B_Sel {
		SIB58.toSEL & SCB56scb.toSEL;
	}
	Instance R56B Of WrappedInstr  {
		InputPort SI = SIB57.SO;
		InputPort SEL = R56B_Sel;
		Parameter Size = 2015;
	}
	ScanMux SCB56 SelectedBy SCB56scb.toSEL {
		1'b0 : R56A.SO;
		1'b1 : R56B.SO;
	}
	Instance SCB56scb Of SCB {
		InputPort SI = SCB56;
		InputPort SEL = SIB58.toSEL;
	}
	Instance SIB58 Of SIB_mux_pre {
		InputPort SI = R59.SO;
		InputPort SEL = SEL;
		InputPort fromSO = SCB56scb.SO;
	}
	LogicSignal R55t_Sel {
		SIB55.toSEL;
	}
	Instance R55t Of WrappedInstr  {
		InputPort SI = SIB55.toSI;
		InputPort SEL = R55t_Sel;
		Parameter Size = 3235;
	}
	LogicSignal R54t_Sel {
		SIB54.toSEL;
	}
	Instance R54t Of WrappedInstr  {
		InputPort SI = SIB54.toSI;
		InputPort SEL = R54t_Sel;
		Parameter Size = 3693;
	}
	LogicSignal R53_Sel {
		SIB54.toSEL;
	}
	Instance R53 Of WrappedInstr  {
		InputPort SI = R54t;
		InputPort SEL = R53_Sel;
		Parameter Size = 1870;
	}
	LogicSignal R52t_Sel {
		SIB52.toSEL;
	}
	Instance R52t Of WrappedInstr  {
		InputPort SI = SIB52.toSI;
		InputPort SEL = R52t_Sel;
		Parameter Size = 4368;
	}
	LogicSignal R51_Sel {
		SIB52.toSEL;
	}
	Instance R51 Of WrappedInstr  {
		InputPort SI = R52t;
		InputPort SEL = R51_Sel;
		Parameter Size = 173;
	}
	LogicSignal R50t_Sel {
		SIB50.toSEL;
	}
	Instance R50t Of WrappedInstr  {
		InputPort SI = SIB50.toSI;
		InputPort SEL = R50t_Sel;
		Parameter Size = 2241;
	}
	LogicSignal R49t_Sel {
		SIB49.toSEL;
	}
	Instance R49t Of WrappedInstr  {
		InputPort SI = SIB49.toSI;
		InputPort SEL = R49t_Sel;
		Parameter Size = 1181;
	}
	LogicSignal R48_Sel {
		SIB49.toSEL;
	}
	Instance R48 Of WrappedInstr  {
		InputPort SI = R49t;
		InputPort SEL = R48_Sel;
		Parameter Size = 4439;
	}
	LogicSignal R47A_Sel {
		SIB49.toSEL & ~SCB47scb.toSEL;
	}
	Instance R47A Of WrappedInstr  {
		InputPort SI = R48;
		InputPort SEL = R47A_Sel;
		Parameter Size = 478;
	}
	LogicSignal R47B_Sel {
		SIB49.toSEL & SCB47scb.toSEL;
	}
	Instance R47B Of WrappedInstr  {
		InputPort SI = R48;
		InputPort SEL = R47B_Sel;
		Parameter Size = 2834;
	}
	ScanMux SCB47 SelectedBy SCB47scb.toSEL {
		1'b0 : R47A.SO;
		1'b1 : R47B.SO;
	}
	Instance SCB47scb Of SCB {
		InputPort SI = SCB47;
		InputPort SEL = SIB49.toSEL;
	}
	LogicSignal R46_Sel {
		SIB49.toSEL;
	}
	Instance R46 Of WrappedInstr  {
		InputPort SI = SCB47scb.SO;
		InputPort SEL = R46_Sel;
		Parameter Size = 617;
	}
	Instance SIB49 Of SIB_mux_pre {
		InputPort SI = R50t.SO;
		InputPort SEL = SIB50.toSEL;
		InputPort fromSO = R46.SO;
	}
	LogicSignal R45t_Sel {
		SIB45.toSEL;
	}
	Instance R45t Of WrappedInstr  {
		InputPort SI = SIB45.toSI;
		InputPort SEL = R45t_Sel;
		Parameter Size = 2707;
	}
	LogicSignal R44t_Sel {
		SIB44.toSEL;
	}
	Instance R44t Of WrappedInstr  {
		InputPort SI = SIB44.toSI;
		InputPort SEL = R44t_Sel;
		Parameter Size = 3860;
	}
	LogicSignal R43t_Sel {
		SIB43.toSEL;
	}
	Instance R43t Of WrappedInstr  {
		InputPort SI = SIB43.toSI;
		InputPort SEL = R43t_Sel;
		Parameter Size = 2565;
	}
	LogicSignal R42t_Sel {
		SIB42.toSEL;
	}
	Instance R42t Of WrappedInstr  {
		InputPort SI = SIB42.toSI;
		InputPort SEL = R42t_Sel;
		Parameter Size = 3404;
	}
	LogicSignal R41_Sel {
		SIB42.toSEL;
	}
	Instance R41 Of WrappedInstr  {
		InputPort SI = R42t;
		InputPort SEL = R41_Sel;
		Parameter Size = 3543;
	}
	LogicSignal R40t_Sel {
		SIB40.toSEL;
	}
	Instance R40t Of WrappedInstr  {
		InputPort SI = SIB40.toSI;
		InputPort SEL = R40t_Sel;
		Parameter Size = 2181;
	}
	LogicSignal R39t_Sel {
		SIB39.toSEL;
	}
	Instance R39t Of WrappedInstr  {
		InputPort SI = SIB39.toSI;
		InputPort SEL = R39t_Sel;
		Parameter Size = 2701;
	}
	LogicSignal R38t_Sel {
		SIB38.toSEL;
	}
	Instance R38t Of WrappedInstr  {
		InputPort SI = SIB38.toSI;
		InputPort SEL = R38t_Sel;
		Parameter Size = 891;
	}
	LogicSignal R37A_Sel {
		SIB38.toSEL & ~SCB37scb.toSEL;
	}
	Instance R37A Of WrappedInstr  {
		InputPort SI = R38t;
		InputPort SEL = R37A_Sel;
		Parameter Size = 3230;
	}
	LogicSignal R37B_Sel {
		SIB38.toSEL & SCB37scb.toSEL;
	}
	Instance R37B Of WrappedInstr  {
		InputPort SI = R38t;
		InputPort SEL = R37B_Sel;
		Parameter Size = 1862;
	}
	ScanMux SCB37 SelectedBy SCB37scb.toSEL {
		1'b0 : R37A.SO;
		1'b1 : R37B.SO;
	}
	Instance SCB37scb Of SCB {
		InputPort SI = SCB37;
		InputPort SEL = SIB38.toSEL;
	}
	LogicSignal R36A_Sel {
		SIB38.toSEL & ~SCB36scb.toSEL;
	}
	Instance R36A Of WrappedInstr  {
		InputPort SI = SCB37scb.SO;
		InputPort SEL = R36A_Sel;
		Parameter Size = 3377;
	}
	LogicSignal R36B_Sel {
		SIB38.toSEL & SCB36scb.toSEL;
	}
	Instance R36B Of WrappedInstr  {
		InputPort SI = SCB37scb.SO;
		InputPort SEL = R36B_Sel;
		Parameter Size = 3390;
	}
	ScanMux SCB36 SelectedBy SCB36scb.toSEL {
		1'b0 : R36A.SO;
		1'b1 : R36B.SO;
	}
	Instance SCB36scb Of SCB {
		InputPort SI = SCB36;
		InputPort SEL = SIB38.toSEL;
	}
	LogicSignal R35_Sel {
		SIB38.toSEL;
	}
	Instance R35 Of WrappedInstr  {
		InputPort SI = SCB36scb.SO;
		InputPort SEL = R35_Sel;
		Parameter Size = 279;
	}
	Instance SIB38 Of SIB_mux_pre {
		InputPort SI = R39t.SO;
		InputPort SEL = SIB39.toSEL;
		InputPort fromSO = R35.SO;
	}
	Instance SIB39 Of SIB_mux_pre {
		InputPort SI = R40t.SO;
		InputPort SEL = SIB40.toSEL;
		InputPort fromSO = SIB38.SO;
	}
	Instance SIB40 Of SIB_mux_pre {
		InputPort SI = R41.SO;
		InputPort SEL = SIB42.toSEL;
		InputPort fromSO = SIB39.SO;
	}
	Instance SIB42 Of SIB_mux_pre {
		InputPort SI = R43t.SO;
		InputPort SEL = SIB43.toSEL;
		InputPort fromSO = SIB40.SO;
	}
	Instance SIB43 Of SIB_mux_pre {
		InputPort SI = R44t.SO;
		InputPort SEL = SIB44.toSEL;
		InputPort fromSO = SIB42.SO;
	}
	LogicSignal R34t_Sel {
		SIB34.toSEL;
	}
	Instance R34t Of WrappedInstr  {
		InputPort SI = SIB34.toSI;
		InputPort SEL = R34t_Sel;
		Parameter Size = 3394;
	}
	LogicSignal R33A_Sel {
		SIB34.toSEL & ~SCB33scb.toSEL;
	}
	Instance R33A Of WrappedInstr  {
		InputPort SI = R34t;
		InputPort SEL = R33A_Sel;
		Parameter Size = 2710;
	}
	LogicSignal R33B_Sel {
		SIB34.toSEL & SCB33scb.toSEL;
	}
	Instance R33B Of WrappedInstr  {
		InputPort SI = R34t;
		InputPort SEL = R33B_Sel;
		Parameter Size = 4474;
	}
	ScanMux SCB33 SelectedBy SCB33scb.toSEL {
		1'b0 : R33A.SO;
		1'b1 : R33B.SO;
	}
	Instance SCB33scb Of SCB {
		InputPort SI = SCB33;
		InputPort SEL = SIB34.toSEL;
	}
	Instance SIB34 Of SIB_mux_pre {
		InputPort SI = SIB43.SO;
		InputPort SEL = SIB44.toSEL;
		InputPort fromSO = SCB33scb.SO;
	}
	Instance SIB44 Of SIB_mux_pre {
		InputPort SI = R45t.SO;
		InputPort SEL = SIB45.toSEL;
		InputPort fromSO = SIB34.SO;
	}
	Instance SIB45 Of SIB_mux_pre {
		InputPort SI = SIB49.SO;
		InputPort SEL = SIB50.toSEL;
		InputPort fromSO = SIB44.SO;
	}
	Instance SIB50 Of SIB_mux_pre {
		InputPort SI = R51.SO;
		InputPort SEL = SIB52.toSEL;
		InputPort fromSO = SIB45.SO;
	}
	Instance SIB52 Of SIB_mux_pre {
		InputPort SI = R53.SO;
		InputPort SEL = SIB54.toSEL;
		InputPort fromSO = SIB50.SO;
	}
	LogicSignal R32A_Sel {
		SIB54.toSEL & ~SCB32scb.toSEL;
	}
	Instance R32A Of WrappedInstr  {
		InputPort SI = SIB52.SO;
		InputPort SEL = R32A_Sel;
		Parameter Size = 4282;
	}
	LogicSignal R32B_Sel {
		SIB54.toSEL & SCB32scb.toSEL;
	}
	Instance R32B Of WrappedInstr  {
		InputPort SI = SIB52.SO;
		InputPort SEL = R32B_Sel;
		Parameter Size = 2576;
	}
	ScanMux SCB32 SelectedBy SCB32scb.toSEL {
		1'b0 : R32A.SO;
		1'b1 : R32B.SO;
	}
	Instance SCB32scb Of SCB {
		InputPort SI = SCB32;
		InputPort SEL = SIB54.toSEL;
	}
	LogicSignal R31_Sel {
		SIB54.toSEL;
	}
	Instance R31 Of WrappedInstr  {
		InputPort SI = SCB32scb.SO;
		InputPort SEL = R31_Sel;
		Parameter Size = 351;
	}
	Instance SIB54 Of SIB_mux_pre {
		InputPort SI = R55t.SO;
		InputPort SEL = SIB55.toSEL;
		InputPort fromSO = R31.SO;
	}
	Instance SIB55 Of SIB_mux_pre {
		InputPort SI = SIB58.SO;
		InputPort SEL = SEL;
		InputPort fromSO = SIB54.SO;
	}
	LogicSignal R30t_Sel {
		SIB30.toSEL;
	}
	Instance R30t Of WrappedInstr  {
		InputPort SI = SIB30.toSI;
		InputPort SEL = R30t_Sel;
		Parameter Size = 1626;
	}
	LogicSignal R29t_Sel {
		SIB29.toSEL;
	}
	Instance R29t Of WrappedInstr  {
		InputPort SI = SIB29.toSI;
		InputPort SEL = R29t_Sel;
		Parameter Size = 1644;
	}
	Instance SIB29 Of SIB_mux_pre {
		InputPort SI = R30t.SO;
		InputPort SEL = SIB30.toSEL;
		InputPort fromSO = R29t.SO;
	}
	Instance SIB30 Of SIB_mux_pre {
		InputPort SI = SIB55.SO;
		InputPort SEL = SEL;
		InputPort fromSO = SIB29.SO;
	}
	LogicSignal R28t_Sel {
		SIB28.toSEL;
	}
	Instance R28t Of WrappedInstr  {
		InputPort SI = SIB28.toSI;
		InputPort SEL = R28t_Sel;
		Parameter Size = 2680;
	}
	LogicSignal R27A_Sel {
		SIB28.toSEL & ~SCB27scb.toSEL;
	}
	Instance R27A Of WrappedInstr  {
		InputPort SI = R28t;
		InputPort SEL = R27A_Sel;
		Parameter Size = 910;
	}
	LogicSignal R27B_Sel {
		SIB28.toSEL & SCB27scb.toSEL;
	}
	Instance R27B Of WrappedInstr  {
		InputPort SI = R28t;
		InputPort SEL = R27B_Sel;
		Parameter Size = 3337;
	}
	ScanMux SCB27 SelectedBy SCB27scb.toSEL {
		1'b0 : R27A.SO;
		1'b1 : R27B.SO;
	}
	Instance SCB27scb Of SCB {
		InputPort SI = SCB27;
		InputPort SEL = SIB28.toSEL;
	}
	Instance SIB28 Of SIB_mux_pre {
		InputPort SI = SIB30.SO;
		InputPort SEL = SEL;
		InputPort fromSO = SCB27scb.SO;
	}
	LogicSignal R26_Sel {
		SEL;
	}
	Instance R26 Of WrappedInstr  {
		InputPort SI = SIB28.SO;
		InputPort SEL = R26_Sel;
		Parameter Size = 2817;
	}
	LogicSignal R25A_Sel {
		SEL & ~SCB25scb.toSEL;
	}
	Instance R25A Of WrappedInstr  {
		InputPort SI = R26;
		InputPort SEL = R25A_Sel;
		Parameter Size = 2099;
	}
	LogicSignal R25B_Sel {
		SEL & SCB25scb.toSEL;
	}
	Instance R25B Of WrappedInstr  {
		InputPort SI = R26;
		InputPort SEL = R25B_Sel;
		Parameter Size = 448;
	}
	ScanMux SCB25 SelectedBy SCB25scb.toSEL {
		1'b0 : R25A.SO;
		1'b1 : R25B.SO;
	}
	Instance SCB25scb Of SCB {
		InputPort SI = SCB25;
		InputPort SEL = SEL;
	}
	LogicSignal R24A_Sel {
		SEL & ~SCB24scb.toSEL;
	}
	Instance R24A Of WrappedInstr  {
		InputPort SI = SCB25scb.SO;
		InputPort SEL = R24A_Sel;
		Parameter Size = 2049;
	}
	LogicSignal R24B_Sel {
		SEL & SCB24scb.toSEL;
	}
	Instance R24B Of WrappedInstr  {
		InputPort SI = SCB25scb.SO;
		InputPort SEL = R24B_Sel;
		Parameter Size = 1400;
	}
	ScanMux SCB24 SelectedBy SCB24scb.toSEL {
		1'b0 : R24A.SO;
		1'b1 : R24B.SO;
	}
	Instance SCB24scb Of SCB {
		InputPort SI = SCB24;
		InputPort SEL = SEL;
	}
	LogicSignal R23_Sel {
		SEL;
	}
	Instance R23 Of WrappedInstr  {
		InputPort SI = SCB24scb.SO;
		InputPort SEL = R23_Sel;
		Parameter Size = 1884;
	}
	LogicSignal R22_Sel {
		SEL;
	}
	Instance R22 Of WrappedInstr  {
		InputPort SI = R23;
		InputPort SEL = R22_Sel;
		Parameter Size = 438;
	}
	LogicSignal R21A_Sel {
		SEL & ~SCB21scb.toSEL;
	}
	Instance R21A Of WrappedInstr  {
		InputPort SI = R22;
		InputPort SEL = R21A_Sel;
		Parameter Size = 3152;
	}
	LogicSignal R21B_Sel {
		SEL & SCB21scb.toSEL;
	}
	Instance R21B Of WrappedInstr  {
		InputPort SI = R22;
		InputPort SEL = R21B_Sel;
		Parameter Size = 2202;
	}
	ScanMux SCB21 SelectedBy SCB21scb.toSEL {
		1'b0 : R21A.SO;
		1'b1 : R21B.SO;
	}
	Instance SCB21scb Of SCB {
		InputPort SI = SCB21;
		InputPort SEL = SEL;
	}
	LogicSignal R20t_Sel {
		SIB20.toSEL;
	}
	Instance R20t Of WrappedInstr  {
		InputPort SI = SIB20.toSI;
		InputPort SEL = R20t_Sel;
		Parameter Size = 2963;
	}
	LogicSignal R19A_Sel {
		SIB20.toSEL & ~SCB19scb.toSEL;
	}
	Instance R19A Of WrappedInstr  {
		InputPort SI = R20t;
		InputPort SEL = R19A_Sel;
		Parameter Size = 560;
	}
	LogicSignal R19B_Sel {
		SIB20.toSEL & SCB19scb.toSEL;
	}
	Instance R19B Of WrappedInstr  {
		InputPort SI = R20t;
		InputPort SEL = R19B_Sel;
		Parameter Size = 2646;
	}
	ScanMux SCB19 SelectedBy SCB19scb.toSEL {
		1'b0 : R19A.SO;
		1'b1 : R19B.SO;
	}
	Instance SCB19scb Of SCB {
		InputPort SI = SCB19;
		InputPort SEL = SIB20.toSEL;
	}
	LogicSignal R18t_Sel {
		SIB18.toSEL;
	}
	Instance R18t Of WrappedInstr  {
		InputPort SI = SIB18.toSI;
		InputPort SEL = R18t_Sel;
		Parameter Size = 212;
	}
	LogicSignal R17_Sel {
		SIB18.toSEL;
	}
	Instance R17 Of WrappedInstr  {
		InputPort SI = R18t;
		InputPort SEL = R17_Sel;
		Parameter Size = 2944;
	}
	LogicSignal R16_Sel {
		SIB18.toSEL;
	}
	Instance R16 Of WrappedInstr  {
		InputPort SI = R17;
		InputPort SEL = R16_Sel;
		Parameter Size = 2227;
	}
	LogicSignal R15t_Sel {
		SIB15.toSEL;
	}
	Instance R15t Of WrappedInstr  {
		InputPort SI = SIB15.toSI;
		InputPort SEL = R15t_Sel;
		Parameter Size = 4215;
	}
	LogicSignal R14_Sel {
		SIB15.toSEL;
	}
	Instance R14 Of WrappedInstr  {
		InputPort SI = R15t;
		InputPort SEL = R14_Sel;
		Parameter Size = 4259;
	}
	Instance SIB15 Of SIB_mux_pre {
		InputPort SI = R16.SO;
		InputPort SEL = SIB18.toSEL;
		InputPort fromSO = R14.SO;
	}
	LogicSignal R13A_Sel {
		SIB18.toSEL & ~SCB13scb.toSEL;
	}
	Instance R13A Of WrappedInstr  {
		InputPort SI = SIB15.SO;
		InputPort SEL = R13A_Sel;
		Parameter Size = 3585;
	}
	LogicSignal R13B_Sel {
		SIB18.toSEL & SCB13scb.toSEL;
	}
	Instance R13B Of WrappedInstr  {
		InputPort SI = SIB15.SO;
		InputPort SEL = R13B_Sel;
		Parameter Size = 3180;
	}
	ScanMux SCB13 SelectedBy SCB13scb.toSEL {
		1'b0 : R13A.SO;
		1'b1 : R13B.SO;
	}
	Instance SCB13scb Of SCB {
		InputPort SI = SCB13;
		InputPort SEL = SIB18.toSEL;
	}
	Instance SIB18 Of SIB_mux_pre {
		InputPort SI = SCB19scb.SO;
		InputPort SEL = SIB20.toSEL;
		InputPort fromSO = SCB13scb.SO;
	}
	LogicSignal R12t_Sel {
		SIB12.toSEL;
	}
	Instance R12t Of WrappedInstr  {
		InputPort SI = SIB12.toSI;
		InputPort SEL = R12t_Sel;
		Parameter Size = 3984;
	}
	LogicSignal R11_Sel {
		SIB12.toSEL;
	}
	Instance R11 Of WrappedInstr  {
		InputPort SI = R12t;
		InputPort SEL = R11_Sel;
		Parameter Size = 2977;
	}
	LogicSignal R10_Sel {
		SIB12.toSEL;
	}
	Instance R10 Of WrappedInstr  {
		InputPort SI = R11;
		InputPort SEL = R10_Sel;
		Parameter Size = 621;
	}
	LogicSignal R9_Sel {
		SIB12.toSEL;
	}
	Instance R9 Of WrappedInstr  {
		InputPort SI = R10;
		InputPort SEL = R9_Sel;
		Parameter Size = 2876;
	}
	Instance SIB12 Of SIB_mux_pre {
		InputPort SI = SIB18.SO;
		InputPort SEL = SIB20.toSEL;
		InputPort fromSO = R9.SO;
	}
	Instance SIB20 Of SIB_mux_pre {
		InputPort SI = SCB21scb.SO;
		InputPort SEL = SEL;
		InputPort fromSO = SIB12.SO;
	}
	LogicSignal R8_Sel {
		SEL;
	}
	Instance R8 Of WrappedInstr  {
		InputPort SI = SIB20.SO;
		InputPort SEL = R8_Sel;
		Parameter Size = 4064;
	}
	LogicSignal R7t_Sel {
		SIB7.toSEL;
	}
	Instance R7t Of WrappedInstr  {
		InputPort SI = SIB7.toSI;
		InputPort SEL = R7t_Sel;
		Parameter Size = 2679;
	}
	Instance SIB7 Of SIB_mux_pre {
		InputPort SI = R8.SO;
		InputPort SEL = SEL;
		InputPort fromSO = R7t.SO;
	}
	LogicSignal R6_Sel {
		SEL;
	}
	Instance R6 Of WrappedInstr  {
		InputPort SI = SIB7.SO;
		InputPort SEL = R6_Sel;
		Parameter Size = 3519;
	}
	LogicSignal R5A_Sel {
		SEL & ~SCB5scb.toSEL;
	}
	Instance R5A Of WrappedInstr  {
		InputPort SI = R6;
		InputPort SEL = R5A_Sel;
		Parameter Size = 3510;
	}
	LogicSignal R5B_Sel {
		SEL & SCB5scb.toSEL;
	}
	Instance R5B Of WrappedInstr  {
		InputPort SI = R6;
		InputPort SEL = R5B_Sel;
		Parameter Size = 754;
	}
	ScanMux SCB5 SelectedBy SCB5scb.toSEL {
		1'b0 : R5A.SO;
		1'b1 : R5B.SO;
	}
	Instance SCB5scb Of SCB {
		InputPort SI = SCB5;
		InputPort SEL = SEL;
	}
	LogicSignal R4t_Sel {
		SIB4.toSEL;
	}
	Instance R4t Of WrappedInstr  {
		InputPort SI = SIB4.toSI;
		InputPort SEL = R4t_Sel;
		Parameter Size = 690;
	}
	Instance SIB4 Of SIB_mux_pre {
		InputPort SI = SCB5scb.SO;
		InputPort SEL = SEL;
		InputPort fromSO = R4t.SO;
	}
	LogicSignal R3_Sel {
		SEL;
	}
	Instance R3 Of WrappedInstr  {
		InputPort SI = SIB4.SO;
		InputPort SEL = R3_Sel;
		Parameter Size = 815;
	}
	LogicSignal R2A_Sel {
		SEL & ~SCB2scb.toSEL;
	}
	Instance R2A Of WrappedInstr  {
		InputPort SI = R3;
		InputPort SEL = R2A_Sel;
		Parameter Size = 1209;
	}
	LogicSignal R2B_Sel {
		SEL & SCB2scb.toSEL;
	}
	Instance R2B Of WrappedInstr  {
		InputPort SI = R3;
		InputPort SEL = R2B_Sel;
		Parameter Size = 4377;
	}
	ScanMux SCB2 SelectedBy SCB2scb.toSEL {
		1'b0 : R2A.SO;
		1'b1 : R2B.SO;
	}
	Instance SCB2scb Of SCB {
		InputPort SI = SCB2;
		InputPort SEL = SEL;
	}
	LogicSignal R1_Sel {
		SEL;
	}
	Instance R1 Of WrappedInstr  {
		InputPort SI = SCB2scb.SO;
		InputPort SEL = R1_Sel;
		Parameter Size = 2830;
	}
	LogicSignal R0t_Sel {
		SIB0.toSEL;
	}
	Instance R0t Of WrappedInstr  {
		InputPort SI = SIB0.toSI;
		InputPort SEL = R0t_Sel;
		Parameter Size = 3663;
	}
	Instance SIB0 Of SIB_mux_pre {
		InputPort SI = R1.SO;
		InputPort SEL = SEL;
		InputPort fromSO = R0t.SO;
	}

}