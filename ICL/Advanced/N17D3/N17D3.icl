/*
* Author: Aleksa Damljanovic, Politecnico di Torino
*
* Uses modules from files: 
* - Instruments.icl
* - NetworkStructs.icl
*/


Module N17D3 {
	Attribute lic = 'h f08a4318;
	ScanInPort SI;
	CaptureEnPort CE;
	ShiftEnPort SE;
	UpdateEnPort UE;
	SelectPort SEL;
	ResetPort RST;
	TCKPort TCK;
	ScanOutPort SO {
		Source SIB4.SO;
	}
	LogicSignal R16A_Sel {
		SEL & ~SCB16scb.toSEL;
	}
	Instance R16A Of WrappedInstr  {
		InputPort SI = SI;
		InputPort SEL = R16A_Sel;
		Parameter Size = 10;
	}
	LogicSignal R16B_Sel {
		SEL & SCB16scb.toSEL;
	}
	Instance R16B Of WrappedInstr  {
		InputPort SI = SI;
		InputPort SEL = R16B_Sel;
		Parameter Size = 11;
	}
	ScanMux SCB16 SelectedBy SCB16scb.toSEL {
		1'b0 : R16A.SO;
		1'b1 : R16B.SO;
	}
	Instance SCB16scb Of SCB {
		InputPort SI = SCB16;
		InputPort SEL = SEL;
	}
	LogicSignal R15A_Sel {
		SEL & ~SCB15scb.toSEL;
	}
	Instance R15A Of WrappedInstr  {
		InputPort SI = SCB16scb.SO;
		InputPort SEL = R15A_Sel;
		Parameter Size = 14;
	}
	LogicSignal R15B_Sel {
		SEL & SCB15scb.toSEL;
	}
	Instance R15B Of WrappedInstr  {
		InputPort SI = SCB16scb.SO;
		InputPort SEL = R15B_Sel;
		Parameter Size = 20;
	}
	ScanMux SCB15 SelectedBy SCB15scb.toSEL {
		1'b0 : R15A.SO;
		1'b1 : R15B.SO;
	}
	Instance SCB15scb Of SCB {
		InputPort SI = SCB15;
		InputPort SEL = SEL;
	}
	LogicSignal R14A_Sel {
		SEL & ~SCB14scb.toSEL;
	}
	Instance R14A Of WrappedInstr  {
		InputPort SI = SCB15scb.SO;
		InputPort SEL = R14A_Sel;
		Parameter Size = 28;
	}
	LogicSignal R14B_Sel {
		SEL & SCB14scb.toSEL;
	}
	Instance R14B Of WrappedInstr  {
		InputPort SI = SCB15scb.SO;
		InputPort SEL = R14B_Sel;
		Parameter Size = 21;
	}
	ScanMux SCB14 SelectedBy SCB14scb.toSEL {
		1'b0 : R14A.SO;
		1'b1 : R14B.SO;
	}
	Instance SCB14scb Of SCB {
		InputPort SI = SCB14;
		InputPort SEL = SEL;
	}
	LogicSignal R13t_Sel {
		SIB13.toSEL;
	}
	Instance R13t Of WrappedInstr  {
		InputPort SI = SIB13.toSI;
		InputPort SEL = R13t_Sel;
		Parameter Size = 5;
	}
	LogicSignal R12t_Sel {
		SIB12.toSEL;
	}
	Instance R12t Of WrappedInstr  {
		InputPort SI = SIB12.toSI;
		InputPort SEL = R12t_Sel;
		Parameter Size = 13;
	}
	LogicSignal R11t_Sel {
		SIB11.toSEL;
	}
	Instance R11t Of WrappedInstr  {
		InputPort SI = SIB11.toSI;
		InputPort SEL = R11t_Sel;
		Parameter Size = 10;
	}
	LogicSignal R10A_Sel {
		SIB11.toSEL & ~SCB10scb.toSEL;
	}
	Instance R10A Of WrappedInstr  {
		InputPort SI = R11t;
		InputPort SEL = R10A_Sel;
		Parameter Size = 6;
	}
	LogicSignal R10B_Sel {
		SIB11.toSEL & SCB10scb.toSEL;
	}
	Instance R10B Of WrappedInstr  {
		InputPort SI = R11t;
		InputPort SEL = R10B_Sel;
		Parameter Size = 21;
	}
	ScanMux SCB10 SelectedBy SCB10scb.toSEL {
		1'b0 : R10A.SO;
		1'b1 : R10B.SO;
	}
	Instance SCB10scb Of SCB {
		InputPort SI = SCB10;
		InputPort SEL = SIB11.toSEL;
	}
	LogicSignal R9A_Sel {
		SIB11.toSEL & ~SCB9scb.toSEL;
	}
	Instance R9A Of WrappedInstr  {
		InputPort SI = SCB10scb.SO;
		InputPort SEL = R9A_Sel;
		Parameter Size = 24;
	}
	LogicSignal R9B_Sel {
		SIB11.toSEL & SCB9scb.toSEL;
	}
	Instance R9B Of WrappedInstr  {
		InputPort SI = SCB10scb.SO;
		InputPort SEL = R9B_Sel;
		Parameter Size = 17;
	}
	ScanMux SCB9 SelectedBy SCB9scb.toSEL {
		1'b0 : R9A.SO;
		1'b1 : R9B.SO;
	}
	Instance SCB9scb Of SCB {
		InputPort SI = SCB9;
		InputPort SEL = SIB11.toSEL;
	}
	LogicSignal R8_Sel {
		SIB11.toSEL;
	}
	Instance R8 Of WrappedInstr  {
		InputPort SI = SCB9scb.SO;
		InputPort SEL = R8_Sel;
		Parameter Size = 31;
	}
	Instance SIB11 Of SIB_mux_pre {
		InputPort SI = R12t.SO;
		InputPort SEL = SIB12.toSEL;
		InputPort fromSO = R8.SO;
	}
	Instance SIB12 Of SIB_mux_pre {
		InputPort SI = R13t.SO;
		InputPort SEL = SIB13.toSEL;
		InputPort fromSO = SIB11.SO;
	}
	Instance SIB13 Of SIB_mux_pre {
		InputPort SI = SCB14scb.SO;
		InputPort SEL = SEL;
		InputPort fromSO = SIB12.SO;
	}
	LogicSignal R7_Sel {
		SEL;
	}
	Instance R7 Of WrappedInstr  {
		InputPort SI = SIB13.SO;
		InputPort SEL = R7_Sel;
		Parameter Size = 26;
	}
	LogicSignal R6A_Sel {
		SEL & ~SCB6scb.toSEL;
	}
	Instance R6A Of WrappedInstr  {
		InputPort SI = R7;
		InputPort SEL = R6A_Sel;
		Parameter Size = 3;
	}
	LogicSignal R6B_Sel {
		SEL & SCB6scb.toSEL;
	}
	Instance R6B Of WrappedInstr  {
		InputPort SI = R7;
		InputPort SEL = R6B_Sel;
		Parameter Size = 18;
	}
	ScanMux SCB6 SelectedBy SCB6scb.toSEL {
		1'b0 : R6A.SO;
		1'b1 : R6B.SO;
	}
	Instance SCB6scb Of SCB {
		InputPort SI = SCB6;
		InputPort SEL = SEL;
	}
	LogicSignal R5A_Sel {
		SEL & ~SCB5scb.toSEL;
	}
	Instance R5A Of WrappedInstr  {
		InputPort SI = SCB6scb.SO;
		InputPort SEL = R5A_Sel;
		Parameter Size = 14;
	}
	LogicSignal R5B_Sel {
		SEL & SCB5scb.toSEL;
	}
	Instance R5B Of WrappedInstr  {
		InputPort SI = SCB6scb.SO;
		InputPort SEL = R5B_Sel;
		Parameter Size = 5;
	}
	ScanMux SCB5 SelectedBy SCB5scb.toSEL {
		1'b0 : R5A.SO;
		1'b1 : R5B.SO;
	}
	Instance SCB5scb Of SCB {
		InputPort SI = SCB5;
		InputPort SEL = SEL;
	}
	LogicSignal R4t_Sel {
		SIB4.toSEL;
	}
	Instance R4t Of WrappedInstr  {
		InputPort SI = SIB4.toSI;
		InputPort SEL = R4t_Sel;
		Parameter Size = 18;
	}
	LogicSignal R3t_Sel {
		SIB3.toSEL;
	}
	Instance R3t Of WrappedInstr  {
		InputPort SI = SIB3.toSI;
		InputPort SEL = R3t_Sel;
		Parameter Size = 27;
	}
	LogicSignal R2t_Sel {
		SIB2.toSEL;
	}
	Instance R2t Of WrappedInstr  {
		InputPort SI = SIB2.toSI;
		InputPort SEL = R2t_Sel;
		Parameter Size = 4;
	}
	Instance SIB2 Of SIB_mux_pre {
		InputPort SI = R3t.SO;
		InputPort SEL = SIB3.toSEL;
		InputPort fromSO = R2t.SO;
	}
	LogicSignal R1t_Sel {
		SIB1.toSEL;
	}
	Instance R1t Of WrappedInstr  {
		InputPort SI = SIB1.toSI;
		InputPort SEL = R1t_Sel;
		Parameter Size = 25;
	}
	LogicSignal R0_Sel {
		SIB1.toSEL;
	}
	Instance R0 Of WrappedInstr  {
		InputPort SI = R1t;
		InputPort SEL = R0_Sel;
		Parameter Size = 15;
	}
	LogicSignal R21_Sel {
		SIB1.toSEL;
	}
	Instance R21 Of WrappedInstr  {
		InputPort SI = R0;
		InputPort SEL = R21_Sel;
		Parameter Size = 16;
	}
	Instance SIB1 Of SIB_mux_pre {
		InputPort SI = SIB2.SO;
		InputPort SEL = SIB3.toSEL;
		InputPort fromSO = R21.SO;
	}
	Instance SIB3 Of SIB_mux_pre {
		InputPort SI = R4t.SO;
		InputPort SEL = SIB4.toSEL;
		InputPort fromSO = SIB1.SO;
	}
	LogicSignal R22A_Sel {
		SIB4.toSEL & ~SCB22scb.toSEL;
	}
	Instance R22A Of WrappedInstr  {
		InputPort SI = SIB3.SO;
		InputPort SEL = R22A_Sel;
		Parameter Size = 31;
	}
	LogicSignal R22B_Sel {
		SIB4.toSEL & SCB22scb.toSEL;
	}
	Instance R22B Of WrappedInstr  {
		InputPort SI = SIB3.SO;
		InputPort SEL = R22B_Sel;
		Parameter Size = 14;
	}
	ScanMux SCB22 SelectedBy SCB22scb.toSEL {
		1'b0 : R22A.SO;
		1'b1 : R22B.SO;
	}
	Instance SCB22scb Of SCB {
		InputPort SI = SCB22;
		InputPort SEL = SIB4.toSEL;
	}
	Instance SIB4 Of SIB_mux_pre {
		InputPort SI = SCB5scb.SO;
		InputPort SEL = SEL;
		InputPort fromSO = SCB22scb.SO;
	}

}