-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 27.11.2017 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY TrapOrFlap_tb IS
END TrapOrFlap_tb;
 
ARCHITECTURE behavior OF TrapOrFlap_tb IS 
 
    -- Declaration of TrapOrFlap iJTAG benchmark
    COMPONENT TrapOrFlap
    PORT(
         SI : IN  std_logic;
         CE : IN  std_logic;
         SE : IN  std_logic;
         UE : IN  std_logic;
         SEL : IN  std_logic;
         RST : IN  std_logic;
         TCK : IN  std_logic;
         SO : OUT  std_logic;
		 INSTR_CLK : IN  std_logic;
		 INSTR_RST : IN  std_logic
        );
    END COMPONENT;

   -- iJTAG I/O signals
   signal SI : std_logic := '0';
   signal CE : std_logic := '0';
   signal SE : std_logic := '0';
   signal UE : std_logic := '0';
   signal SEL : std_logic := '0';
   signal RST : std_logic := '0';
   signal TCK : std_logic := '0';
   signal INSTR_RST : std_logic := '0';
   signal INSTR_CLK : std_logic := '0';	
   signal SO : std_logic;
	
	-- Testbench constants
	constant TCK_period : time := 20 ns;
	constant CLK_period : time := 20 ns;
	
	constant HALF_SEPARATOR : time := 2*TCK_period;
	constant FULL_SEPARATOR : time := 8*TCK_period;	

    -- Sets maximum history size for SO output	
	constant MAX_SCANCHAIN_LENGTH : positive := 8; -- Only TESTBYTE is going to be checked
	
	-- Testbench Functions
	
	-- Returns all zeroes std_logic_vector of specified size
   function all_zeroes (number_of_zeroes : in positive) return std_logic_vector is
	  variable zero_array : std_logic_vector(0 to number_of_zeroes-1);
   begin
	  for i in zero_array'range loop
       zero_array(i) := '0';
	  end loop;
	  return zero_array;
   end function all_zeroes;	
	
	-- Converts STD_LOGIC_VECTOR to STRING
   function slv_to_string (slv_data : in std_logic_vector) return string is
	  variable string_out : string(1 to slv_data'length);
   begin
	  for i in string_out'range loop
       string_out(i) := std_logic'image(slv_data(i-1))(2);
	  end loop;
	  return string_out;
   end function slv_to_string;	

   -- Testbench signals   
   signal so_value_history : std_logic_vector(0 to MAX_SCANCHAIN_LENGTH-1);
   signal halt_simulation : std_logic:='0';
	
BEGIN
 
   INSTR_CLK <= (not INSTR_CLK and not halt_simulation) after CLK_period/2;
   
	-- Instantiate the Unit Under Test (UUT)
   uut: TrapOrFlap PORT MAP (
          SI => SI,
          CE => CE,
          SE => SE,
          UE => UE,
          SEL => SEL,
          RST => RST,
          TCK => TCK,
          SO => SO,
		  INSTR_CLK => INSTR_CLK,
		  INSTR_RST => INSTR_RST
        );
		  
   -- Scanchain SO value history	
	so_value_history <= so_value_history(1 to MAX_SCANCHAIN_LENGTH-1)&SO when (falling_edge(TCK) and SE = '1');

   -- Stimulus process
   stim_proc: process
	
	 -- Generate a number of TCK ticks
    procedure clock_tick (number_of_tick : in positive) is
    begin
	     for i in 1 to number_of_tick loop
	       TCK <= '1';
		    wait for TCK_period/2;
		    TCK <= '0';
		    wait for TCK_period/2;
		  end loop;
    end procedure clock_tick;
	 
	 -- Shifts in specified data (Capture -> Shift -> Update)
    procedure shift_data (data : in std_logic_vector) is
    begin
	     -- Capture phase
		  CE <= '1';
	     clock_tick(1);
		  CE <= '0';
		  -- Shift phase
        SE <= '1';
	     for i in data'range loop
			 SI <= data(i);
	       clock_tick(1);
		  end loop;
        SE <= '0';
		  -- Update phase
		  UE <= '1';
	     clock_tick(1);
		  UE <= '0';
    end procedure shift_data;
	 
	 -- Checks data from SO against provided expected data
    procedure data_check (expected_data : in std_logic_vector; report_type : string) is
	   variable so_data : std_logic_vector(0 to expected_data'length-1);
    begin
	   so_data := so_value_history(MAX_SCANCHAIN_LENGTH-expected_data'length to MAX_SCANCHAIN_LENGTH-1);
		if report_type = "report" then
		  if so_data = expected_data then
          report "TEST PASSED";
		  else
		    report "TEST FAILED"
		    severity ERROR;
		  end if;
		  report "Received SO = "&slv_to_string(so_data)&" (<-- SO)";	
		  report "Expected SO = "&slv_to_string(expected_data)&" (<-- SO)";	
		elsif report_type = "assert" then
		  assert (so_data = expected_data)
		  report "assert check FAILED"&LF&"Received SO = "&slv_to_string(so_data)&" (<-- SO)"&LF&"Expected SO = "&slv_to_string(expected_data)&" (<-- SO)"
		  severity ERROR;
		end if;
    end procedure data_check;	

	 variable TESTBYTE : std_logic_vector(0 to 7);	 
	 
   begin		
	   
		-- Set TESTBYTE value ---------------------------------------------------------------
		TESTBYTE := X"B3";
		
		-- Reset iJTAG chain and Instruments
	    RST <= '1';
        INSTR_RST <= '1';
		wait for TCK_period/2;
	    RST <= '0';
        INSTR_RST <= '0';
		-------------------------------------------------------------------------------------
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.0 ------------------------------------------------------------------------
		-- By default all sibs are closed
		-- Scanchain is 4 bits long 
		-- SO <- SCB1 <- SIB_12 <- SIB_11 <- SIB_10 <- SI
		-- Check that default scanchain does not work when SEL is deasserted
		
		-- Deassert SEL
		SEL <= '0';

		-- Shift in TESTBYTE + 4 zeroes to retain the scanchain
		shift_data(TESTBYTE&all_zeroes(4));
		
		-- Check that UNINITIALIZED has appeared from SO
		report "TEST No.0 is finished";
        data_check("UUUUUUUU","report");
		
		-- Assert SEL for other tests
		SEL <= '1';
	
		-- END of TEST No.0 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

        -- TEST No.1 ------------------------------------------------------------------------
		-- All sibs are closed
		-- Scanchain is 4 bits long 
		-- SO <- SCB1 <- SIB_12 <- SIB_11 <- SIB_10 <- SI
		-- Check the default scanchain

		-- Shift in TESTBYTE + 4 zeroes to retain the scanchain
		shift_data(TESTBYTE&all_zeroes(4));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.1 is finished";
        data_check(TESTBYTE,"report");
	
		-- END of TEST No.1 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

        -- TEST No.2 ------------------------------------------------------------------------
		-- Scanchain is 4 bits long 
		-- SO <- SCB1 <- SIB_12 <- SIB_11 <- SIB_10 <- SI
		-- Reconfigure SREG0 and SREG1 to value "11", write "10101101" to each Instrument
		
        -- Open SIB_10 and SIB_11 (set SIB_10 and SIB_11 to value 1)
		shift_data("0011");
		
		wait for HALF_SEPARATOR;
		
		-- SIB_20, SIB_21, SIB_22, SIB_23 and SIB_24 are now included in the scanchain
		-- Scanchain is 9 bits long 
		-- SO <- SCB1 <- SIB_12 <- SIB_11 <- SIB_24 <- SIB_23 <- SIB_10 <- SIB_22 <- SIB_21 <- SIB_20 <- SI
		
		-- Shift in TESTBYTE + needed data to all SIBs except SIB_12 (set all required SIBs to value 1)
		shift_data(TESTBYTE&"001111111");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- SREG0, SIB_30, W_7, WI_2, WI_5, WI_6, SREG1 are included in the scanchain
		-- Scanchain is 46 bits long 
		-- SO <- SCB1 <- SIB_12 <-
		-- <- SIB_11 <- SIB_24 <- SREG1 <- SIB_23 <- WI_6 <- WI_5 <- 
		-- <- SIB_10 <- SIB_22 <- WI_2 <- SIB_21 <- WI_7 <- SIB_30 <- SIB_20 <- SREG0 <- SI
		
		-- Shift in TESTBYTE + needed data to set SREG1 and SREG0 to "11" and open SIB_30
		-- (select path "1" for sMux1 and sMux5, set SIB_30 to value 1)
		shift_data(TESTBYTE&"00"&
		           "11"&"11"&'1'&X"AD"&X"AD"&
				   "11"&X"AD"&'1'&X"AD"&"11"&"11");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- WI_5, WI_2 are removed from the scanchain
		-- WI_1, WI_3, WI_4 and WI_8 are included in the scanchain
		-- Scanchain is 62 bits long 
		-- SO <- SCB1 <- SIB_12 <-
		-- <- SIB_11 <- SIB_24 <- SREG1 <- WI_4 <- WI_3 <- SIB_23 <- WI_6 <-
		-- <- SIB_10 <- SIB_22 <- WI_1 <- SIB_21 <- WI_7 <- SIB_30 <- WI_8 <- SIB_20 <- SREG0 <- SI
		
		-- Shift in TESTBYTE + needed data to retain the scanchain
		shift_data(TESTBYTE&"00"&
		           "11"&"11"&X"AD"&X"AD"&'1'&X"AD"&
				   "11"&X"AD"&'1'&X"AD"&'1'&X"AD"&'1'&"11");
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.2 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.2 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.3 ------------------------------------------------------------------------
		-- Scanchain is 62 bits long 
		-- SO <- SCB1 <- SIB_12 <-
		-- <- SIB_11 <- SIB_24 <- SREG1 <- WI_4 <- WI_3 <- SIB_23 <- WI_6 <-
		-- <- SIB_10 <- SIB_22 <- WI_1 <- SIB_21 <- WI_7 <- SIB_30 <- WI_8 <- SIB_20 <- SREG0 <- SI
		-- Open SIB_12, SIB25 and SIB26 and select path "1" for sMux4, sMux6 and sMux7, this should break the chain

        -- Close SIB_10 and SIB_11 (set SIB_10 and SIB_11 to value 0)
        -- Open SIB_12 (set SIB_12 to value 1)
		shift_data(TESTBYTE&"01"&
		           "01"&"11"&X"AD"&X"AD"&'1'&X"AD"&
				   "01"&X"AD"&'1'&X"AD"&'1'&X"AD"&'1'&"11");
		
		wait for HALF_SEPARATOR;
				   
		-- SIB_24, SREG1, WI_4, WI_3, SIB_23, WI_6, SIB_22, WI_1, SIB_21, WI_7,
		-- SIB_30, WI_8, SIB_20, SREG0 are removed from the scanchain
		-- SIB25 and SIB26 are now added to the scanchain
		-- Scanchain is 6 bits long 
		-- SO <- SCB1 <-
		-- <- SIB_12 <- SIB_26 <- SIB_25 <-
		-- <- SIB_11 <- SIB_10 <- SI

		-- Shift in TESTBYTE + needed data to open SIB25 and SIB26 (set SIB25 and SIB26 to value 1)
		shift_data(TESTBYTE&'0'&
		           "111"&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;				   
				   
		-- CONF1 and CONF2 are now added to the scanchain
		-- Scanchain is 3078 bits long 
		-- SO <- SCB1 <-
		-- <- SIB_12 <- SIB_26 <- CONF2 <- SIB_25 <- CONF1 <-
		-- <- SIB_11 <- SIB_10 <- SI

		-- Shift in TESTBYTE + needed data to set SCB1 to "1" (to select path 1 for sMux4, sMux6 and sMux7)
		shift_data(TESTBYTE&'1'&
		           "11"&all_zeroes(2048)&'1'&all_zeroes(1024)&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;					   
				   
		-- Scanchain is ???? bits long (scanchain should be broken,
		-- SIB_25 and SIB_26 are in the scanchain but unselected)
		-- SO <- SCB1 <- CONF2 <- CONF1 <- SIB_12 <- SIB_26 <- / <- SIB_25 <- SIB_11 <- SIB_10 <- SI

		-- Shift in TESTBYTE + all zero data
		shift_data(TESTBYTE&'0'&all_zeroes(2048)&all_zeroes(1024)&"00000");

		-- Check that TESTBYTE has appeared from SO
		report "TEST No.3 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.3 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.4 ------------------------------------------------------------------------
		-- Scanchain is ???? bits long (scanchain should be broken,
		-- SIB_25 and SIB_26 are in the scanchain but unselected)
		-- SO <- SCB1 <- CONF2 <- CONF1 <- SIB_12 <- SIB_26 <- / <- SIB_25 <- SIB_11 <- SIB_10 <- SI
		-- Apply RST to return the scanchain to the default state

	    RST <= '1';
        INSTR_RST <= '1';
		wait for TCK_period/2;
	    RST <= '0';
        INSTR_RST <= '0';

		-- After RST all sibs are closed
		-- Scanchain is 4 bits long 
		-- SO <- SCB1 <- SIB_12 <- SIB_11 <- SIB_10 <- SI
		
		-- Shift in TESTBYTE + needed data to retain the scanchain
		shift_data(TESTBYTE&"0000");

		-- Check that TESTBYTE has appeared from SO
		report "TEST No.4 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.4 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
		-- STOP SIMULATION ------------------------------------------------------------------
        halt_simulation <= '1';
        wait;
        
   end process;

END;
