-- Copyright Testonica Lab (C) 2017

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 27.11.2017 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TrapOrFlap is
 Generic ( regSize : positive := 8;			-- Parameter regSize = 8
           conf1Size : positive := 1024; 	-- Parameter conf1Size = 1024
		   conf2Size : positive := 2048); 	-- Parameter conf2Size = 2048
    Port ( -- iJTAG interface
	       SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort	
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
		   -- Instruments interface
		   INSTR_CLK : in STD_LOGIC; -- Instruments Clock
		   INSTR_RST : in STD_LOGIC); -- Instruments Reset
end TrapOrFlap;

architecture TrapOrFlap_arch of TrapOrFlap is

component SIB_mux_pre is
    Port ( -- Scan Interface  client --------------
	       SI : in  STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
		   -- Scan Interface  host ----------------
           fromSO : in  STD_LOGIC; -- ScanInPort
           toCE : out  STD_LOGIC; -- ToCaptureEnPort
           toSE : out  STD_LOGIC; -- ToShiftEnPort
           toUE : out  STD_LOGIC; -- ToUpdateEnPort
           toSEL : out  STD_LOGIC; -- ToSelectPort
           toRST : out  STD_LOGIC; -- ToResetPort
           toTCK : out  STD_LOGIC; -- ToTCKPort
           toSI : out  STD_LOGIC); -- ScanOutPort
end component;

signal SIB_10_so, SIB_11_so, SIB_12_so, SIB_20_so, SIB_21_so, SIB_22_so, SIB_23_so, SIB_24_so, SIB_25_so, SIB_26_so, SIB_30_so : std_logic;
signal SIB_10_toCE, SIB_11_toCE, SIB_12_toCE, SIB_20_toCE, SIB_21_toCE, SIB_22_toCE, SIB_23_toCE, SIB_24_toCE, SIB_25_toCE, SIB_26_toCE, SIB_30_toCE : std_logic;
signal SIB_10_toSE, SIB_11_toSE, SIB_12_toSE, SIB_20_toSE, SIB_21_toSE, SIB_22_toSE, SIB_23_toSE, SIB_24_toSE, SIB_25_toSE, SIB_26_toSE, SIB_30_toSE : std_logic;
signal SIB_10_toUE, SIB_11_toUE, SIB_12_toUE, SIB_20_toUE, SIB_21_toUE, SIB_22_toUE, SIB_23_toUE, SIB_24_toUE, SIB_25_toUE, SIB_26_toUE, SIB_30_toUE : std_logic;
signal SIB_10_toSEL, SIB_11_toSEL, SIB_12_toSEL, SIB_20_toSEL, SIB_21_toSEL, SIB_22_toSEL, SIB_23_toSEL, SIB_24_toSEL, SIB_25_toSEL, SIB_26_toSEL, SIB_30_toSEL : std_logic;
signal SIB_10_toRST, SIB_11_toRST, SIB_12_toRST, SIB_20_toRST, SIB_21_toRST, SIB_22_toRST, SIB_23_toRST, SIB_24_toRST, SIB_25_toRST, SIB_26_toRST, SIB_30_toRST : std_logic;
signal SIB_10_toTCK, SIB_11_toTCK, SIB_12_toTCK, SIB_20_toTCK, SIB_21_toTCK, SIB_22_toTCK, SIB_23_toTCK, SIB_24_toTCK, SIB_25_toTCK, SIB_26_toTCK, SIB_30_toTCK : std_logic;
signal SIB_10_toSI, SIB_11_toSI, SIB_12_toSI, SIB_20_toSI, SIB_21_toSI, SIB_22_toSI, SIB_23_toSI, SIB_24_toSI, SIB_25_toSI, SIB_26_toSI, SIB_30_toSI : std_logic;

component ScanMux is
 Generic (ControlSize : positive);
    Port ( ScanMux_in : in STD_LOGIC_VECTOR((2**ControlSize)-1 downto 0);
           SelectedBy : in STD_LOGIC_VECTOR(ControlSize-1 downto 0);
           ScanMux_out : out STD_LOGIC);
end component;

signal sMux1_out, sMux2_out, sMux3_out, sMux4_out, sMux5_out, sMux6_out, sMux7_out : std_logic; 

component SCB is
    Port ( -- Scan Interface  client --------------
	       SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
           ----------------------------------------
		   toSEL : out STD_LOGIC; -- ToSelectPort
		   DO : out STD_LOGIC_VECTOR (0 downto 0)); -- DataOutPort
end component;

signal SCB1_so : std_logic;
signal SCB1_toSEL : std_logic;
signal SCB1_do : std_logic_vector(0 downto 0);

component SReg is
 Generic ( Size : positive := 7 );
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
	       DI : in STD_LOGIC_VECTOR (Size-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR (Size-1 downto 0) ); --DataOutPort
end component;

signal SREG0_so : STD_LOGIC;
signal SREG0_do : STD_LOGIC_VECTOR (1 downto 0);
signal SREG1_so : STD_LOGIC;
signal SREG1_do : STD_LOGIC_VECTOR (1 downto 0);
signal CONF1_so : STD_LOGIC;
signal CONF1_do : STD_LOGIC_VECTOR (conf1Size-1 downto 0);
signal CONF2_so : STD_LOGIC;
signal CONF2_do : STD_LOGIC_VECTOR (conf2Size-1 downto 0);

component WrappedInstr is
 Generic ( Size : positive);
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   -- Instruments interface
		   INSTR_CLK : in STD_LOGIC; -- Instruments Clock
		   INSTR_RST : in STD_LOGIC); -- Instruments Reset
end component;

signal WI_0_so, WI_1_so, WI_2_so, WI_3_so, WI_4_so, WI_5_so, WI_6_so, WI_7_so, WI_8_so  : std_logic;

signal sel_WI_1 : std_logic; --LogicSignal sel_WI_1
signal sel_WI_2 : std_logic; --LogicSignal sel_WI_2
signal sel_WI_3 : std_logic; --LogicSignal sel_WI_3
signal sel_WI_4 : std_logic; --LogicSignal sel_WI_4
signal sel_WI_5 : std_logic; --LogicSignal sel_WI_5
signal sel_SIB_25_26 : std_logic; --LogicSignal sel_SIB_25_26
signal sel_CONF1 : std_logic; --LogicSignal sel_CONF1
signal sel_CONF2 : std_logic; --LogicSignal sel_CONF2

begin

SO <= SCB1_so; -- Source SCB1.SO

SIB_10 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => SI, --InputPort SI = SI
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL, --InputPort SEL = SEL
               RST => RST,
               TCK => TCK,
               SO => SIB_10_so,
		         -- Scan Interface host ----------------
               fromSO => SIB_22_so, --InputPort fromSO = SIB_22.SO
               toCE => SIB_10_toCE,
               toSE => SIB_10_toSE,
               toUE => SIB_10_toUE,
               toSEL => SIB_10_toSEL,
               toRST => SIB_10_toRST,
               toTCK => SIB_10_toTCK,
               toSI => SIB_10_toSI);	
			   
SIB_11 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => SIB_10_so, --InputPort SI = SIB_10.SO
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL, --InputPort SEL = SEL
               RST => RST,
               TCK => TCK,
               SO => SIB_11_so,
		         -- Scan Interface host ----------------
               fromSO => SIB_24_so, --InputPort fromSO = SIB_24.SO
               toCE => SIB_11_toCE,
               toSE => SIB_11_toSE,
               toUE => SIB_11_toUE,
               toSEL => SIB_11_toSEL,
               toRST => SIB_11_toRST,
               toTCK => SIB_11_toTCK,
               toSI => SIB_11_toSI);	
			   
SIB_12 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => SIB_11_so, --InputPort SI = SIB_11.SO
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL, --InputPort SEL = SEL
               RST => RST,
               TCK => TCK,
               SO => SIB_12_so,
		         -- Scan Interface host ----------------
               fromSO => SIB_26_so, --InputPort fromSO = SIB_26.SO
               toCE => SIB_12_toCE,
               toSE => SIB_12_toSE,
               toUE => SIB_12_toUE,
               toSEL => SIB_12_toSEL,
               toRST => SIB_12_toRST,
               toTCK => SIB_12_toTCK,
               toSI => SIB_12_toSI);	
			   
sMux4 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB_12_so, -- 1'b0 : SIB_12.SO
	           ScanMux_in(1) => CONF2_so, -- 1'b1 : CONF2.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux4_out);
			   
SCB1 : SCB
    Port map ( -- Scan Interface  client --------------
	           SI => sMux4_out, -- InputPort SI = sMux4
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL, -- InputPort SEL = SEL
               RST => RST,
               TCK => TCK,
               SO => SCB1_so,
               ----------------------------------------
			   toSEL => SCB1_toSEL,
			   DO => SCB1_do);
			   
SIB_20 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => SIB_10_toSI, --InputPort SI = SIB_10.toSI
               CE => SIB_10_toCE,
               SE => SIB_10_toSE,
               UE => SIB_10_toUE,
               SEL => SIB_10_toSEL, --InputPort SEL = SIB_10.toSEL
               RST => SIB_10_toRST,
               TCK => SIB_10_toTCK,
               SO => SIB_20_so,
		         -- Scan Interface host ----------------
               fromSO => SREG0_so, --InputPort fromSO = SREG0.SO
               toCE => SIB_20_toCE,
               toSE => SIB_20_toSE,
               toUE => SIB_20_toUE,
               toSEL => SIB_20_toSEL,
               toRST => SIB_20_toRST,
               toTCK => SIB_20_toTCK,
               toSI => SIB_20_toSI);	
			   
SIB_21 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => SIB_20_so, --InputPort SI = SIB_20.SO
               CE => SIB_10_toCE,
               SE => SIB_10_toSE,
               UE => SIB_10_toUE,
               SEL => SIB_10_toSEL, --InputPort SEL = SIB_10.toSEL
               RST => SIB_10_toRST,
               TCK => SIB_10_toTCK,
               SO => SIB_21_so,
		         -- Scan Interface host ----------------
               fromSO => WI_7_so, --InputPort fromSO = WI_7.SO
               toCE => SIB_21_toCE,
               toSE => SIB_21_toSE,
               toUE => SIB_21_toUE,
               toSEL => SIB_21_toSEL,
               toRST => SIB_21_toRST,
               toTCK => SIB_21_toTCK,
               toSI => SIB_21_toSI);	
			   
SIB_22 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => SIB_21_so, --InputPort SI = SIB_21.SO
               CE => SIB_10_toCE,
               SE => SIB_10_toSE,
               UE => SIB_10_toUE,
               SEL => SIB_10_toSEL, --InputPort SEL = SIB_10.toSEL
               RST => SIB_10_toRST,
               TCK => SIB_10_toTCK,
               SO => SIB_22_so,
		         -- Scan Interface host ----------------
               fromSO => sMux1_out, -- InputPort fromSO = sMux1
               toCE => SIB_22_toCE,
               toSE => SIB_22_toSE,
               toUE => SIB_22_toUE,
               toSEL => SIB_22_toSEL,
               toRST => SIB_22_toRST,
               toTCK => SIB_22_toTCK,
               toSI => SIB_22_toSI);	

SREG0 : SReg 
 Generic map ( Size => 2 ) -- Parameter Size = 2
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB_20_toSI, -- Input Port SI = SIB_20.toSI
               SO => SREG0_so,
               SEL => SIB_20_toSEL,
               ----------------------------------------		
               SE => SIB_20_toSE,
               CE => SIB_20_toCE,
               UE => SIB_20_toUE,
               RST => SIB_20_toRST,
               TCK => SIB_20_toTCK,
			   DI => "11", -- Input Port DI = 2'b11
			   DO => SREG0_do);	

SIB_30 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => SIB_21_toSI, --InputPort SI = SIB_21.toSI
               CE => SIB_21_toCE,
               SE => SIB_21_toSE,
               UE => SIB_21_toUE,
               SEL => SIB_21_toSEL, --InputPort SEL = SIB_21.toSEL
               RST => SIB_21_toRST,
               TCK => SIB_21_toTCK,
               SO => SIB_30_so,
		         -- Scan Interface host ----------------
               fromSO => WI_8_so, --InputPort fromSO = WI_8.SO
               toCE => SIB_30_toCE,
               toSE => SIB_30_toSE,
               toUE => SIB_30_toUE,
               toSEL => SIB_30_toSEL,
               toRST => SIB_30_toRST,
               toTCK => SIB_30_toTCK,
               toSI => SIB_30_toSI);

WI_7 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB_30_so, --InputPort SI = SIB_30.SO
               SO => WI_7_so,
               SEL => SIB_21_toSEL, -- InputPort SEL = SIB_21.toSEL
               ----------------------------------------		
               SE => SIB_21_toSE,
               CE => SIB_21_toCE,
               UE => SIB_21_toUE,
               RST => SIB_21_toRST,
               TCK => SIB_21_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);			

WI_8 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB_30_toSI, --InputPort SI = SIB_30.toSI
               SO => WI_8_so,
               SEL => SIB_30_toSEL, -- InputPort SEL = SIB_30.toSEL
               ----------------------------------------		
               SE => SIB_30_toSE,
               CE => SIB_30_toCE,
               UE => SIB_30_toUE,
               RST => SIB_30_toRST,
               TCK => SIB_30_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);		

sel_WI_1 <= SREG0_do(0) and SIB_22_toSEL; --LogicSignal sel_WI_1
sel_WI_2 <= not SREG0_do(0) and SIB_22_toSEL; --LogicSignal sel_WI_2
			  
WI_1 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB_22_toSI, --InputPort SI = SIB_22.toSI
               SO => WI_1_so,
               SEL => sel_WI_1, -- InputPort SEL = sel_WI_1
               ----------------------------------------		
               SE => SIB_22_toSE,
               CE => SIB_22_toCE,
               UE => SIB_22_toUE,
               RST => SIB_22_toRST,
               TCK => SIB_22_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);			

WI_2 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB_22_toSI, --InputPort SI = SIB_22.toSI
               SO => WI_2_so,
               SEL => sel_WI_2, -- InputPort SEL = sel_WI_2
               ----------------------------------------		
               SE => SIB_22_toSE,
               CE => SIB_22_toCE,
               UE => SIB_22_toUE,
               RST => SIB_22_toRST,
               TCK => SIB_22_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);	

sMux1 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => WI_2_so, -- 1'b0 : WI_2.SO
	           ScanMux_in(1) => WI_1_so, -- 1'b1 : WI_1.SO
               SelectedBy => SREG0_do(0 downto 0), --SelectedBy SREG0.DO[0]
               ScanMux_out => sMux1_out);	

SIB_23 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => SIB_11_toSI, --InputPort SI = SIB_11.toSI
               CE => SIB_11_toCE,
               SE => SIB_11_toSE,
               UE => SIB_11_toUE,
               SEL => SIB_11_toSEL, --InputPort SEL = SIB_11.toSEL
               RST => SIB_11_toRST,
               TCK => SIB_11_toTCK,
               SO => SIB_23_so,
		         -- Scan Interface host ----------------
               fromSO => WI_6_so, --InputPort fromSO = WI_6.SO
               toCE => SIB_23_toCE,
               toSE => SIB_23_toSE,
               toUE => SIB_23_toUE,
               toSEL => SIB_23_toSEL,
               toRST => SIB_23_toRST,
               toTCK => SIB_23_toTCK,
               toSI => SIB_23_toSI);		

SIB_24 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => SIB_23_so, --InputPort SI = SIB_23.SO
               CE => SIB_11_toCE,
               SE => SIB_11_toSE,
               UE => SIB_11_toUE,
               SEL => SIB_11_toSEL, --InputPort SEL = SIB_11.toSEL
               RST => SIB_11_toRST,
               TCK => SIB_11_toTCK,
               SO => SIB_24_so,
		         -- Scan Interface host ----------------
               fromSO => SREG1_so, --InputPort fromSO = SREG1.SO
               toCE => SIB_24_toCE,
               toSE => SIB_24_toSE,
               toUE => SIB_24_toUE,
               toSEL => SIB_24_toSEL,
               toRST => SIB_24_toRST,
               toTCK => SIB_24_toTCK,
               toSI => SIB_24_toSI);	

sel_WI_5 <= not SREG0_do(1) and SIB_23_toSEL; --LogicSignal sel_WI_5	

WI_5 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB_23_toSI, --InputPort SI = SIB_23.toSI
               SO => WI_5_so,
               SEL => sel_WI_5, -- InputPort SEL = sel_WI_5
               ----------------------------------------		
               SE => SIB_23_toSE,
               CE => SIB_23_toCE,
               UE => SIB_23_toUE,
               RST => SIB_23_toRST,
               TCK => SIB_23_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);		

sMux5 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => WI_5_so, -- 1'b0 : WI_5.SO
	           ScanMux_in(1) => SIB_23_toSI, -- 1'b1 : SIB_23.toSI
               SelectedBy => SREG0_do(1 downto 1), --SelectedBy SREG0.DO[1]
               ScanMux_out => sMux5_out);				   
			   
WI_6 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	           SI => sMux5_out, --InputPort SI = sMux5
               SO => WI_6_so,
               SEL => SIB_23_toSEL, -- InputPort SEL = SIB_23.toSEL
               ----------------------------------------		
               SE => SIB_23_toSE,
               CE => SIB_23_toCE,
               UE => SIB_23_toUE,
               RST => SIB_23_toRST,
               TCK => SIB_23_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);	

sel_WI_3 <= SREG1_do(1) and SIB_24_toSEL; --LogicSignal sel_WI_3	

WI_3 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB_24_toSI, --InputPort SI = SIB_24.toSI
               SO => WI_3_so,
               SEL => sel_WI_3, -- InputPort SEL = sel_WI_3
               ----------------------------------------		
               SE => SIB_24_toSE,
               CE => SIB_24_toCE,
               UE => SIB_24_toUE,
               RST => SIB_24_toRST,
               TCK => SIB_24_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);	

sMux2 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB_24_toSI, -- 1'b0 : SIB_24.toSI
	           ScanMux_in(1) => WI_3_so, -- 1'b1 : WI_3.SO
               SelectedBy => SREG1_do(1 downto 1), --SelectedBy SREG1.DO[1]
               ScanMux_out => sMux2_out);	

sMux3 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => sMux2_out, -- 1'b0 : sMux2
	           ScanMux_in(1) => WI_4_so, -- 1'b1 : WI_4.SO
               SelectedBy => SREG1_do(0 downto 0), --SelectedBy SREG1.DO[0]
               ScanMux_out => sMux3_out);	

sel_WI_4 <= SREG1_do(0) and SIB_24_toSEL; --LogicSignal sel_WI_4	

WI_4 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	           SI => sMux2_out, --InputPort SI = sMux2
               SO => WI_4_so,
               SEL => sel_WI_4, -- InputPort SEL = sel_WI_4
               ----------------------------------------		
               SE => SIB_24_toSE,
               CE => SIB_24_toCE,
               UE => SIB_24_toUE,
               RST => SIB_24_toRST,
               TCK => SIB_24_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);		

SREG1 : SReg 
 Generic map ( Size => 2 ) -- Parameter Size = 2
    Port map ( -- Scan Interface scan_client ----------
	           SI => sMux3_out, -- Input Port SI = sMux3
               SO => SREG1_so,
               SEL => SIB_24_toSEL,
               ----------------------------------------		
               SE => SIB_24_toSE,
               CE => SIB_24_toCE,
               UE => SIB_24_toUE,
               RST => SIB_24_toRST,
               TCK => SIB_24_toTCK,
			   DI => "00", -- Input Port DI = 2'b00
			   DO => SREG1_do);		

sel_SIB_25_26 <= not SCB1_do(0) and SIB_12_toSEL; --LogicSignal sel_SIB_25_26

SIB_25 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => SIB_12_toSI, --InputPort SI = SIB_12.toSI
               CE => SIB_12_toCE,
               SE => SIB_12_toSE,
               UE => SIB_12_toUE,
               SEL => sel_SIB_25_26, --InputPort SEL = sel_SIB_25_26
               RST => SIB_12_toRST,
               TCK => SIB_12_toTCK,
               SO => SIB_25_so,
		         -- Scan Interface host ----------------
               fromSO => CONF1_so, --InputPort fromSO = CONF1.SO
               toCE => SIB_25_toCE,
               toSE => SIB_25_toSE,
               toUE => SIB_25_toUE,
               toSEL => SIB_25_toSEL,
               toRST => SIB_25_toRST,
               toTCK => SIB_25_toTCK,
               toSI => SIB_25_toSI);

SIB_26 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => SIB_25_so, --InputPort SI = SIB_25.SO
               CE => SIB_12_toCE,
               SE => SIB_12_toSE,
               UE => SIB_12_toUE,
               SEL => sel_SIB_25_26, --InputPort SEL = sel_SIB_25_26
               RST => SIB_12_toRST,
               TCK => SIB_12_toTCK,
               SO => SIB_26_so,
		         -- Scan Interface host ----------------
               fromSO => CONF2_so, --InputPort fromSO = CONF2.SO
               toCE => SIB_26_toCE,
               toSE => SIB_26_toSE,
               toUE => SIB_26_toUE,
               toSEL => SIB_26_toSEL,
               toRST => SIB_26_toRST,
               toTCK => SIB_26_toTCK,
               toSI => SIB_26_toSI);

sMux6 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB_25_toSI, -- 1'b0 : SIB_25.toSI
	           ScanMux_in(1) => SIB_12_so, -- 1'b1 : SIB_12.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux6_out);	

sMux7 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB_26_toSI, -- 1'b0 : SIB_26.toSI
	           ScanMux_in(1) => CONF1_so, -- 1'b1 : CONF1.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux7_out);	

sel_CONF1 <= SCB1_do(0) or SIB_25_toSEL; --LogicSignal sel_CONF1			   
sel_CONF2 <= SCB1_do(0) or SIB_26_toSEL; --LogicSignal sel_CONF2	

CONF1 : SReg 
 Generic map ( Size => conf1Size ) -- Parameter Size = $conf1Size
    Port map ( -- Scan Interface scan_client ----------
	           SI => sMux6_out, -- Input Port SI = sMux6
               SO => CONF1_so,
               SEL => sel_CONF1, -- Input Port SEL = sel_CONF1
               ----------------------------------------		
               SE => SIB_25_toSE,
               CE => SIB_25_toCE,
               UE => SIB_25_toUE,
               RST => SIB_25_toRST,
               TCK => SIB_25_toTCK,
			   DI => std_logic_vector(to_unsigned(0, conf1Size)), -- Input Port DI = 0
			   DO => CONF1_do);	

CONF2 : SReg 
 Generic map ( Size => conf2Size ) -- Parameter Size = $conf2Size
    Port map ( -- Scan Interface scan_client ----------
	           SI => sMux7_out, -- Input Port SI = sMux7
               SO => CONF2_so,
               SEL => sel_CONF2, -- Input Port SEL = sel_CONF2
               ----------------------------------------		
               SE => SIB_26_toSE,
               CE => SIB_26_toCE,
               UE => SIB_26_toUE,
               RST => SIB_26_toRST,
               TCK => SIB_26_toTCK,
			   DI => std_logic_vector(to_unsigned(0, conf2Size)), -- Input Port DI = 0
			   DO => CONF2_do);			   
			   
end TrapOrFlap_arch;