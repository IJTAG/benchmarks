-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 08.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Instrument is
 Generic ( Size : positive := 8 );
    Port ( RST : in STD_LOGIC;
           CLK : in STD_LOGIC;
           DI : in STD_LOGIC_VECTOR (Size-1 downto 0); -- DataInPort
           DO : out STD_LOGIC_VECTOR (Size-1 downto 0) ); -- DataOutPort
end Instrument;

architecture Instrument_arch of Instrument is

signal dr_do: STD_LOGIC_VECTOR (Size-1 downto 0);
signal enable : STD_LOGIC;

component DataRegister is
 Generic ( Size : positive;
           ResetValue : STD_LOGIC_VECTOR );
    Port ( RST : in STD_LOGIC;
           CLK : in STD_LOGIC;
           WriteEnableSource : in STD_LOGIC;
           WriteDataSource : in STD_LOGIC_VECTOR (Size-1 downto 0);
           DataRegister_out : out STD_LOGIC_VECTOR (Size-1 downto 0) );
end component;

constant ResetValue : STD_LOGIC_VECTOR (Size-1 downto 0) := (others => '0'); -- default

begin

DO <= dr_do; -- Source dr
enable <= DI(Size-1); -- Alias enable = DI(Size-1)

dr : DataRegister
 Generic map ( Size => Size,
               ResetValue => ResetValue )
    Port map ( RST => RST, -- default
               CLK => CLK, -- default
               WriteEnableSource => enable, -- WriteEnableSource enable
               WriteDataSource => DI, -- WriteDataSource DI
               DataRegister_out => dr_do );

end Instrument_arch;
---------------------------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity LoopDIDO is
 Generic ( LoopDIDO_width : positive := 1 ); -- Parameter width = 1; (width is a reserved word!!)
    Port ( DI : in STD_LOGIC_VECTOR (LoopDIDO_width-1 downto 0); -- DataInPort
           DO : out STD_LOGIC_VECTOR (LoopDIDO_width-1 downto 0) ); -- DataOutPort
end LoopDIDO;

architecture LoopDIDO_arch of LoopDIDO is

begin

DO <= DI; -- Source DI [$width-1 : 0]

end LoopDIDO_arch;
---------------------------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SReg is
 Generic ( Size : positive := 7 );
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
	       DI : in STD_LOGIC_VECTOR (Size-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR (Size-1 downto 0) ); --DataOutPort
end SReg;

architecture SReg_arch of SReg is

signal SR_so : STD_LOGIC;
signal SR_do : STD_LOGIC_VECTOR (Size-1 downto 0);

component ScanRegister is
 Generic ( Size : positive;
           BitOrder : string; -- MSBLSB / LSBMSB
           SOSource : natural;
           ResetValue : STD_LOGIC_VECTOR );
    Port ( SI : in STD_LOGIC;
           CE : in STD_LOGIC;
           SE : in STD_LOGIC;
           UE : in STD_LOGIC;
           SEL : in STD_LOGIC;
           RST : in STD_LOGIC;
           TCK : in STD_LOGIC;
           SO : out STD_LOGIC;
           CaptureSource : in STD_LOGIC_VECTOR (Size-1 downto 0);
           ScanRegister_out : out STD_LOGIC_VECTOR (Size-1 downto 0) );
end component;

constant ResetValue : STD_LOGIC_VECTOR (Size-1 downto 0) := (others => '0'); -- ResetValue 1'b0

begin

SO <= SR_so; -- Source SR
DO <= SR_do; -- Source SR

SR : ScanRegister
 Generic map ( Size => Size,
               BitOrder => "MSBLSB", -- MSBLSB / LSBMSB
               SOSource => 0, -- Source SR[0]
               ResetValue => ResetValue )
    Port map ( SI => SI, -- ScanInSource SI
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL,
               RST => RST,
               TCK => TCK,
               SO => SR_so,
               CaptureSource => DI, -- CaptureSource DI
               ScanRegister_out => SR_do );

end SReg_arch;
---------------------------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity WrappedInstr is
 Generic ( Size : positive := 8 );
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   -- Instruments interface
		   INSTR_CLK : in STD_LOGIC; -- Instruments Clock
		   INSTR_RST : in STD_LOGIC ); -- Instruments Reset
end WrappedInstr;

architecture WrappedInstr_arch of WrappedInstr is

signal I1_do: STD_LOGIC_VECTOR (Size-1 downto 0);

component Instrument is
 Generic ( Size : positive := 8 );
    Port ( RST : in STD_LOGIC;
           CLK : in STD_LOGIC;
           DI : in STD_LOGIC_VECTOR (Size-1 downto 0); -- DataInPort
           DO : out STD_LOGIC_VECTOR (Size-1 downto 0) ); -- DataOutPort
end component;

signal reg8_so: STD_LOGIC;
signal reg8_do: STD_LOGIC_VECTOR (Size-1 downto 0);

component SReg is
 Generic ( Size : positive := 7 );
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR (Size-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR (Size-1 downto 0) ); --DataOutPort
end component;

begin

SO <= reg8_so; -- Source reg8.SO

I1 : Instrument
 Generic map ( Size => Size )
    Port map ( RST => INSTR_RST,
               CLK => INSTR_CLK,
               DI => reg8_do, -- Input Port DI = reg8.DO
               DO => I1_do );
			  
reg8 : SReg 
 Generic map ( Size => Size )
    Port map ( -- Scan Interface scan_client ----------
	           SI => SI, -- Input Port SI = SI
               SO => reg8_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
			   DI => I1_do, -- Input Port DI = I1.DO
			   DO => reg8_do );		  

end WrappedInstr_arch;
---------------------------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity WrappedScan is
 Generic ( dataWidth : positive := 1 );
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC ); -- TCKPort
end WrappedScan;

architecture WrappedScan_arch of WrappedScan is

signal I1_do: STD_LOGIC_VECTOR (dataWidth-1 downto 0);

component LoopDIDO is
 Generic ( LoopDIDO_width : positive := 1 ); -- Parameter width = 1; (width is a reserved word!!)
    Port ( DI : in STD_LOGIC_VECTOR (LoopDIDO_width-1 downto 0); -- DataInPort
           DO : out STD_LOGIC_VECTOR (LoopDIDO_width-1 downto 0) ); -- DataOutPort
end component;

signal reg_so: STD_LOGIC;
signal reg_do: STD_LOGIC_VECTOR (dataWidth-1 downto 0);

component SReg is
 Generic ( Size : positive := 7 );
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR (Size-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR (Size-1 downto 0) ); --DataOutPort
end component;

begin

SO <= reg_so; -- Source reg.SO

I1 : LoopDIDO
 Generic map ( LoopDIDO_width => dataWidth ) -- Parameter width = $dataWidth;
    Port map ( DI => reg_do, -- Input Port DI = reg.DO
               DO => I1_do );
			  
reg : SReg 
 Generic map ( Size => dataWidth ) -- Parameter Size = $dataWidth;
    Port map ( -- Scan Interface scan_client ----------
	           SI => SI, -- Input Port SI = SI
               SO => reg_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
			   DI => I1_do, -- Input Port DI = I1.DO
			   DO => reg_do );		  

end WrappedScan_arch;