-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 21.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ScanRegister is
 Generic (Size : positive := 1;
          BitOrder : string := "MSBLSB"; -- MSBLSB / LSBMSB
          SOSource : natural := 0;
          ResetValue : STD_LOGIC_VECTOR := "0");
    Port ( SI : in STD_LOGIC;
           CE : in STD_LOGIC;
           SE : in STD_LOGIC;
           UE : in STD_LOGIC;
           SEL : in STD_LOGIC;
           RST : in STD_LOGIC;
           TCK : in STD_LOGIC;
           SO : out STD_LOGIC;
           CaptureSource : in STD_LOGIC_VECTOR (Size-1 downto 0);
           ScanRegister_out : out STD_LOGIC_VECTOR (Size-1 downto 0));
end ScanRegister;

architecture ScanRegister_arch of ScanRegister is

signal and_ce, and_se, and_ue: std_logic;
signal internal_si: std_logic_vector(Size downto 0);
signal cs_reg: std_logic_vector(Size-1 downto 0);
signal u_reg: std_logic_vector(Size-1 downto 0):=ResetValue;
signal se_mux, ce_mux, ue_mux: std_logic_vector(Size-1 downto 0);

begin

-- Basic Combinational Logic
and_ce <= CE and SEL;
and_se <= SE and SEL;
and_ue <= UE and SEL;
internal_si(Size) <= SI;

-- TDR Shift Register Core
SCAN_REGISTER: for i in Size-1 downto 0 generate

-- Multiplexers
se_mux(i) <= internal_si(i+1) when and_se = '1' else cs_reg(i);
ce_mux(i) <= CaptureSource(i) when and_ce = '1' else se_mux(i);
ue_mux(i) <= cs_reg(i) when and_ue = '1' else u_reg(i);

-- Flip-Flops
cs_reg(i) <= ce_mux(i) when TCK'event and TCK = '1';
process(RST,TCK)
begin
  if RST = '1' then
    u_reg(i) <= ResetValue(Size-1-i);
  elsif TCK'event and TCK = '0' then
    u_reg(i) <= ue_mux(i);
  end if;
end process;

-- Internal Connections
internal_si(i) <= cs_reg(i);

end generate;

-- Outputs
MSBLSB_SO : if BitOrder = "MSBLSB" generate
              SO <= internal_si(SOSource);
				end generate;
LSBMSB_SO : if BitOrder = "LSBMSB" generate
              SO <= internal_si(Size-1-SOSource);
				end generate;				
ScanRegister_out <= u_reg;

end ScanRegister_arch;
---------------------------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ScanMux is
 Generic (ControlSize : positive := 3);
    Port ( ScanMux_in : in STD_LOGIC_VECTOR((2**ControlSize)-1 downto 0);
           SelectedBy : in STD_LOGIC_VECTOR(ControlSize-1 downto 0);
           ScanMux_out : out STD_LOGIC);
end ScanMux;

architecture ScanMux_arch of ScanMux is

signal internal_connections: std_logic_vector((2**(ControlSize+1))-2 downto 0);

begin

internal_connections((2**ControlSize)-1 downto 0) <= ScanMux_in;

mux_network: for i in 0 to ControlSize-1 generate

 mux_layer: for j in 0 to 2**(ControlSize-1-i)-1 generate
 
   internal_connections((2**(ControlSize+1))-(2**(ControlSize-i))+j) <= internal_connections((2**(ControlSize+1))-(2**(ControlSize+1-i))+2*j+1) when SelectedBy(i) = '1' else internal_connections((2**(ControlSize+1))-(2**(ControlSize+1-i))+2*j+0);

 end generate;

end generate;

ScanMux_out <= internal_connections((2**(ControlSize+1))-2);

end ScanMux_arch;
---------------------------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DataRegister is
 Generic (Size : positive := 8;
          ResetValue : STD_LOGIC_VECTOR := "00000001");
    Port ( RST : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           WriteEnableSource : in STD_LOGIC;
           WriteDataSource : in STD_LOGIC_VECTOR (Size-1 downto 0);
           DataRegister_out : out STD_LOGIC_VECTOR (Size-1 downto 0));
end DataRegister;

architecture DataRegister_arch of DataRegister is

begin

process(RST,CLK)
begin
  if RST = '1' then
    DataRegister_out <= ResetValue;
  elsif CLK'event and CLK = '0' then
    if WriteEnableSource = '1' then
      DataRegister_out <= WriteDataSource;
	 end if;
  end if;
end process;

end DataRegister_arch;
---------------------------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DataMux is
 Generic ( ControlSize : positive := 2;
           DataSize : positive := 8);
    Port ( DataMux_in : in STD_LOGIC_VECTOR(((2**ControlSize)*DataSize)-1 downto 0);
           SelectedBy : in STD_LOGIC_VECTOR(ControlSize-1 downto 0);
           DataMux_out : out STD_LOGIC_VECTOR(DataSize-1 downto 0));
end DataMux;

architecture DataMux_arch of DataMux is

type internal_connections_array is array ((2**(ControlSize+1))-2 downto 0) of std_logic_vector(DataSize-1 downto 0);
signal internal_connections: internal_connections_array;

begin

mux_data_init: for i in 0 to (2**ControlSize)-1 generate
 
  internal_connections(i) <= DataMux_in((DataSize*(i+1))-1 downto DataSize*i);

end generate;

mux_network: for i in 0 to ControlSize-1 generate

 mux_layer: for j in 0 to 2**(ControlSize-1-i)-1 generate
 
   internal_connections((2**(ControlSize+1))-(2**(ControlSize-i))+j) <= internal_connections((2**(ControlSize+1))-(2**(ControlSize+1-i))+2*j+1) when SelectedBy(i) = '1' else internal_connections((2**(ControlSize+1))-(2**(ControlSize+1-i))+2*j+0);

 end generate;

end generate;

DataMux_out <= internal_connections((2**(ControlSize+1))-2);

end DataMux_arch;