-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 08.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TreeUnbalanced is
    Port ( -- iJTAG interface
	       SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort	
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC); -- ScanOutPort
end TreeUnbalanced;

architecture TreeUnbalanced_arch of TreeUnbalanced is

component SIB_mux_pre is
    Port ( -- Scan Interface  client --------------
	       SI : in  STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
		   -- Scan Interface  host ----------------
           fromSO : in  STD_LOGIC; -- ScanInPort
           toCE : out  STD_LOGIC; -- ToCaptureEnPort
           toSE : out  STD_LOGIC; -- ToShiftEnPort
           toUE : out  STD_LOGIC; -- ToUpdateEnPort
           toSEL : out  STD_LOGIC; -- ToSelectPort
           toRST : out  STD_LOGIC; -- ToResetPort
           toTCK : out  STD_LOGIC; -- ToTCKPort
           toSI : out  STD_LOGIC); -- ScanOutPort
end component;

signal sib0_so, sib1_so, sibM6_so, sib2_so, sibM7_so : std_logic;
signal sib0_toCE, sib1_toCE, sibM6_toCE, sib2_toCE, sibM7_toCE : std_logic;
signal sib0_toSE, sib1_toSE, sibM6_toSE, sib2_toSE, sibM7_toSE : std_logic;
signal sib0_toUE, sib1_toUE, sibM6_toUE, sib2_toUE, sibM7_toUE : std_logic;
signal sib0_toSEL, sib1_toSEL, sibM6_toSEL, sib2_toSEL, sibM7_toSEL : std_logic;
signal sib0_toRST, sib1_toRST, sibM6_toRST, sib2_toRST, sibM7_toRST : std_logic;
signal sib0_toTCK, sib1_toTCK, sibM6_toTCK, sib2_toTCK, sibM7_toTCK : std_logic;
signal sib0_toSI, sib1_toSI, sibM6_toSI, sib2_toSI, sibM7_toSI : std_logic;

component EmptyModule is
 Generic ( inputs : positive := 1; -- Parameter inputs = 1
           outputs : positive := 1;-- Parameter outputs = 1
		   bidirs : positive := 1;-- Parameter bidirs = 1
		   size : positive := inputs + outputs + bidirs*3); -- Parameter inputs = $inputs + $outputs + $bidirs
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC); -- TCKPort
end component;

signal M0_so : STD_LOGIC;

component EmptyModule_NoBidirs is
 Generic ( inputs : positive := 1; -- Parameter inputs = 1
           outputs : positive := 1;-- Parameter outputs = 1
		   size : positive := inputs + outputs); -- Parameter inputs = $inputs + $outputs
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC); -- TCKPort
end component;

signal m6_so, m7_so : STD_LOGIC;

component A586710_Basic_M5 is
 Generic ( inputs : positive := 343; -- Parameter inputs = 343
           outputs : positive := 218; -- Parameter outputs = 218
		   bidirs : positive := 111); -- Parameter bidirs = 111
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs+bidirs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs+bidirs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs+bidirs-1 downto 0)); --DataOutPort
end component;

signal m5_so : STD_LOGIC;

component A586710_Basic_M1 is
 Generic ( inputs : positive := 437; -- Parameter inputs = 437
           outputs : positive := 370; -- Parameter outputs = 370
		   bidirs : positive := 2); -- Parameter bidirs = 2
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs+bidirs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs+bidirs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs+bidirs-1 downto 0)); --DataOutPort
end component;

signal m1_so : STD_LOGIC;

begin

SO <= sib1_so; -- Source sib1.SO

-- Level 1
sib0 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => SI, --InputPort SI = SI
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL,
               RST => RST,
               TCK => TCK,
               SO => sib0_so,
		         -- Scan Interface  host ----------------
               fromSO => M0_so, --InputPort fromSO = M0.SO
               toCE => sib0_toCE,
               toSE => sib0_toSE,
               toUE => sib0_toUE,
               toSEL => sib0_toSEL,
               toRST => sib0_toRST,
               toTCK => sib0_toTCK,
               toSI => sib0_toSI);

M0 : EmptyModule
 Generic map ( inputs => 31, -- Parameter inputs = 31
               outputs => 59, -- Parameter outputs = 59
			   bidirs => 111) -- Parameter bidirs = 111
    Port map ( -- Scan Interface scan_client ----------
	           SI => sib0_toSI, -- InputPort SI = sib0.toSI
               SO => M0_so,
               SEL => sib0_toSEL,
               ----------------------------------------		
               SE => sib0_toSE,
               CE => sib0_toCE,
               UE => sib0_toUE,
               RST => sib0_toRST,
               TCK => sib0_toTCK);
			   
sib1 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => sib0_so, --InputPort SI = sib0.SO
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL,
               RST => RST,
               TCK => TCK,
               SO => sib1_so,
		         -- Scan Interface  host ----------------
               fromSO => sib2_so, --InputPort fromSO = sib2.SO
               toCE => sib1_toCE,
               toSE => sib1_toSE,
               toUE => sib1_toUE,
               toSEL => sib1_toSEL,
               toRST => sib1_toRST,
               toTCK => sib1_toTCK,
               toSI => sib1_toSI);
			   
-- Level 2
sibM6 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => sib1_toSI, --InputPort SI = sib1.toSI
               CE => sib1_toCE,
               SE => sib1_toSE,
               UE => sib1_toUE,
               SEL => sib1_toSEL,
               RST => sib1_toRST,
               TCK => sib1_toTCK,
               SO => sibM6_so,
		         -- Scan Interface  host ----------------
               fromSO => m6_so, --InputPort fromSO = m6.SO
               toCE => sibM6_toCE,
               toSE => sibM6_toSE,
               toUE => sibM6_toUE,
               toSEL => sibM6_toSEL,
               toRST => sibM6_toRST,
               toTCK => sibM6_toTCK,
               toSI => sibM6_toSI);			   
			   
m6 : EmptyModule_NoBidirs
 Generic map ( inputs => 34, -- Parameter inputs = 34
               outputs => 35) -- Parameter outputs = 35
    Port map ( -- Scan Interface scan_client ----------
	           SI => sibM6_toSI, -- InputPort SI = sibM6.toSI
               SO => m6_so,
               SEL => sibM6_toSEL,
               ----------------------------------------		
               SE => sibM6_toSE,
               CE => sibM6_toCE,
               UE => sibM6_toUE,
               RST => sibM6_toRST,
               TCK => sibM6_toTCK);			   

sib2 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => sibM6_so, --InputPort SI = sibM6.SO
               CE => sib1_toCE,
               SE => sib1_toSE,
               UE => sib1_toUE,
               SEL => sib1_toSEL,
               RST => sib1_toRST,
               TCK => sib1_toTCK,
               SO => sib2_so,
		         -- Scan Interface  host ----------------
               fromSO => m1_so, --InputPort fromSO = m1.SO
               toCE => sib2_toCE,
               toSE => sib2_toSE,
               toUE => sib2_toUE,
               toSEL => sib2_toSEL,
               toRST => sib2_toRST,
               toTCK => sib2_toTCK,
               toSI => sib2_toSI);

-- Level 3 and lower
sibM7 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => sib2_toSI, --InputPort SI = sib2.toSI
               CE => sib2_toCE,
               SE => sib2_toSE,
               UE => sib2_toUE,
               SEL => sib2_toSEL,
               RST => sib2_toRST,
               TCK => sib2_toTCK,
               SO => sibM7_so,
		         -- Scan Interface  host ----------------
               fromSO => m7_so, --InputPort fromSO = m7.SO
               toCE => sibM7_toCE,
               toSE => sibM7_toSE,
               toUE => sibM7_toUE,
               toSEL => sibM7_toSEL,
               toRST => sibM7_toRST,
               toTCK => sibM7_toTCK,
               toSI => sibM7_toSI);				   

m7 : EmptyModule_NoBidirs
 Generic map ( inputs => 226, -- Parameter inputs = 226
               outputs => 100) -- Parameter outputs = 100
    Port map ( -- Scan Interface scan_client ----------
	           SI => sibM7_toSI, -- InputPort SI = sibM7.toSI
               SO => m7_so,
               SEL => sibM7_toSEL,
               ----------------------------------------		
               SE => sibM7_toSE,
               CE => sibM7_toCE,
               UE => sibM7_toUE,
               RST => sibM7_toRST,
               TCK => sibM7_toTCK);	

m5 : A586710_Basic_M5
    Port map ( -- Scan Interface scan_client ----------
	           SI => sibM7_so, -- InputPort SI = sibM7.SO
               SO => m5_so,
               SEL => sib2_toSEL,
               ----------------------------------------		
               SE => sib2_toSE,
               CE => sib2_toCE,
               UE => sib2_toUE,
               RST => sib2_toRST,
               TCK => sib2_toTCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);

m1 : A586710_Basic_M1
    Port map ( -- Scan Interface scan_client ----------
	           SI => m5_so, -- InputPort SI = m5.SO
               SO => m1_so,
               SEL => sib2_toSEL,
               ----------------------------------------		
               SE => sib2_toSE,
               CE => sib2_toCE,
               UE => sib2_toUE,
               RST => sib2_toRST,
               TCK => sib2_toTCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);

end TreeUnbalanced_arch;