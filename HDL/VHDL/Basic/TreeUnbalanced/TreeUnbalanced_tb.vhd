-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 08.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY TreeUnbalanced_tb IS
END TreeUnbalanced_tb;
 
ARCHITECTURE behavior OF TreeUnbalanced_tb IS 
 
    -- Declaration of TreeUnbalanced iJTAG benchmark
    COMPONENT TreeUnbalanced
    PORT(
         SI : IN  std_logic;
         CE : IN  std_logic;
         SE : IN  std_logic;
         UE : IN  std_logic;
         SEL : IN  std_logic;
         RST : IN  std_logic;
         TCK : IN  std_logic;
         SO : OUT  std_logic
        );
    END COMPONENT;

   -- iJTAG I/O signals
   signal SI : std_logic := '0';
   signal CE : std_logic := '0';
   signal SE : std_logic := '0';
   signal UE : std_logic := '0';
   signal SEL : std_logic := '0';
   signal RST : std_logic := '0';
   signal TCK : std_logic := '0';
   signal INSTR_RST : std_logic := '0';
   signal INSTR_CLK : std_logic := '0';	
   signal SO : std_logic;
	
	-- Testbench constants
	constant TCK_period : time := 20 ns;
	constant CLK_period : time := 20 ns;
	
	constant HALF_SEPARATOR : time := 2*TCK_period;
	constant FULL_SEPARATOR : time := 8*TCK_period;	

    -- Sets maximum history size for SO output	
	constant MAX_SCANCHAIN_LENGTH : positive := 8; -- Only TESTBYTE is going to be checked
	
	-- Testbench Functions
	
	-- Returns all zeroes std_logic_vector of specified size
   function all_zeroes (number_of_zeroes : in positive) return std_logic_vector is
	  variable zero_array : std_logic_vector(0 to number_of_zeroes-1);
   begin
	  for i in zero_array'range loop
       zero_array(i) := '0';
	  end loop;
	  return zero_array;
   end function all_zeroes;	
	
	-- Converts STD_LOGIC_VECTOR to STRING
   function slv_to_string (slv_data : in std_logic_vector) return string is
	  variable string_out : string(1 to slv_data'length);
   begin
	  for i in string_out'range loop
       string_out(i) := std_logic'image(slv_data(i-1))(2);
	  end loop;
	  return string_out;
   end function slv_to_string;	

   -- Testbench signals   
   signal so_value_history : std_logic_vector(0 to MAX_SCANCHAIN_LENGTH-1);
   signal halt_simulation : std_logic:='0';
	
BEGIN
 
   INSTR_CLK <= (not INSTR_CLK and not halt_simulation) after CLK_period/2;
   
	-- Instantiate the Unit Under Test (UUT)
   uut: TreeUnbalanced PORT MAP (
          SI => SI,
          CE => CE,
          SE => SE,
          UE => UE,
          SEL => SEL,
          RST => RST,
          TCK => TCK,
          SO => SO
        );
		  
   -- Scanchain SO value history	
	so_value_history <= so_value_history(1 to MAX_SCANCHAIN_LENGTH-1)&SO when (falling_edge(TCK) and SE = '1');

   -- Stimulus process
   stim_proc: process
	
	 -- Generate a number of TCK ticks
    procedure clock_tick (number_of_tick : in positive) is
    begin
	     for i in 1 to number_of_tick loop
	       TCK <= '1';
		    wait for TCK_period/2;
		    TCK <= '0';
		    wait for TCK_period/2;
		  end loop;
    end procedure clock_tick;
	 
	 -- Shifts in specified data (Capture -> Shift -> Update)
    procedure shift_data (data : in std_logic_vector) is
    begin
	     -- Capture phase
		  CE <= '1';
	     clock_tick(1);
		  CE <= '0';
		  -- Shift phase
        SE <= '1';
	     for i in data'range loop
			 SI <= data(i);
	       clock_tick(1);
		  end loop;
        SE <= '0';
		  -- Update phase
		  UE <= '1';
	     clock_tick(1);
		  UE <= '0';
    end procedure shift_data;
	 
	 -- Checks data from SO against provided expected data
    procedure data_check (expected_data : in std_logic_vector; report_type : string) is
	   variable so_data : std_logic_vector(0 to expected_data'length-1);
    begin
	   so_data := so_value_history(MAX_SCANCHAIN_LENGTH-expected_data'length to MAX_SCANCHAIN_LENGTH-1);
		if report_type = "report" then
		  if so_data = expected_data then
          report "TEST PASSED";
		  else
		    report "TEST FAILED"
		    severity ERROR;
		  end if;
		  report "Received SO = "&slv_to_string(so_data)&" (<-- SO)";	
		  report "Expected SO = "&slv_to_string(expected_data)&" (<-- SO)";	
		elsif report_type = "assert" then
		  assert (so_data = expected_data)
		  report "assert check FAILED"&LF&"Received SO = "&slv_to_string(so_data)&" (<-- SO)"&LF&"Expected SO = "&slv_to_string(expected_data)&" (<-- SO)"
		  severity ERROR;
		end if;
    end procedure data_check;	

	 variable TESTBYTE : std_logic_vector(0 to 7);	 
	 
   begin		
	   
		-- Set TESTBYTE value ---------------------------------------------------------------
		TESTBYTE := X"B3";
		
		-- Reset iJTAG chain and Instruments
	    RST <= '1';
        INSTR_RST <= '1';
		wait for TCK_period/2;
	    RST <= '0';
        INSTR_RST <= '0';
		-------------------------------------------------------------------------------------
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.0 ------------------------------------------------------------------------
		-- By default sib0 and sib1 are closed
		-- Scanchain is 2 bits long 
		-- SO <- sib1 <- sib0 <- SI
		-- Check that default scanchain does not work when SEL is deasserted
		
		-- Deassert SEL
		SEL <= '0';

		-- Shift in TESTBYTE + 2 zeroes to retain the scanchain
		shift_data(TESTBYTE&all_zeroes(2));
		
		-- Check that UNINITIALIZED has appeared from SO
		report "TEST No.0 is finished";
        data_check("UUUUUUUU","report");
		
		-- Assert SEL for other tests
		SEL <= '1';
	
		-- END of TEST No.0 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

        -- TEST No.1 ------------------------------------------------------------------------
		-- sib0 and sib1 are closed
		-- Scanchain is 2 bits long 
		-- SO <- sib1 <- sib0 <- SI
		-- Check the default scanchain

		-- Shift in TESTBYTE + 2 zeroes to retain the scanchain
		shift_data(TESTBYTE&all_zeroes(2));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.1 is finished";
        data_check(TESTBYTE,"report");
	
		-- END of TEST No.1 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

        -- TEST No.2 ------------------------------------------------------------------------
		-- sib0 and sib1 are closed
		-- Scanchain is 2 bits long 
		-- SO <- sib1 <- sib0 <- SI
		-- Reconfigure scanchain to include all scan registers	
		
        -- Open sib1 (set sib1 to value 1)
		shift_data("10");
		
		wait for HALF_SEPARATOR;
		
		-- sibM6 and sib2 are now included in the scanchain
		-- Scanchain is 4 bits long 
		-- SO <- sib1 <- sib2 <- sibM6 <- sib0 <- SI	
		
		-- Shift in TESTBYTE + "1100" to open sib2 (set sib2 to value 1)
		shift_data(TESTBYTE&"1100");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

		-- sibM7, m5|sibTop and m1|sibTop are included in the scanchain
		-- Scanchain is 7 bits long 
		-- SO <- sib1 <- sib2 <- m1:sibTop <- m5:sibTop <- sibM7 <- sibM6 <- sib0 <- SI	
		
		-- Shift in TESTBYTE + "1111000" to open m1|sibTop and m5|sibTop (set m1|sibTop and m5|sibTop to value 1)
		shift_data(TESTBYTE&"1111000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

		-- default scanchains of modules m5 and m1 are included in the scanchain
		-- Scanchain is 18 bits long 
		-- SO <- sib1 <- sib2 <-
		-- <- m1|sibTop <- m1|sib1 <- m1|sibWrp <-
		-- <- m5|sibTop <- m5|sib8 <- m5|sib7 <- m5|sib6 <- m5|sib5 <- m5|sib4 <- m5|sib3 <- m5|sib2 <- m5|sib1<- m5|sibWrp <-  
		-- <- sibM7 <- sibM6 <- sib0 <- SI	
		
		-- Shift in TESTBYTE + needed data to open m1|sib1 and m1|sibWrp (set m1|sib1 and m1|sibWrp to value 1)
		shift_data(TESTBYTE&
		           "11"&
				   "111"&
				   "1000000000"&
				   "000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

		-- m1|WrpReg, m1|sibM2, m1|sibM3, m1|sibM4, m1|sib2 and m1|SR1 are included in the scanchain
		-- Scanchain is 2990 bits long 
		-- SO <- sib1 <- sib2 <-
		-- <- m1|sibTop <- m1|sib1 <- m1|sib2 <- m1|SR1 <- m1|sibWrp <- m1|sibM4 <- m1|sibM3 <- m1|sibM2 <- m1|WrpReg <-   
		-- <- m5|sibTop <- m5|sib8 <- m5|sib7 <- m5|sib6 <- m5|sib5 <- m5|sib4 <- m5|sib3 <- m5|sib2 <- m5|sib1<- m5|sibWrp <-  
		-- <- sibM7 <- sibM6 <- sib0 <- SI	
		
		-- Shift in TESTBYTE + needed data to open m1|sib2 (set m1|sib2 to value 1)
		shift_data(TESTBYTE&
		           "11"&
				   "111"&all_zeroes(2155)&"1000"&all_zeroes(813)&
				   "1000000000"&
				   "000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

		-- m1|sib3 and m1|SR2 are included in the scanchain
		-- Scanchain is 5146 bits long 
		-- SO <- sib1 <- sib2 <-
		-- <- m1|sibTop <- m1|sib1 <- m1|sib2 <- m1|sib3 <- m1|(SR2+SR1) <- 
		-- <- m1|sibWrp <- m1|sibM4 <- m1|sibM3 <- m1|sibM2 <- m1|WrpReg <-   
		-- <- m5|sibTop <- m5|sib8 <- m5|sib7 <- m5|sib6 <- m5|sib5 <- m5|sib4 <- m5|sib3 <- m5|sib2 <- m5|sib1<- m5|sibWrp <-  
		-- <- sibM7 <- sibM6 <- sib0 <- SI	
		
		-- Shift in TESTBYTE + needed data to open m1|sib3 (set m1|sib3 to value 1)
		shift_data(TESTBYTE&
		           "11"&
				   "1111"&all_zeroes(2155+2155)&
				   "1000"&all_zeroes(813)&
				   "1000000000"&
				   "000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m1|sib4 and m1|SR3 are included in the scanchain
		-- Scanchain is 7288 bits long 
		-- SO <- sib1 <- sib2 <-
		-- <- m1|sibTop <- m1|sib1 <- m1|sib2 <- m1|sib3 <- m1|sib4 <- m1|(SR3+SR2+SR1) <- 
		-- <- m1|sibWrp <- m1|sibM4 <- m1|sibM3 <- m1|sibM2 <- m1|WrpReg <-   
		-- <- m5|sibTop <- m5|sib8 <- m5|sib7 <- m5|sib6 <- m5|sib5 <- m5|sib4 <- m5|sib3 <- m5|sib2 <- m5|sib1<- m5|sibWrp <-  
		-- <- sibM7 <- sibM6 <- sib0 <- SI	
		
		-- Shift in TESTBYTE + needed data to open m1|sib4 (set m1|sib4 to value 1)
		shift_data(TESTBYTE&
		           "11"&
				   "11111"&all_zeroes(2141+2155+2155)&
				   "1000"&all_zeroes(813)&
				   "1000000000"&
				   "000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m1|sib5 and m1|SR4 are included in the scanchain
		-- Scanchain is 9444 bits long 
		-- SO <- sib1 <- sib2 <-
		-- <- m1|sibTop <- m1|sib1 <- m1|sib2 <- m1|sib3 <- m1|sib4 <- m1|sib5 <- m1|(SR4+SR3+SR2+SR1) <- 
		-- <- m1|sibWrp <- m1|sibM4 <- m1|sibM3 <- m1|sibM2 <- m1|WrpReg <-   
		-- <- m5|sibTop <- m5|sib8 <- m5|sib7 <- m5|sib6 <- m5|sib5 <- m5|sib4 <- m5|sib3 <- m5|sib2 <- m5|sib1<- m5|sibWrp <-  
		-- <- sibM7 <- sibM6 <- sib0 <- SI	
		
		-- Shift in TESTBYTE + needed data to open m1|sib5 (set m1|sib5 to value 1)
		shift_data(TESTBYTE&
		           "11"&
				   "111111"&all_zeroes(2155+2141+2155+2155)&
				   "1000"&all_zeroes(813)&
				   "1000000000"&
				   "000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m1|sib6 and m1|SR5 are included in the scanchain
		-- Scanchain is 11600 bits long 
		-- SO <- sib1 <- sib2 <-
		-- <- m1|sibTop <- m1|sib1 <- m1|sib2 <- m1|sib3 <- m1|sib4 <- m1|sib5 <- m1|sib6 <- m1|(SR5+SR4+SR3+SR2+SR1) <- 
		-- <- m1|sibWrp <- m1|sibM4 <- m1|sibM3 <- m1|sibM2 <- m1|WrpReg <-   
		-- <- m5|sibTop <- m5|sib8 <- m5|sib7 <- m5|sib6 <- m5|sib5 <- m5|sib4 <- m5|sib3 <- m5|sib2 <- m5|sib1<- m5|sibWrp <-  
		-- <- sibM7 <- sibM6 <- sib0 <- SI	
		
		-- Shift in TESTBYTE + needed data to open m1|sib6 (set m1|sib6 to value 1)
		shift_data(TESTBYTE&
		           "11"&
				   "1111111"&all_zeroes(2155+2155+2141+2155+2155)&
				   "1000"&all_zeroes(813)&
				   "1000000000"&
				   "000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m1|sib7 and m1|SR6 are included in the scanchain
		-- Scanchain is 13756 bits long 
		-- SO <- sib1 <- sib2 <-
		-- <- m1|sibTop <- m1|sib1 <- m1|sib2 <- m1|sib3 <- m1|sib4 <- m1|sib5 <- m1|sib6 <- m1|sib7 <- m1|(SR6+SR5+SR4+SR3+SR2+SR1) <- 
		-- <- m1|sibWrp <- m1|sibM4 <- m1|sibM3 <- m1|sibM2 <- m1|WrpReg <-   
		-- <- m5|sibTop <- m5|sib8 <- m5|sib7 <- m5|sib6 <- m5|sib5 <- m5|sib4 <- m5|sib3 <- m5|sib2 <- m5|sib1<- m5|sibWrp <-  
		-- <- sibM7 <- sibM6 <- sib0 <- SI	
		
		-- Shift in TESTBYTE + needed data to open m1|sib7 (set m1|sib7 to value 1)
		shift_data(TESTBYTE&
		           "11"&
				   "11111111"&all_zeroes(2155+2155+2155+2141+2155+2155)&
				   "1000"&all_zeroes(813)&
				   "1000000000"&
				   "000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m1|sib8 and m1|SR7 are included in the scanchain
		-- Scanchain is 15912 bits long 
		-- SO <- sib1 <- sib2 <-
		-- <- m1|sibTop <- m1|sib1 <- m1|sib2 <- m1|sib3 <- m1|sib4 <- m1|sib5 <- m1|sib6 <- m1|sib7 <- m1|sib8 <- m1|(SR7+SR6+SR5+SR4+SR3+SR2+SR1) <- 
		-- <- m1|sibWrp <- m1|sibM4 <- m1|sibM3 <- m1|sibM2 <- m1|WrpReg <-   
		-- <- m5|sibTop <- m5|sib8 <- m5|sib7 <- m5|sib6 <- m5|sib5 <- m5|sib4 <- m5|sib3 <- m5|sib2 <- m5|sib1<- m5|sibWrp <-  
		-- <- sibM7 <- sibM6 <- sib0 <- SI	
		
		-- Shift in TESTBYTE + needed data to open all remaining SIBs
		shift_data(TESTBYTE&
		           "11"&
				   "111111111"&all_zeroes(2155+2155+2155+2155+2141+2155+2155)&
				   "1111"&all_zeroes(813)&
				   "1111111111"&
				   "111");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- all SIBs and scan registers are now included in the scanchain
		-- Scanchain is 41887 bits long 
		-- SO <- sib1 <- sib2 <-
		-- <- m1|sibTop <- m1|sib1 <- m1|sib2 <- m1|sib3 <- m1|sib4 <- m1|sib5 <- m1|sib6 <- m1|sib7 <- m1|sib8 <- m1|(SR8+SR7+SR6+SR5+SR4+SR3+SR2+SR1) <- 
		-- <- m1|sibWrp <- m1|sibM4 <- m1|m4|WrpReg <- m1|sibM3 <- m1|m3|WrpReg <- m1|sibM2 <- m1|m2|WrpReg <- m1|WrpReg <-   
		-- <- m5|sibTop <- m5|sib8 <- m5|SR8 <- m5|sib7 <- m5|SR7 <- m5|sib6 <- m5|SR6 <- m5|sib5 <- m5|SR5 <- m5|sib4 <- m5|SR4 <- m5|sib3 <- m5|SR3 <- m5|sib2 <- m5|SR2 <- m5|sib1 <- m5|SR1 <- m5|sibWrp <- m5|WrpReg <-  
		-- <- sibM7 <- m7|WrpReg <- sibM6 <- m6|WrpReg <- sib0 <- M0|WrpReg <- SI	
		
		-- Shift in TESTBYTE + needed data to retain the scanchain
		shift_data(TESTBYTE&
		           "11"&
				   "111111111"&all_zeroes(2155+2155+2155+2155+2155+2141+2155+2155)&
				   "11"&all_zeroes(530)&'1'&all_zeroes(651)&'1'&all_zeroes(497)&all_zeroes(813)&
				   "11"&all_zeroes(2626)&'1'&all_zeroes(2548)&'1'&all_zeroes(2532)&'1'&all_zeroes(2548)&'1'&all_zeroes(2540)&'1'&all_zeroes(2540)&'1'&all_zeroes(2548)&'1'&all_zeroes(2548)&'1'&all_zeroes(894)&
				   '1'&all_zeroes(326)&'1'&all_zeroes(69)&'1'&all_zeroes(423));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.2 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.2 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.3 ------------------------------------------------------------------------
		-- all SIBs and scan registers are included in the scanchain
		-- Scanchain is 41887 bits long 
		-- SO <- sib1 <- sib2 <-
		-- <- m1|sibTop <- m1|sib1 <- m1|sib2 <- m1|sib3 <- m1|sib4 <- m1|sib5 <- m1|sib6 <- m1|sib7 <- m1|sib8 <- m1|(SR8+SR7+SR6+SR5+SR4+SR3+SR2+SR1) <- 
		-- <- m1|sibWrp <- m1|sibM4 <- m1|m4|WrpReg <- m1|sibM3 <- m1|m3|WrpReg <- m1|sibM2 <- m1|m2|WrpReg <- m1|WrpReg <-   
		-- <- m5|sibTop <- m5|sib8 <- m5|SR8 <- m5|sib7 <- m5|SR7 <- m5|sib6 <- m5|SR6 <- m5|sib5 <- m5|SR5 <- m5|sib4 <- m5|SR4 <- m5|sib3 <- m5|SR3 <- m5|sib2 <- m5|SR2 <- m5|sib1 <- m5|SR1 <- m5|sibWrp <- m5|WrpReg <-  
		-- <- sibM7 <- m7|WrpReg <- sibM6 <- m6|WrpReg <- sib0 <- M0|WrpReg <- SI	
		-- Repeat the last shift in order to check that scanchain remained the same
		
		-- Shift in TESTBYTE + needed data to retain the scanchain
		shift_data(TESTBYTE&
		           "11"&
				   "111111111"&all_zeroes(2155+2155+2155+2155+2155+2141+2155+2155)&
				   "11"&all_zeroes(530)&'1'&all_zeroes(651)&'1'&all_zeroes(497)&all_zeroes(813)&
				   "11"&all_zeroes(2626)&'1'&all_zeroes(2548)&'1'&all_zeroes(2532)&'1'&all_zeroes(2548)&'1'&all_zeroes(2540)&'1'&all_zeroes(2540)&'1'&all_zeroes(2548)&'1'&all_zeroes(2548)&'1'&all_zeroes(894)&
				   '1'&all_zeroes(326)&'1'&all_zeroes(69)&'1'&all_zeroes(423));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.3 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.3 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
                
		-- STOP SIMULATION ------------------------------------------------------------------
        halt_simulation <= '1';
        wait;
        
   end process;

END;
