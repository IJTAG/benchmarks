-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 21.12.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TreeFlat_Ex is
    Port ( -- iJTAG interface
	       SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort	
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC); -- ScanOutPort
end TreeFlat_Ex;

architecture TreeFlat_Ex_arch of TreeFlat_Ex is

component SIB_mux_pre is
    Port ( -- Scan Interface  client --------------
	       SI : in  STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
		   -- Scan Interface  host ----------------
           fromSO : in  STD_LOGIC; -- ScanInPort
           toCE : out  STD_LOGIC; -- ToCaptureEnPort
           toSE : out  STD_LOGIC; -- ToShiftEnPort
           toUE : out  STD_LOGIC; -- ToUpdateEnPort
           toSEL : out  STD_LOGIC; -- ToSelectPort
           toRST : out  STD_LOGIC; -- ToResetPort
           toTCK : out  STD_LOGIC; -- ToTCKPort
           toSI : out  STD_LOGIC); -- ScanOutPort
end component;

signal sibM13_so, sibM14_so : std_logic;
signal sibM13_toCE, sibM14_toCE : std_logic;
signal sibM13_toSE, sibM14_toSE : std_logic;
signal sibM13_toUE, sibM14_toUE : std_logic;
signal sibM13_toSEL, sibM14_toSEL : std_logic;
signal sibM13_toRST, sibM14_toRST : std_logic;
signal sibM13_toTCK, sibM14_toTCK : std_logic;
signal sibM13_toSI, sibM14_toSI : std_logic;

component G1023_Basic_M1 is
 Generic ( inputs : positive := 139; -- Parameter inputs = 139
           outputs : positive := 273); -- Parameter outputs = 273
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m1_so: STD_LOGIC;

component G1023_Basic_M2 is
 Generic ( inputs : positive := 221; -- Parameter inputs = 221
           outputs : positive := 215); -- Parameter outputs = 215
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m2_so: STD_LOGIC;

component G1023_Basic_M3 is
 Generic ( inputs : positive := 192; -- Parameter inputs = 192
           outputs : positive := 171); -- Parameter outputs = 171
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m3_so: STD_LOGIC;

component G1023_Basic_M4 is
 Generic ( inputs : positive := 145; -- Parameter inputs = 145
           outputs : positive := 155); -- Parameter outputs = 155
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m4_so: STD_LOGIC;

component G1023_Basic_M5 is
 Generic ( inputs : positive := 32; -- Parameter inputs = 32
           outputs : positive := 27); -- Parameter outputs = 27
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m5_so: STD_LOGIC;

component G1023_Basic_M6 is
 Generic ( inputs : positive := 20; -- Parameter inputs = 20
           outputs : positive := 18); -- Parameter outputs = 18
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m6_so: STD_LOGIC;

component G1023_Basic_M7 is
 Generic ( inputs : positive := 20; -- Parameter inputs = 20
           outputs : positive := 18); -- Parameter outputs = 18
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m7_so: STD_LOGIC;

component G1023_Basic_M8 is
 Generic ( inputs : positive := 63; -- Parameter inputs = 63
           outputs : positive := 80); -- Parameter outputs = 80
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m8_so: STD_LOGIC;

component G1023_Basic_M9 is
 Generic ( inputs : positive := 56; -- Parameter inputs = 56
           outputs : positive := 34); -- Parameter outputs = 34
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m9_so: STD_LOGIC;

component G1023_Basic_M10 is
 Generic ( inputs : positive := 301; -- Parameter inputs = 301
           outputs : positive := 377); -- Parameter outputs = 377
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m10_so: STD_LOGIC;

component G1023_Basic_M11 is
 Generic ( inputs : positive := 145; -- Parameter inputs = 145
           outputs : positive := 191); -- Parameter outputs = 191
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m11_so: STD_LOGIC;

component G1023_Basic_M12 is
 Generic ( inputs : positive := 157; -- Parameter inputs = 157
           outputs : positive := 161); -- Parameter outputs = 161
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m12_so: STD_LOGIC;

component EmptyModule_NoBidirs is
 Generic ( inputs : positive := 1; -- Parameter inputs = 1
           outputs : positive := 1;-- Parameter outputs = 1
		   size : positive := inputs + outputs); -- Parameter inputs = $inputs + $outputs
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC); -- TCKPort
end component;

signal m13_so, m14_so: STD_LOGIC;

begin

SO <= sibM14_so; -- Source sibM14.SO

m1 : G1023_Basic_M1
    Port map ( -- Scan Interface scan_client ----------
	           SI => SI, --InputPort SI = SI
               SO => m1_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);

m2 : G1023_Basic_M2
    Port map ( -- Scan Interface scan_client ----------
	           SI => m1_so, --InputPort SI = m1.SO
               SO => m2_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);

m3 : G1023_Basic_M3
    Port map ( -- Scan Interface scan_client ----------
	           SI => m2_so, --InputPort SI = m2.SO
               SO => m3_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);

m4 : G1023_Basic_M4
    Port map ( -- Scan Interface scan_client ----------
	           SI => m3_so, --InputPort SI = m3.SO
               SO => m4_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);

m5 : G1023_Basic_M5
    Port map ( -- Scan Interface scan_client ----------
	           SI => m4_so, --InputPort SI = m4.SO
               SO => m5_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	

m6 : G1023_Basic_M6
    Port map ( -- Scan Interface scan_client ----------
	           SI => m5_so, --InputPort SI = m5.SO
               SO => m6_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	
			   
m7 : G1023_Basic_M7
    Port map ( -- Scan Interface scan_client ----------
	           SI => m6_so, --InputPort SI = m6.SO
               SO => m7_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	

m8 : G1023_Basic_M8
    Port map ( -- Scan Interface scan_client ----------
	           SI => m7_so, --InputPort SI = m7.SO
               SO => m8_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	

m9 : G1023_Basic_M9
    Port map ( -- Scan Interface scan_client ----------
	           SI => m8_so, --InputPort SI = m8.SO
               SO => m9_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	

m10 : G1023_Basic_M10
    Port map ( -- Scan Interface scan_client ----------
	           SI => m9_so, --InputPort SI = m9.SO
               SO => m10_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	

m11 : G1023_Basic_M11
    Port map ( -- Scan Interface scan_client ----------
	           SI => m10_so, --InputPort SI = m10.SO
               SO => m11_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	

m12 : G1023_Basic_M12
    Port map ( -- Scan Interface scan_client ----------
	           SI => m11_so, --InputPort SI = m11.SO
               SO => m12_so,
               SEL => SEL,
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	
			   
sibM13 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => m12_so, --InputPort SI = m12.SO
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL,
               RST => RST,
               TCK => TCK,
               SO => sibM13_so,
		         -- Scan Interface host ----------------
               fromSO => m13_so, --InputPort fromSO = m13.SO
               toCE => sibM13_toCE,
               toSE => sibM13_toSE,
               toUE => sibM13_toUE,
               toSEL => sibM13_toSEL,
               toRST => sibM13_toRST,
               toTCK => sibM13_toTCK,
               toSI => sibM13_toSI);	
			   
m13 : EmptyModule_NoBidirs
 Generic map ( inputs => 58, -- Parameter inputs = 58
               outputs => 64) -- Parameter outputs = 64
    Port map ( -- Scan Interface scan_client ----------
	           SI => sibM13_toSI, --InputPort SI = sibM13.toSI
               SO => m13_so,
               SEL => sibM13_toSEL,
               ----------------------------------------		
               SE => sibM13_toSE,
               CE => sibM13_toCE,
               UE => sibM13_toUE,
               RST => sibM13_toRST,
               TCK => sibM13_toTCK);	
			   
sibM14 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	           SI => sibM13_so, --InputPort SI = sibM13.SO
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL,
               RST => RST,
               TCK => TCK,
               SO => sibM14_so,
		         -- Scan Interface host ----------------
               fromSO => m14_so, --InputPort fromSO = m14.SO
               toCE => sibM14_toCE,
               toSE => sibM14_toSE,
               toUE => sibM14_toUE,
               toSEL => sibM14_toSEL,
               toRST => sibM14_toRST,
               toTCK => sibM14_toTCK,
               toSI => sibM14_toSI);	
			   
m14 : EmptyModule_NoBidirs
 Generic map ( inputs => 140, -- Parameter inputs = 140
               outputs => 114) -- Parameter outputs = 114
    Port map ( -- Scan Interface scan_client ----------
	           SI => sibM14_toSI, --InputPort SI = sibM14.toSI
               SO => m14_so,
               SEL => sibM14_toSEL,
               ----------------------------------------		
               SE => sibM14_toSE,
               CE => sibM14_toCE,
               UE => sibM14_toUE,
               RST => sibM14_toRST,
               TCK => sibM14_toTCK);	
			   
end TreeFlat_Ex_arch;