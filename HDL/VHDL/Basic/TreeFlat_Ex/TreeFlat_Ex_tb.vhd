-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 22.12.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY TreeFlat_Ex_tb IS
END TreeFlat_Ex_tb;
 
ARCHITECTURE behavior OF TreeFlat_Ex_tb IS 
 
    -- Declaration of TreeFlat_Ex iJTAG benchmark
    COMPONENT TreeFlat_Ex
    PORT(
         SI : IN  std_logic;
         CE : IN  std_logic;
         SE : IN  std_logic;
         UE : IN  std_logic;
         SEL : IN  std_logic;
         RST : IN  std_logic;
         TCK : IN  std_logic;
         SO : OUT  std_logic
        );
    END COMPONENT;

   -- iJTAG I/O signals
   signal SI : std_logic := '0';
   signal CE : std_logic := '0';
   signal SE : std_logic := '0';
   signal UE : std_logic := '0';
   signal SEL : std_logic := '0';
   signal RST : std_logic := '0';
   signal TCK : std_logic := '0';
   signal INSTR_RST : std_logic := '0';
   signal INSTR_CLK : std_logic := '0';	
   signal SO : std_logic;
	
	-- Testbench constants
	constant TCK_period : time := 20 ns;
	constant CLK_period : time := 20 ns;
	
	constant HALF_SEPARATOR : time := 2*TCK_period;
	constant FULL_SEPARATOR : time := 8*TCK_period;	

    -- Sets maximum history size for SO output	
	constant MAX_SCANCHAIN_LENGTH : positive := 8; -- Only TESTBYTE is going to be checked
	
	-- Testbench Functions
	
	-- Returns all zeroes std_logic_vector of specified size
   function all_zeroes (number_of_zeroes : in positive) return std_logic_vector is
	  variable zero_array : std_logic_vector(0 to number_of_zeroes-1);
   begin
	  for i in zero_array'range loop
       zero_array(i) := '0';
	  end loop;
	  return zero_array;
   end function all_zeroes;	
	
	-- Converts STD_LOGIC_VECTOR to STRING
   function slv_to_string (slv_data : in std_logic_vector) return string is
	  variable string_out : string(1 to slv_data'length);
   begin
	  for i in string_out'range loop
       string_out(i) := std_logic'image(slv_data(i-1))(2);
	  end loop;
	  return string_out;
   end function slv_to_string;	

   -- Testbench signals   
   signal so_value_history : std_logic_vector(0 to MAX_SCANCHAIN_LENGTH-1);
   signal halt_simulation : std_logic:='0';
	
BEGIN
 
   INSTR_CLK <= (not INSTR_CLK and not halt_simulation) after CLK_period/2;
   
	-- Instantiate the Unit Under Test (UUT)
   uut: TreeFlat_Ex PORT MAP (
          SI => SI,
          CE => CE,
          SE => SE,
          UE => UE,
          SEL => SEL,
          RST => RST,
          TCK => TCK,
          SO => SO
        );
		  
   -- Scanchain SO value history	
	so_value_history <= so_value_history(1 to MAX_SCANCHAIN_LENGTH-1)&SO when (falling_edge(TCK) and SE = '1');

   -- Stimulus process
   stim_proc: process
	
	 -- Generate a number of TCK ticks
    procedure clock_tick (number_of_tick : in positive) is
    begin
	     for i in 1 to number_of_tick loop
	       TCK <= '1';
		    wait for TCK_period/2;
		    TCK <= '0';
		    wait for TCK_period/2;
		  end loop;
    end procedure clock_tick;
	 
	 -- Shifts in specified data (Capture -> Shift -> Update)
    procedure shift_data (data : in std_logic_vector) is
    begin
	     -- Capture phase
		  CE <= '1';
	     clock_tick(1);
		  CE <= '0';
		  -- Shift phase
        SE <= '1';
	     for i in data'range loop
			 SI <= data(i);
	       clock_tick(1);
		  end loop;
        SE <= '0';
		  -- Update phase
		  UE <= '1';
	     clock_tick(1);
		  UE <= '0';
    end procedure shift_data;
	 
	 -- Checks data from SO against provided expected data
    procedure data_check (expected_data : in std_logic_vector; report_type : string) is
	   variable so_data : std_logic_vector(0 to expected_data'length-1);
    begin
	   so_data := so_value_history(MAX_SCANCHAIN_LENGTH-expected_data'length to MAX_SCANCHAIN_LENGTH-1);
		if report_type = "report" then
		  if so_data = expected_data then
          report "TEST PASSED";
		  else
		    report "TEST FAILED"
		    severity ERROR;
		  end if;
		  report "Received SO = "&slv_to_string(so_data)&" (<-- SO)";	
		  report "Expected SO = "&slv_to_string(expected_data)&" (<-- SO)";	
		elsif report_type = "assert" then
		  assert (so_data = expected_data)
		  report "assert check FAILED"&LF&"Received SO = "&slv_to_string(so_data)&" (<-- SO)"&LF&"Expected SO = "&slv_to_string(expected_data)&" (<-- SO)"
		  severity ERROR;
		end if;
    end procedure data_check;	

	 variable TESTBYTE : std_logic_vector(0 to 7);	 
	 
   begin		
	   
		-- Set TESTBYTE value ---------------------------------------------------------------
		TESTBYTE := X"B3";
		
		-- Reset iJTAG chain and Instruments
	    RST <= '1';
        INSTR_RST <= '1';
		wait for TCK_period/2;
	    RST <= '0';
        INSTR_RST <= '0';
		-------------------------------------------------------------------------------------
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.0 ------------------------------------------------------------------------
		-- By default all sibs are closed
		-- Scanchain is 14 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- 
		-- <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI
		-- Check that default scanchain does not work when SEL is deasserted
		
		-- Deassert SEL
		SEL <= '0';

		-- Shift in TESTBYTE + 14 zeroes to retain the scanchain
		shift_data(TESTBYTE&all_zeroes(14));
		
		-- Check that UNINITIALIZED has appeared from SO
		report "TEST No.0 is finished";
        data_check("UUUUUUUU","report");
		
		-- Assert SEL for other tests
		SEL <= '1';
	
		-- END of TEST No.0 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

        -- TEST No.1 ------------------------------------------------------------------------
		-- All sibs are closed
		-- Scanchain is 14 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- 
		-- <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI
		-- Check the default scanchain

		-- Shift in TESTBYTE + 14 zeroes to retain the scanchain
		shift_data(TESTBYTE&all_zeroes(14));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.1 is finished";
        data_check(TESTBYTE,"report");
	
		-- END of TEST No.1 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

        -- TEST No.2 ------------------------------------------------------------------------
		-- Scanchain is 14 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- 
		-- <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI
		-- Reconfigure scanchain to include each scan register in m5 finishing with SR1	
		
        -- Open m5|sibTop (set m5|sibTop to value 1)
		shift_data("00000000010000");
		
		wait for HALF_SEPARATOR;
		
		-- m5|sib1 and m5|sibWrp are now included in the scanchain
		-- Scanchain is 16 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|sibWrp <- 
		-- <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI
		
		-- Shift in TESTBYTE + needed data to open m5|sib1 (set m5|sib1 to value 1)
		shift_data(TESTBYTE&
		           "000000000"&
				   "110"&
				   "0000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m5|SCREG is included in the scanchain
		-- Scanchain is 19 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|sibWrp <- 
		-- <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI
		
		-- Shift in TESTBYTE + needed data to set m5|SCREG to "101" (select path "101" for m5|sMux)
		shift_data(TESTBYTE&
		           "000000000"&
				   "11"&"101"&'0'&
				   "0000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m5|SR4 is included in the scanchain
		-- Scanchain is 51 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR4 <- m5|sibWrp <- 
		-- <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI
		
		-- Shift in TESTBYTE + needed data to set m5|SCREG to "011" (select path "011" for m5|sMux)
		shift_data(TESTBYTE&
		           "000000000"&
				   "11"&"110"&all_zeroes(32)&'0'&
				   "0000");		
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	
		
		-- m5|SR3 is included in the scanchain
		-- Scanchain is 50 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR3 <- m5|sibWrp <- 
		-- <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI
		
		-- Shift in TESTBYTE + needed data to set m5|SCREG to "010" (select path "010" for m5|sMux)
		shift_data(TESTBYTE&
		           "000000000"&
				   "11"&"010"&all_zeroes(31)&'0'&
				   "0000");		
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	
		
		-- m5|SR2 is included in the scanchain
		-- Scanchain is 51 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR2 <- m5|sibWrp <- 
		-- <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI
		
		-- Shift in TESTBYTE + needed data to set m5|SCREG to "001" (select path "001" for m5|sMux)
		shift_data(TESTBYTE&
		           "000000000"&
				   "11"&"100"&all_zeroes(32)&'0'&
				   "0000");		
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	
		
		-- m5|SR1 is included in the scanchain
		-- Scanchain is 51 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR1 <- m5|sibWrp <- 
		-- <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI
		
		-- Shift in TESTBYTE + needed data to close m5|sibTop and m5|sib1 (set m5|sibTop and m5|sib1 to value 0)
		shift_data(TESTBYTE&
		           "000000000"&
				   "00"&"100"&all_zeroes(32)&'0'&
				   "0000");				
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m5 is excluded from the scanchain (only m5|sibTop remains)
		-- Scanchain is 14 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- 
		-- <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI

		-- Shift in TESTBYTE + needed data to retain the scanchain
		shift_data(TESTBYTE&"00000000000000");
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.2 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.2 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.3 ------------------------------------------------------------------------
		-- All sibs are closed
		-- Scanchain is 14 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- 
		-- <- m6|sibTop <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI
		-- Open the longest path in all modules

        -- Open m7|sibTop (set m7|sibTop to value 1)
		shift_data("00000001000000");
		
		wait for HALF_SEPARATOR;
				   
		-- m7|sib1 and m7|sibWrp are now adder to the scanchain
		-- Scanchain is 16 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- 
		-- <- m7|sibTop <- m7|sib1 <- m7|sibWrp <- 
		-- <- m6|sibTop <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI

		-- Shift in TESTBYTE + needed data to open m7|sib1 (set m7|sib1 to value 1)
		shift_data(TESTBYTE&
		           "0000000"&
				   "110"&
				   "000000");	
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;				   
				   
		-- m7|SCREG is now added to the scanchain
--		-- Scanchain is 18 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- 
		-- <- m7|sibTop <- m7|sib1 <- m7|SCREG <- m7|sibWrp <- 
		-- <- m6|sibTop <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI

		-- Shift in TESTBYTE + needed data to set m2|SCREG to "11" (to open path 1 for sMux1 and path 1 for sMux2)
		shift_data(TESTBYTE&
		           "0000000"&
				   "11"&"11"&'0'&
				   "000000");	
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;					   
				   
		-- m7|SR1 and m7|SR2 are now added to the scanchain
		-- Scanchain is 112 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- 
		-- <- m7|sibTop <- m7|sib1 <- m7|SCREG <- m7|(SR2+SR1) <- m7|sibWrp <- 
		-- <- m6|sibTop <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to close m7|sibTop and m7|sib1 (set m7|sibTop and m7|sib1 to value 0)
		shift_data(TESTBYTE&
		           "0000000"&
				   "00"&"11"&all_zeroes(47)&all_zeroes(47)&'0'&
				   "000000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

		-- m7 is now excluded from the scanchain (only m7|sibTop remains)
		-- Scanchain is 14 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- 
		-- <- m7|sibTop <- m6|sibTop <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open m8|sibTop (set m8|sibTop to value 1)
		shift_data(TESTBYTE&"00000010000000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m8|sib1 and m8|sibWrp are now added to the scanchain
		-- Scanchain is 16 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- 
		-- <- m8|sibTop <- m8|sib1 <- m8|sibWrp <-
		-- <- m7|sibTop <- m6|sibTop <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open m8|sib1 (set m8|sib1 to value 1)
		shift_data(TESTBYTE&
		           "000000"&
				   "110"&
				   "0000000");	
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;		
		
		-- m8|sib2 and m8|SR1 are now added to the scanchain
		-- Scanchain is 69 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- 
		-- <- m8|sibTop <- m8|sib1 <- m8|sib2 <- m8|SR1 <- m8|sibWrp <-
		-- <- m7|sibTop <- m6|sibTop <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open m8|sib2 (set m8|sib2 to value 1)
		shift_data(TESTBYTE&
		           "000000"&
				   "111"&all_zeroes(52)&'0'&
				   "0000000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;			

		-- m8|SR2 is now added to the scanchain
		-- Scanchain is 121 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- 
		-- <- m8|sibTop <- m8|sib1 <- m8|sib2 <- m8|(SR2+SR1) <- m8|sibWrp <-
		-- <- m7|sibTop <- m6|sibTop <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to close m8|sibTop and m8|sib1 (set m8|sibTop and m8|sib1 to value 0)
		shift_data(TESTBYTE&
		           "000000"&
				   "001"&all_zeroes(52)&all_zeroes(52)&'0'&
				   "0000000");		
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m8 is now excluded from the scanchain (only m8|sibTop remains)
		-- Scanchain is 14 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- m5|sibTop <- 
		-- <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open m4|sibTop (set m4|sibTop to value 1)
		shift_data(TESTBYTE&"00000000001000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m4|sib1 and m4|sibWrp are now added to the scanchain
		-- Scanchain is 16 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- m5|sibTop <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sibWrp <-
		-- <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open m4|sib1 (set m4|sib1 to value 1)
		shift_data(TESTBYTE&
		           "0000000000"&
				   "110"&
				   "000");	
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;			
		
		-- m4|SR1 and m4|sib2 are now added to the scanchain
		-- Scanchain is 71 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- m5|sibTop <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|SR1 <- m4|sibWrp <-
		-- <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open m4|sib2 (set m4|sib2 to value 1)
		shift_data(TESTBYTE&
		           "0000000000"&
				   "111"&all_zeroes(54)&'0'&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;			
		
		-- m4|SR2 and m4|sib3 are now added to the scanchain
		-- Scanchain is 126 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- m5|sibTop <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|sib3 <- m4|(SR2+SR1) <- m4|sibWrp <-
		-- <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open m4|sib3 (set m4|sib3 to value 1)
		shift_data(TESTBYTE&
		           "0000000000"&
				   "1111"&all_zeroes(54)&all_zeroes(54)&'0'&
				   "000");		
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;			
		
		-- m4|SR3 and m4|sib4 are now added to the scanchain
--		-- Scanchain is 181 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- m5|sibTop <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|sib3 <- m4|sib4 <- m4|(SR3+SR2+SR1) <- m4|sibWrp <-
		-- <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open m4|sib4 (set m4|sib4 to value 1)
		shift_data(TESTBYTE&
		           "0000000000"&
				   "11111"&all_zeroes(54)&all_zeroes(54)&all_zeroes(54)&'0'&
				   "000");			
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m4|SR4 is now added to the scanchain
		-- Scanchain is 235 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- m5|sibTop <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|sib3 <- m4|sib4 <- m4|(SR4+SR3+SR2+SR1) <- m4|sibWrp <-
		-- <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI			

		-- Shift in TESTBYTE + needed data to close m4|sibTop and m4|sib1 (set m4|sibTop and m4|sib1 to value 0)
		shift_data(TESTBYTE&
		           "0000000000"&
				   "00111"&all_zeroes(54)&all_zeroes(54)&all_zeroes(54)&all_zeroes(54)&'0'&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	
		
		-- m4 is now excluded from the scanchain (only m4|sibTop remains)
		-- Scanchain is 14 bits long 
		-- SO <- sibM14 <- sibM13 <- m12|sibTop <- m11|sibTop <- m10|sibTop <- m9|sibTop <- m8|sibTop <- m7|sibTop <- m6|sibTop <- 
		-- <- m5|sibTop <- m4|sibTop <- m3|sibTop <- m2|sibTop <- m1|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open sibTop-s for all modules (set sibTop-s for all modules to value 1)
		shift_data(TESTBYTE&"00111111111111");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;																				

		-- m1|sib14, m1|sib13, m1|sib12, m1|sib11, m1|sib10, m1|sib9, m1|sib8, 
		-- m1|sib7, m1|sib6, m1|sib5, m1|sib4, m1|sib3, m1|sib2, m1|sib1, m1|sibWrp,
		-- m2|sib2, m2|sib1, m2|sibWrp, m3|sib1, m3|sibWrp, m4|sib1, m4|sibWrp,
		-- m5|sib1, m5|sibWrp, m6|sib2, m6|sib1, m6|sibWrp, m7|sib1, m7|sibWrp,
		-- m8|sib1, m8|sibWrp, m9|sib1, m9|sibWrp, m10|sib1, m10|sibWrp,
		-- m11|sib1, m11|sibWrp, m12|sib1 and m12|sibWrp
		-- are now added to the scanchain
		-- Scanchain is 53 bits long 
		-- SO <- 
		-- <- sibM14 <- 
		-- <- sibM13 <- 
		-- <- m12|sibTop <- m12|sib1 <- m12|sibWrp <- 
		-- <- m11|sibTop <- m11|sib1 <- m11|sibWrp <- 
		-- <- m10|sibTop <- m10|sib1 <- m10|sibWrp <- 
		-- <- m9|sibTop <- m9|sib1 <- m9|sibWrp <- 
		-- <- m8|sibTop <- m8|sib1 <- m8|sibWrp <- 
		-- <- m7|sibTop <- m7|sib1 <- m7|sibWrp <- 
		-- <- m6|sibTop <- m6|sib2 <- m6|sib1 <- m6|sibWrp <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|sibWrp <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sibWrp <- 
		-- <- m3|sibTop <- m3|sib1 <- m3|sibWrp <- 
		-- <- m2|sibTop <- m2|sib2 <- m2|sib1 <- m2|sibWrp <- 
		-- <- m1|sibTop <- m1|sib14 <- m1|sib13 <- m1|sib12 <- m1|sib11 <- m1|sib10 <- m1|sib9 <- m1|sib8 <- 
		-- <- m1|sib7 <- m1|sib6 <- m1|sib5 <- m1|sib4 <- m1|sib3 <- m1|sib2 <- m1|sib1 <- m1|sibWrp <- 
		-- <- SI	

		-- Shift in TESTBYTE + needed data to open all remaining sibs (set all remaining sibs to value 1)
		shift_data(TESTBYTE&
				   '1'&
				   '1'&
				   "111"&
				   "111"&
				   "111"&
				   "111"&
				   "111"&
				   "111"&
				   "1111"&
				   "111"&
				   "111"&
				   "111"&
				   "1111"&
				   "11111111"&
				   "11111111");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m1|wrpOut, m1|wrpIn, m1|SR1, m1|SR2, m1|SR3, m1|SR4, m1|SR5, m1|SR6, m1|SR7, m1|SR8, m1|SR9, m1|SR10, m1|SR11, m1|SR12,
		-- m1|SR13, m1|SR14, m2|wrpOut, m2|wrpIn, m2|SR1, m2|SR2, m3|wrpOut, m3|wrpIn, m3|SR1, m4|wrpOut, m4|wrpIn, m4|SR1, m4|SR2,
		-- m4|SR3, m4|SR4, m4|sib2, m4|sib3, m4|sib4, m5|wrpOut, m5|wrpIn, m5|SCREG, m5|SR1, m6|wrpOut, m6|wrpIn, m6|SR1, m6|SR2,
		-- m7|wrpOut, m7|wrpIn, m7|SR1, m7|SR2, m7|SCREG, m8|wrpOut, m8|wrpIn, m8|SR1, m8|SR2, m8|sib2, m9|wrpOut, m9|wrpIn, m9|SR1,
		-- m10|wrpOut, m10|wrpIn, m10|SR1, m11|wrpOut, m11|wrpIn, m11|SR1, m12|wrpOut, m12|wrpIn, m12|SR1, m13|wrpOut, m13|wrpIn,
		-- m14|wrpOut, m14|wrpIn are now added to the scanchain
		-- Scanchain is 5100 bits long 
		-- SO <- 
		-- <- sibM14 <- m14|wrpOut <- m14|wrpIn <- 
		-- <- sibM13 <- m13|wrpOut <- m13|wrpIn <- 
		-- <- m12|sibTop <- m12|sib1 <- m12|SR1 <- m12|sibWrp <- m12|wrpOut <- m12|wrpIn <- 
		-- <- m11|sibTop <- m11|sib1 <- m11|SR1 <- m11|sibWrp <- m11|wrpOut <- m11|wrpIn <- 
		-- <- m10|sibTop <- m10|sib1 <- m10|SR1 <- m10|sibWrp <- m10|wrpOut <- m10|wrpIn <- 
		-- <- m9|sibTop <- m9|sib1 <- m9|SR1 <- m9|sibWrp <- m9|wrpOut <- m9|wrpIn <- 
		-- <- m8|sibTop <- m8|sib1 <- m8|sib2 <- m8|(SR2+SR1) <- m8|sibWrp <- m8|wrpOut <- m8|wrpIn <- 
		-- <- m7|sibTop <- m7|sib1 <- m7|SCREG <- m7|(SR2+SR1) <- m7|sibWrp <- m7|wrpOut <- m7|wrpIn <- 
		-- <- m6|sibTop <- m6|sib2 <- m6|SR2 <- m6|sib1 <- m6|SR1 <- m6|sibWrp <- m6|wrpOut <- m6|wrpIn <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR1 <- m5|sibWrp <- m5|wrpOut <- m5|wrpIn <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|sib3 <- m4|sib4 <- m4|(SR4+SR3+SR2+SR1) <- m4|sibWrp <- m4|wrpOut <- m4|wrpIn <- 
		-- <- m3|sibTop <- m3|sib1 <- m3|SR1 <- m3|sibWrp <- m3|wrpOut <- m3|wrpIn <- 
		-- <- m2|sibTop <- m2|sib2 <- m2|SR2 <- m2|sib1 <- m2|SR1 <- m2|sibWrp <- m2|wrpOut <- m2|wrpIn <- 
		-- <- m1|sibTop <- m1|sib14 <- m1|SR14 <- m1|sib13 <- m1|SR13 <- m1|sib12 <- m1|SR12 <- m1|sib11 <- m1|SR11 <- m1|sib10 <- 
		-- <- m1|SR10 <- m1|sib9 <- m1|SR9 <- m1|sib8 <- m1|SR8 <- m1|sib7 <- m1|SR7 <- m1|sib6 <- m1|SR6 <- m1|sib5 <- m1|SR5 <-
		-- <- m1|sib4 <- m1|SR4 <- m1|sib3 <- m1|SR3 <- m1|sib2 <- m1|SR2 <- m1|sib1 <- m1|SR1 <- m1|sibWrp <- m1|wrpOut <- m1|wrpIn <- 
		-- <- SI

		-- Shift in TESTBYTE + needed data to retain the scanchain
		shift_data(TESTBYTE&
				   '1'&all_zeroes(114)&all_zeroes(140)&
				   '1'&all_zeroes(64)&all_zeroes(58)&
				   "11"&all_zeroes(13)&'1'&all_zeroes(161)&all_zeroes(157)&
				   "11"&all_zeroes(9)&'1'&all_zeroes(191)&all_zeroes(145)&
				   "11"&all_zeroes(13)&'1'&all_zeroes(377)&all_zeroes(301)&
				   "11"&all_zeroes(64)&'1'&all_zeroes(34)&all_zeroes(56)&
				   "111"&all_zeroes(52)&all_zeroes(52)&'1'&all_zeroes(80)&all_zeroes(63)&
				   "11"&"11"&all_zeroes(47)&all_zeroes(47)&'1'&all_zeroes(18)&all_zeroes(20)&
				   "11"&all_zeroes(47)&'1'&all_zeroes(47)&'1'&all_zeroes(18)&all_zeroes(20)&
				   "11"&"100"&all_zeroes(32)&'1'&all_zeroes(27)&all_zeroes(32)&
				   "11111"&all_zeroes(54)&all_zeroes(54)&all_zeroes(54)&all_zeroes(54)&'1'&all_zeroes(155)&all_zeroes(145)&
				   "11"&all_zeroes(53)&'1'&all_zeroes(171)&all_zeroes(192)&
				   "11"&all_zeroes(83)&'1'&all_zeroes(84)&'1'&all_zeroes(215)&all_zeroes(221)&
				   "11"&all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&'1'&
				   all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&
				   '1'&all_zeroes(43)&'1'&all_zeroes(43)&'1'&all_zeroes(43)&'1'&all_zeroes(43)&'1'&all_zeroes(273)&all_zeroes(139));
				   				
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.3 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.3 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.4 ------------------------------------------------------------------------
		-- Scanchain is 5100 bits long 
		-- SO <- 
		-- <- sibM14 <- m14|wrpOut <- m14|wrpIn <- 
		-- <- sibM13 <- m13|wrpOut <- m13|wrpIn <- 
		-- <- m12|sibTop <- m12|sib1 <- m12|SR1 <- m12|sibWrp <- m12|wrpOut <- m12|wrpIn <- 
		-- <- m11|sibTop <- m11|sib1 <- m11|SR1 <- m11|sibWrp <- m11|wrpOut <- m11|wrpIn <- 
		-- <- m10|sibTop <- m10|sib1 <- m10|SR1 <- m10|sibWrp <- m10|wrpOut <- m10|wrpIn <- 
		-- <- m9|sibTop <- m9|sib1 <- m9|SR1 <- m9|sibWrp <- m9|wrpOut <- m9|wrpIn <- 
		-- <- m8|sibTop <- m8|sib1 <- m8|sib2 <- m8|(SR2+SR1) <- m8|sibWrp <- m8|wrpOut <- m8|wrpIn <- 
		-- <- m7|sibTop <- m7|sib1 <- m7|SCREG <- m7|(SR2+SR1) <- m7|sibWrp <- m7|wrpOut <- m7|wrpIn <- 
		-- <- m6|sibTop <- m6|sib2 <- m6|SR2 <- m6|sib1 <- m6|SR1 <- m6|sibWrp <- m6|wrpOut <- m6|wrpIn <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR1 <- m5|sibWrp <- m5|wrpOut <- m5|wrpIn <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|sib3 <- m4|sib4 <- m4|(SR4+SR3+SR2+SR1) <- m4|sibWrp <- m4|wrpOut <- m4|wrpIn <- 
		-- <- m3|sibTop <- m3|sib1 <- m3|SR1 <- m3|sibWrp <- m3|wrpOut <- m3|wrpIn <- 
		-- <- m2|sibTop <- m2|sib2 <- m2|SR2 <- m2|sib1 <- m2|SR1 <- m2|sibWrp <- m2|wrpOut <- m2|wrpIn <- 
		-- <- m1|sibTop <- m1|sib14 <- m1|SR14 <- m1|sib13 <- m1|SR13 <- m1|sib12 <- m1|SR12 <- m1|sib11 <- m1|SR11 <- m1|sib10 <- 
		-- <- m1|SR10 <- m1|sib9 <- m1|SR9 <- m1|sib8 <- m1|SR8 <- m1|sib7 <- m1|SR7 <- m1|sib6 <- m1|SR6 <- m1|sib5 <- m1|SR5 <-
		-- <- m1|sib4 <- m1|SR4 <- m1|sib3 <- m1|SR3 <- m1|sib2 <- m1|SR2 <- m1|sib1 <- m1|SR1 <- m1|sibWrp <- m1|wrpOut <- m1|wrpIn <- 
		-- <- SI
		-- Repeat the last shift in order to check that scanchain remained the same

		-- Shift in TESTBYTE + needed data to retain the scanchain
		shift_data(TESTBYTE&
				   '1'&all_zeroes(114)&all_zeroes(140)&
				   '1'&all_zeroes(64)&all_zeroes(58)&
				   "11"&all_zeroes(13)&'1'&all_zeroes(161)&all_zeroes(157)&
				   "11"&all_zeroes(9)&'1'&all_zeroes(191)&all_zeroes(145)&
				   "11"&all_zeroes(13)&'1'&all_zeroes(377)&all_zeroes(301)&
				   "11"&all_zeroes(64)&'1'&all_zeroes(34)&all_zeroes(56)&
				   "111"&all_zeroes(52)&all_zeroes(52)&'1'&all_zeroes(80)&all_zeroes(63)&
				   "11"&"11"&all_zeroes(47)&all_zeroes(47)&'1'&all_zeroes(18)&all_zeroes(20)&
				   "11"&all_zeroes(47)&'1'&all_zeroes(47)&'1'&all_zeroes(18)&all_zeroes(20)&
				   "11"&"100"&all_zeroes(32)&'1'&all_zeroes(27)&all_zeroes(32)&
				   "11111"&all_zeroes(54)&all_zeroes(54)&all_zeroes(54)&all_zeroes(54)&'1'&all_zeroes(155)&all_zeroes(145)&
				   "11"&all_zeroes(53)&'1'&all_zeroes(171)&all_zeroes(192)&
				   "11"&all_zeroes(83)&'1'&all_zeroes(84)&'1'&all_zeroes(215)&all_zeroes(221)&
				   "11"&all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&'1'&
				   all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&'1'&all_zeroes(42)&
				   '1'&all_zeroes(43)&'1'&all_zeroes(43)&'1'&all_zeroes(43)&'1'&all_zeroes(43)&'1'&all_zeroes(273)&all_zeroes(139));
				   				
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.4 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.4 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
		-- STOP SIMULATION ------------------------------------------------------------------
        halt_simulation <= '1';
        wait;
        
   end process;

END;
