-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 08.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------
--         1.1 | 21.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BasicSCB is
 Generic ( regSize : positive := 32;
           regSize1 : positive := regSize;
           regSize2 : positive := regSize;
           regSize3 : positive := regSize;
           regSize4 : positive := regSize;
           regSize5 : positive := regSize);
    Port ( -- iJTAG interface
	        SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort	
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC;
			  -- Instruments interface
			  INSTR_CLK : in STD_LOGIC; -- Instruments Clock
			  INSTR_RST : in STD_LOGIC); -- Instruments Reset
end BasicSCB;

architecture BasicSCB_arch of BasicSCB is

signal sel_SCB1_SCB3 : std_logic; --LogicSignal sel_SCB1_SCB3
signal sel_nSCB1_SCB3 : std_logic; --LogicSignal sel_nSCB1_SCB3
signal sel_nSCB3 : std_logic; --LogicSignal sel_nSCB3
signal sel_SCB2_SCB3 : std_logic; --LogicSignal sel_SCB2_SCB3
signal sel_SCB4_nSCB5_SCB6_nSCB7 : std_logic; --LogicSignal sel_SCB4_nSCB5_SCB6_nSCB7
signal sel_nSCB4_nSCB5_SCB6_nSCB7 : std_logic; --LogicSignal sel_nSCB4_nSCB5_SCB6_nSCB7
signal sel_nSCB6_nSCB7 : std_logic; --LogicSignal sel_nSCB6_nSCB7
signal sel_nSCB5_SCB6_nSCB7 : std_logic; --LogicSignal sel_nSCB5_SCB6_nSCB7
signal sel_SCB5_SCB6_nSCB7 : std_logic; --LogicSignal sel_SCB5_SCB6_nSCB7
signal sel_nSCB7 : std_logic; --LogicSignal sel_nSCB7
signal sel_nSCB2_SCB3 : std_logic; --LogicSignal sel_nSCB2_SCB3
signal sel_nSCB2_SCB3_SCB8_nSCB10 : std_logic; --LogicSignal sel_nSCB2_SCB3_SCB8_nSCB10
signal sel_nSCB2_nSCB8 : std_logic; --LogicSignal sel_nSCB2_nSCB8
signal sel_nSCB2_nSCB10_SCB3 : std_logic; --LogicSignal sel_nSCB2_nSCB10_SCB3
signal sel_nSCB2_SCB10_SCB9_SCB3 : std_logic; --LogicSignal sel_nSCB2_SCB10_SCB9_SCB3
signal sel_nSCB2_SCB10_nSCB9_SCB3 : std_logic; --LogicSignal sel_nSCB2_SCB10_nSCB9_SCB3

component WrappedInstr is
 Generic ( Size : positive := 8);
    Port ( -- Scan Interface scan_client ----------
	        SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
			  -- Instruments interface
			  INSTR_CLK : in STD_LOGIC; -- Instruments Clock
			  INSTR_RST : in STD_LOGIC); -- Instruments Reset
end component;

signal WI1_so, WI2_so, WI3_so, WI4_so, WI5_so : std_logic;

component BypassReg is
    Port ( -- Scan Interface  client --------------
	        SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC); -- ScanOutPort
           ----------------------------------------
end component;

signal Void1_so, Void2_so, Void3_so, Void4_so, Void5_so, Void6_so : std_logic; 

component SCB is
    Port ( -- Scan Interface  client --------------
	        SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
           ----------------------------------------
			  toSEL : out STD_LOGIC; -- ToSelectPort
			  DO : out STD_LOGIC_VECTOR (0 downto 0)); -- DataOutPort
end component;

signal SCB1_so, SCB2_so, SCB3_so, SCB4_so, SCB5_so, SCB6_so, SCB7_so, SCB8_so, SCB9_so, SCB10_so : std_logic;
signal SCB1_toSEL, SCB2_toSEL, SCB3_toSEL, SCB4_toSEL, SCB5_toSEL, SCB6_toSEL, SCB7_toSEL, SCB8_toSEL, SCB9_toSEL, SCB10_toSEL : std_logic;
signal SCB1_do, SCB2_do, SCB3_do, SCB4_do, SCB5_do, SCB6_do, SCB7_do, SCB8_do, SCB9_do, SCB10_do : std_logic_vector(0 downto 0);

component ScanMux is
 Generic (ControlSize : positive := 3);
    Port ( ScanMux_in : in STD_LOGIC_VECTOR((2**ControlSize)-1 downto 0);
           SelectedBy : in STD_LOGIC_VECTOR(ControlSize-1 downto 0);
           ScanMux_out : out STD_LOGIC);
end component;

signal sMux1_out, sMux2_out, sMux3_out, sMux4_out, sMux5_out, sMux6_out, sMux7_out, sMux8_out, sMux9_out, sMux10_out : std_logic; 

begin

SO <= SCB7_so; -- Source SCB7.SO

sel_SCB1_SCB3 <= SEL and SCB1_do(0) and SCB3_do(0); --LogicSignal sel_SCB1_SCB3
sel_nSCB1_SCB3 <= SEL and not SCB1_do(0) and SCB3_do(0); --LogicSignal sel_nSCB1_SCB3
sel_nSCB3 <= SEL and not SCB3_do(0); --LogicSignal sel_nSCB3
sel_SCB2_SCB3 <= SEL and SCB2_do(0) and SCB3_do(0); --LogicSignal sel_SCB2_SCB3
sel_SCB4_nSCB5_SCB6_nSCB7 <= SEL and SCB4_do(0) and not SCB5_do(0) and SCB6_do(0) and not SCB7_do(0); --LogicSignal sel_SCB4_nSCB5_SCB6_nSCB7
sel_nSCB4_nSCB5_SCB6_nSCB7 <= SEL and not SCB4_do(0) and not SCB5_do(0) and SCB6_do(0) and not SCB7_do(0); --LogicSignal sel_nSCB4_nSCB5_SCB6_nSCB7
sel_nSCB6_nSCB7 <= SEL and not SCB6_do(0) and not SCB7_do(0); --LogicSignal sel_nSCB6_nSCB7
sel_nSCB5_SCB6_nSCB7 <= SEL and not SCB5_do(0) and SCB6_do(0) and not SCB7_do(0); --LogicSignal sel_nSCB5_SCB6_nSCB7
sel_SCB5_SCB6_nSCB7 <= SEL and SCB5_do(0) and SCB6_do(0) and not SCB7_do(0); --LogicSignal sel_SCB5_SCB6_nSCB7
sel_nSCB7 <= SEL and not SCB7_do(0); --LogicSignal sel_nSCB7
sel_nSCB2_SCB3 <= SEL and not SCB2_do(0) and SCB3_do(0); --LogicSignal sel_nSCB2_SCB3
sel_nSCB2_SCB3_SCB8_nSCB10 <= SEL and not SCB2_do(0) and SCB3_do(0) and SCB8_do(0) and SCB10_do(0); --LogicSignal sel_nSCB2_SCB3_SCB8_nSCB10
sel_nSCB2_nSCB8 <= SEL and not SCB2_do(0) and not SCB8_do(0); --LogicSignal sel_nSCB2_nSCB8
sel_nSCB2_nSCB10_SCB3 <= SEL and not SCB2_do(0) and not SCB10_do(0) and SCB3_do(0); --LogicSignal sel_nSCB2_nSCB10_SCB3
sel_nSCB2_SCB10_SCB9_SCB3 <= SEL and not SCB2_do(0) and SCB10_do(0) and SCB9_do(0) and SCB3_do(0); --LogicSignal sel_nSCB2_SCB10_SCB9_SCB3
sel_nSCB2_SCB10_nSCB9_SCB3 <= SEL and not SCB2_do(0) and SCB10_do(0) and not SCB9_do(0) and SCB3_do(0); --LogicSignal sel_nSCB2_SCB10_nSCB9_SCB3

WI1 : WrappedInstr
 Generic map ( Size => regSize1) -- Parameter Size = $regSize1
    Port map ( -- Scan Interface scan_client ----------
	            SI => SI, --InputPort SI = SI
               SO => WI1_so,
               SEL => sel_SCB1_SCB3, -- InputPort SEL = sel_SCB1_SCB3
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
Void1 : BypassReg
    Port map ( -- Scan Interface  client --------------
	            SI => SI, -- InputPort SI = SI 
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB1_SCB3, -- InputPort SEL = sel_nSCB1_SCB3
               RST => RST,
               TCK => TCK,
               SO => Void1_so);
					
SCB1 : SCB
    Port map ( -- Scan Interface  client --------------
	            SI => SI, -- InputPort SI = SI
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB3, -- InputPort SEL = sel_nSCB3
               RST => RST,
               TCK => TCK,
               SO => SCB1_so,
               ----------------------------------------
			      toSEL => SCB1_toSEL,
			      DO => SCB1_do);
					
sMux1 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => Void1_so, -- 1'b0 : Void1.SO
	            ScanMux_in(1) => WI1_so, -- 1'b1 : WI1.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux1_out);
					
Void2 : BypassReg
    Port map ( -- Scan Interface  client --------------
	            SI => sMux1_out, -- InputPort SI = sMux1 
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_SCB2_SCB3, -- InputPort SEL = sel_SCB2_SCB3
               RST => RST,
               TCK => TCK,
               SO => Void2_so);
					
sMux2 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SCB10_so, -- 1'b0 : SCB10.SO
	            ScanMux_in(1) => Void2_so, -- 1'b1 : Void2.SO
               SelectedBy => SCB2_do, --SelectedBy SCB2.DO
               ScanMux_out => sMux2_out);
					
SCB2 : SCB
    Port map ( -- Scan Interface  client --------------
	            SI => SCB1_so, -- InputPort SI = SCB1.SO
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB3, -- InputPort SEL = sel_nSCB3
               RST => RST,
               TCK => TCK,
               SO => SCB2_so,
               ----------------------------------------
			      toSEL => SCB2_toSEL,
			      DO => SCB2_do);
					
sMux3 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SCB2_so, -- 1'b0 : SCB2.SO
	            ScanMux_in(1) => sMux2_out, -- 1'b1 : sMux2
               SelectedBy => SCB3_do, --SelectedBy SCB3.DO
               ScanMux_out => sMux3_out);
					
SCB3 : SCB
    Port map ( -- Scan Interface  client --------------
	            SI => sMux3_out, -- InputPort SI = sMux3
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL,
               RST => RST,
               TCK => TCK,
               SO => SCB3_so,
               ----------------------------------------
			      toSEL => SCB3_toSEL,
			      DO => SCB3_do);

sMux7 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SCB6_so, -- 1'b0 : SCB6.SO
	            ScanMux_in(1) => SCB3_so, -- 1'b1 : SCB3.SO
               SelectedBy => SCB7_do, --SelectedBy SCB7.DO
               ScanMux_out => sMux7_out);
					
SCB7 : SCB
    Port map ( -- Scan Interface  client --------------
	            SI => sMux7_out, -- InputPort SI = sMux7
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL,
               RST => RST,
               TCK => TCK,
               SO => SCB7_so,
               ----------------------------------------
			      toSEL => SCB7_toSEL,
			      DO => SCB7_do);
		
Void4 : BypassReg
    Port map ( -- Scan Interface  client --------------
	            SI => SCB3_so, -- InputPort SI = SCB3.SO 
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB4_nSCB5_SCB6_nSCB7, -- InputPort SEL = sel_nSCB4_nSCB5_SCB6_nSCB7
               RST => RST,
               TCK => TCK,
               SO => Void4_so);					
					
WI4 : WrappedInstr
 Generic map ( Size => regSize4) -- Parameter Size = $regSize4
    Port map ( -- Scan Interface scan_client ----------
	            SI => SCB3_so, --InputPort SI = SCB3.SO 
               SO => WI4_so,
               SEL => sel_SCB4_nSCB5_SCB6_nSCB7, -- InputPort SEL = sel_SCB4_nSCB5_SCB6_nSCB7
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);

SCB4 : SCB
    Port map ( -- Scan Interface  client --------------
	            SI => SCB3_so, -- InputPort SI = SCB3.SO
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB6_nSCB7, -- InputPort SEL = sel_nSCB6_nSCB7
               RST => RST,
               TCK => TCK,
               SO => SCB4_so,
               ----------------------------------------
			      toSEL => SCB4_toSEL,
			      DO => SCB4_do);

sMux4 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => Void4_so, -- 1'b1 : Void4.SO
	            ScanMux_in(1) => WI4_so, -- 1'b1 : WI4.SO
               SelectedBy => SCB4_do, --SelectedBy SCB4.DO
               ScanMux_out => sMux4_out);		

Void5 : BypassReg
    Port map ( -- Scan Interface  client --------------
	            SI => sMux4_out, -- InputPort SI = sMux4 
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB5_SCB6_nSCB7, -- InputPort SEL = sel_nSCB5_SCB6_nSCB7
               RST => RST,
               TCK => TCK,
               SO => Void5_so);	

WI5 : WrappedInstr
 Generic map ( Size => regSize5) -- Parameter Size = $regSize5
    Port map ( -- Scan Interface scan_client ----------
	            SI => SCB3_so, --InputPort SI = SCB3.SO 
               SO => WI5_so,
               SEL => sel_SCB5_SCB6_nSCB7, -- InputPort SEL = sel_SCB5_SCB6_nSCB7
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);	

SCB5 : SCB
    Port map ( -- Scan Interface  client --------------
	            SI => SCB4_so, -- InputPort SI = SCB4.SO
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB6_nSCB7, -- InputPort SEL = sel_nSCB6_nSCB7
               RST => RST,
               TCK => TCK,
               SO => SCB5_so,
               ----------------------------------------
			      toSEL => SCB5_toSEL,
			      DO => SCB5_do);

sMux5 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => Void5_so, -- 1'b1 : Void5.SO
	            ScanMux_in(1) => WI5_so, -- 1'b1 : WI5.SO
               SelectedBy => SCB5_do, --SelectedBy SCB5.DO
               ScanMux_out => sMux5_out);

sMux6 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SCB5_so, -- 1'b1 : SCB5.SO
	            ScanMux_in(1) => sMux5_out, -- 1'b1 : sMux5
               SelectedBy => SCB6_do, --SelectedBy SCB6.DO
               ScanMux_out => sMux6_out);	

SCB6 : SCB
    Port map ( -- Scan Interface  client --------------
	            SI => sMux6_out, -- InputPort SI = sMux6
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB7, -- InputPort SEL = sel_nSCB7
               RST => RST,
               TCK => TCK,
               SO => SCB6_so,
               ----------------------------------------
			      toSEL => SCB6_toSEL,
			      DO => SCB6_do);

WI2 : WrappedInstr
 Generic map ( Size => regSize2) -- Parameter Size = $regSize2
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux1_out, --InputPort SI = sMux1 
               SO => WI2_so,
               SEL => sel_nSCB2_SCB3_SCB8_nSCB10, -- InputPort SEL = sel_nSCB2_SCB3_SCB8_nSCB10
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);

Void3 : BypassReg
    Port map ( -- Scan Interface  client --------------
	            SI => sMux1_out, -- InputPort SI = sMux1 
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB2_nSCB8, -- InputPort SEL = sel_nSCB2_nSCB8
               RST => RST,
               TCK => TCK,
               SO => Void3_so);	

SCB8 : SCB
    Port map ( -- Scan Interface  client --------------
	            SI => sMux1_out, -- InputPort SI = sMux1
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB2_nSCB10_SCB3, -- InputPort SEL = sel_nSCB2_nSCB10_SCB3
               RST => RST,
               TCK => TCK,
               SO => SCB8_so,
               ----------------------------------------
			      toSEL => SCB8_toSEL,
			      DO => SCB8_do);

sMux8 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => Void3_so, -- 1'b1 : Void3.SO
	            ScanMux_in(1) => WI2_so, -- 1'b1 : WI2.SO
               SelectedBy => SCB8_do, --SelectedBy SCB8.DO
               ScanMux_out => sMux8_out);	

WI3 : WrappedInstr
 Generic map ( Size => regSize3) -- Parameter Size = $regSize3
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux8_out, --InputPort SI = sMux8 
               SO => WI3_so,
               SEL => sel_nSCB2_SCB10_SCB9_SCB3, -- InputPort SEL = sel_nSCB2_SCB10_SCB9_SCB3
               ----------------------------------------		
               SE => SE,
               CE => CE,
               UE => UE,
               RST => RST,
               TCK => TCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);

Void6 : BypassReg
    Port map ( -- Scan Interface  client --------------
	            SI => sMux8_out, -- InputPort SI = sMux8 
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB2_SCB10_nSCB9_SCB3, -- InputPort SEL = sel_nSCB2_SCB10_nSCB9_SCB3
               RST => RST,
               TCK => TCK,
               SO => Void6_so);	
					
SCB9 : SCB
    Port map ( -- Scan Interface  client --------------
	            SI => SCB8_so, -- InputPort SI = SCB8.SO
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB2_nSCB10_SCB3, -- InputPort SEL = sel_nSCB2_nSCB10_SCB3
               RST => RST,
               TCK => TCK,
               SO => SCB9_so,
               ----------------------------------------
			      toSEL => SCB9_toSEL,
			      DO => SCB9_do);
					
sMux9 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => Void6_so, -- 1'b1 : Void6.SO
	            ScanMux_in(1) => WI3_so, -- 1'b1 : WI3.SO
               SelectedBy => SCB9_do, --SelectedBy SCB9.DO
               ScanMux_out => sMux9_out);	
					
sMux10 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SCB9_so, -- 1'b1 : SCB9.SO
	            ScanMux_in(1) => sMux9_out, -- 1'b1 : sMux9
               SelectedBy => SCB10_do, --SelectedBy SCB10.DO
               ScanMux_out => sMux10_out);	
										
SCB10 : SCB
    Port map ( -- Scan Interface  client --------------
	            SI => sMux10_out, -- InputPort SI = sMux10
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_nSCB2_SCB3, -- InputPort SEL = sel_nSCB2_SCB3
               RST => RST,
               TCK => TCK,
               SO => SCB10_so,
               ----------------------------------------
			      toSEL => SCB10_toSEL,
			      DO => SCB10_do);					

end BasicSCB_arch;