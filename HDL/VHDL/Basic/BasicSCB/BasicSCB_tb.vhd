-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 08.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY BasicSCB_tb IS
END BasicSCB_tb;
 
ARCHITECTURE behavior OF BasicSCB_tb IS 
 
    -- Declaration of BasicSCB iJTAG benchmark
    COMPONENT BasicSCB
    PORT(
         SI : IN  std_logic;
         CE : IN  std_logic;
         SE : IN  std_logic;
         UE : IN  std_logic;
         SEL : IN  std_logic;
         RST : IN  std_logic;
         TCK : IN  std_logic;
         SO : OUT  std_logic;
			INSTR_CLK : IN  std_logic;
			INSTR_RST : IN  std_logic
        );
    END COMPONENT;

   -- iJTAG I/O signals
   signal SI : std_logic := '0';
   signal CE : std_logic := '0';
   signal SE : std_logic := '0';
   signal UE : std_logic := '0';
   signal SEL : std_logic := '0';
   signal RST : std_logic := '0';
   signal TCK : std_logic := '0';
   signal INSTR_RST : std_logic := '0';
   signal INSTR_CLK : std_logic := '0';	
   signal SO : std_logic;
	
	-- Testbench constants
	constant TCK_period : time := 20 ns;
	constant CLK_period : time := 20 ns;
	
	constant HALF_SEPARATOR : time := 2*TCK_period;
	constant FULL_SEPARATOR : time := 8*TCK_period;	
	
	constant MAX_SCANCHAIN_LENGTH : positive := 4*32+5;
	
	-- Testbench Functions
	
	-- Returns all zeroes std_logic_vector of specified size
   function all_zeroes (number_of_zeroes : in positive) return std_logic_vector is
	  variable zero_array : std_logic_vector(0 to number_of_zeroes-1);
   begin
	  for i in zero_array'range loop
       zero_array(i) := '0';
	  end loop;
	  return zero_array;
   end function all_zeroes;	
	
	-- Converts STD_LOGIC_VECTOR to STRING
   function slv_to_string (slv_data : in std_logic_vector) return string is
	  variable string_out : string(1 to slv_data'length);
   begin
	  for i in string_out'range loop
       string_out(i) := std_logic'image(slv_data(i-1))(2);
	  end loop;
	  return string_out;
   end function slv_to_string;	

   -- Testbench signals   
	
   signal so_value_history : std_logic_vector(0 to MAX_SCANCHAIN_LENGTH-1);
	
BEGIN
 
   INSTR_CLK <= not INSTR_CLK after CLK_period/2;
   
	-- Instantiate the Unit Under Test (UUT)
   uut: BasicSCB PORT MAP (
          SI => SI,
          CE => CE,
          SE => SE,
          UE => UE,
          SEL => SEL,
          RST => RST,
          TCK => TCK,
          SO => SO,
			 INSTR_CLK => INSTR_CLK,
			 INSTR_RST => INSTR_RST
        );
		  
   -- Scanchain SO value history	
	so_value_history <= so_value_history(1 to MAX_SCANCHAIN_LENGTH-1)&SO when (falling_edge(TCK) and SE = '1');

   -- Stimulus process
   stim_proc: process
	
	 -- Generate a number of TCK ticks
    procedure clock_tick (number_of_tick : in positive) is
    begin
	     for i in 1 to number_of_tick loop
	       TCK <= '1';
		    wait for TCK_period/2;
		    TCK <= '0';
		    wait for TCK_period/2;
		  end loop;
    end procedure clock_tick;
	 
	 -- Shifts in specified data (Capture -> Shift -> Update)
    procedure shift_data (data : in std_logic_vector) is
    begin
	     -- Capture phase
		  CE <= '1';
	     clock_tick(1);
		  CE <= '0';
		  -- Shift phase
        SE <= '1';
	     for i in data'range loop
			 SI <= data(i);
	       clock_tick(1);
		  end loop;
        SE <= '0';
		  -- Update phase
		  UE <= '1';
	     clock_tick(1);
		  UE <= '0';
    end procedure shift_data;
	 
	 -- Checks data from SO against provided expected data
    procedure data_check (expected_data : in std_logic_vector; report_type : string) is
	   variable so_data : std_logic_vector(0 to expected_data'length-1);
    begin
	   so_data := so_value_history(MAX_SCANCHAIN_LENGTH-expected_data'length to MAX_SCANCHAIN_LENGTH-1);
		if report_type = "report" then
		  if so_data = expected_data then
          report "TEST PASSED";
		  else
		    report "TEST FAILED"
		    severity ERROR;
		  end if;
		  report "Received SO = "&slv_to_string(so_data)&" (<-- SO)";	
		  report "Expected SO = "&slv_to_string(expected_data)&" (<-- SO)";	
		elsif report_type = "assert" then
		  assert (so_data = expected_data)
		  report "assert check FAILED"&LF&"Received SO = "&slv_to_string(so_data)&" (<-- SO)"&LF&"Expected SO = "&slv_to_string(expected_data)&" (<-- SO)"
		  severity ERROR;
		end if;
    end procedure data_check;	

	 variable TESTBYTE : std_logic_vector(0 to 7);	 
	 
   begin		
	   
		-- Set TESTBYTE value ---------------------------------------------------------------
		TESTBYTE := X"B3";
		
		-- Reset iJTAG chain and Instruments
	   RST <= '1';
      INSTR_RST <= '1';
		wait for TCK_period/2;
	   RST <= '0';
      INSTR_RST <= '0';
		-------------------------------------------------------------------------------------
		
		wait for FULL_SEPARATOR;
		
      -- TEST No.0 ------------------------------------------------------------------------
		-- N2 part is selected by default
		-- Scanchain is 7 bits long 
		-- SO <- SCB7 <- SCB6 <- SCB5 <- SCB4 <- SCB3 <- SCB2 <- SCB1 <- SI
		-- Check that default scanchain does not work when SEL is deasserted
		
		-- Deassert SEL
		SEL <= '0';

		-- Shift in TESTBYTE	+ 7 zeroes to retain the scanpath
		shift_data(TESTBYTE&all_zeroes(7));
		
		-- Check that UNINITIALIZED has appeared from SO
		report "TEST No.0 is finished";
      data_check("UUUUUUUU","report");
		
		-- Assert SEL for other tests
		SEL <= '1';
	
		-- END of TEST No.0 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

      -- TEST No.1 ------------------------------------------------------------------------
		-- N2 part is selected by default
		-- Scanchain is 7 bits long 
		-- SO <- SCB7 <- SCB6 <- SCB5 <- SCB4 <- SCB3 <- SCB2 <- SCB1 <- SI
		-- Check thet default scanchain

		-- Shift in TESTBYTE	+ 7 zeroes to retain the scanpath
		shift_data(TESTBYTE&all_zeroes(7));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.1 is finished";
      data_check(TESTBYTE,"report");
	
		-- END of TEST No.1 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

      -- TEST No.2 ------------------------------------------------------------------------
		-- N2 part is selected
		-- Scanchain is 7 bits long 
		-- SO <- SCB7 <- SCB6 <- SCB5 <- SCB4 <- SCB3 <- SCB2 <- SCB1 <- SI
		-- Reconfigure scanpath to deselect part N2 and select part N1	
		
      -- Set SCB7 and SCB3 to value 1 (select Path 1 for sMux7 and sMux3)
		shift_data("1000100");
		
		wait for HALF_SEPARATOR;
		
		-- N1 part is now selected
		-- Scanchain is 6 bits long 
		-- SO <- SCB7 <- SCB3 <- SCB10 <- SCB9 <- SCB8 <- Void1 <- SI	
		
		-- Shift in TESTBYTE	+ "110000" to retain the scanpath
		shift_data(TESTBYTE&"110000");
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.2 is finished";
      data_check(TESTBYTE,"report");
		
		-- END of TEST No.2 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;

      -- TEST No.3 ------------------------------------------------------------------------
		-- N1 part is selected
		-- Scanchain is 6 bits long 
		-- SO <- SCB7 <- SCB3 <- SCB10 <- SCB9 <- SCB8 <- Void1 <- SI	
		-- Reconfigure scanpath to select part N2 again while leaving part N1 selected	
		
      -- Set SCB7 to value 0 and set SCB3 to value 1 again 
		-- (switch back to Path 0 for sMux7 ant retain Path 1 for sMux3)
		shift_data("010000");
		
		wait for HALF_SEPARATOR;
		
		-- N1 and N2 parts are now selected
		-- Scanchain is 9 bits long 
		-- SO <- SCB7 <- SCB6 <- SCB5 <- SCB4 <- SCB3 <- SCB10 <- SCB9 <- SCB8 <- Void1 <- SI			
		
		-- Shift in TESTBYTE	+ "000010000" to retain the scanpath	
		shift_data(TESTBYTE&"000010000");
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.3 is finished";
      data_check(TESTBYTE,"report");
		
		-- END of TEST No.3 -----------------------------------------------------------------

		wait for FULL_SEPARATOR;
		
      -- TEST No.4 ------------------------------------------------------------------------
		-- N1 and N2 parts are selected
		-- Scanchain is 9 bits long 
		-- SO <- SCB7 <- SCB6 <- SCB5 <- SCB4 <- SCB3 <- SCB10 <- SCB9 <- SCB8 <- Void1 <- SI		
		-- Reconfigure scanchain to the longest length 
		
      -- Set SCB3 to value 0 to include SCB1 into scanchain (switch sMux3 to Path 0)
		shift_data("000000000");
		
		wait for HALF_SEPARATOR;
		
		-- The default scanchain is now selected
		-- Scanchain is 7 bits long 
		-- SO <- SCB7 <- SCB6 <- SCB5 <- SCB4 <- SCB3 <- SCB2 <- SCB1 <- SI
		
		-- Shift in TESTBYTE	+ 7 zeroes to retain the scanpath
		shift_data(TESTBYTE&all_zeroes(7));
		
		-- Check that TESTBYTE has appeared from SO
      data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
      -- Set SCB1 to value 1 to include WI1 into scanchain (switch sMux1 to Path 1)
      -- Set SCB3 to value 1 to include part N1 into scanchain (switch sMux3 to Path 1)
		shift_data("0000101");
		
		wait for HALF_SEPARATOR;
		
		-- Instrument WI1 and part N1 are now selected
		-- Scanchain is 40 bits long 
		-- SO <- SCB7 <- SCB6 <- SCB5 <- SCB4 <- SCB3 <- SCB10 <- SCB9 <- SCB8 <- WI1 <- SI
		
		-- Shift in TESTBYTE	+ "00001000" + 32 zeroes to retain the scanpath
		shift_data(TESTBYTE&"00001000"&all_zeroes(32));
		
		-- Check that TESTBYTE has appeared from SO
      data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
      -- Set SCB8 to value 1 to include WI2 into scanchain (switch sMux8 to Path 1)
      -- Set SCB9 to value 1 to include WI3 into scanchain (switch sMux9 to Path 1)
      -- Set SCB10 to value 1 to add WI2 and WI3 to scanchain (switch sMux10 to Path 1)
      -- Set SCB3 to value 1 to retain part N1 in the scanchain
      -- Set SCB4 to value 1 to include WI4 into scanchain (switch sMux4 to Path 1)
      -- Set SCB6 to value 1 to add WI4 to the scanchain (switch sMux6 to Path 1)
		shift_data("01011111"&all_zeroes(32));
		
		wait for HALF_SEPARATOR;
		
		-- Instruments WI2, WI3, WI4 and bypass register Void5 are now selected
		-- Scanchain is 133 bits long 
		-- SO <- SCB7 <- SCB6 <- Void5 <- WI4 <- SCB3 <- SCB10 <- WI3 <- WI2 <- WI1 <- SI
		
		-- Shift in TESTBYTE	+ "010" + 32 zeroes + "11" + 96 zeroes to retain the scanpath
		shift_data(TESTBYTE&"010"&all_zeroes(32)&"11"&all_zeroes(32)&all_zeroes(32)&all_zeroes(32));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.4 is finished";
      data_check(TESTBYTE,"report");
		
		-- END of TEST No.4 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;
		
      -- TEST No.5 ------------------------------------------------------------------------
		-- The longest scanchain is selected
		-- Scanchain is 133 bits long 
		-- SO <- SCB7 <- SCB6 <- Void5 <- WI4 <- SCB3 <- SCB10 <- WI3 <- WI2 <- WI1 <- SI	
		-- Write data to all instruments and read it back 

      -- Load all 4s into WI4 data register and all 1s into WI1 data register	
		-- Shift in "010" + 0x44444445 + "11" + 64 zeroes + 0x11111111 to retain the scanpath
		shift_data("010"&X"44444445"&"11"&all_zeroes(32)&all_zeroes(32)&X"11111111");

		wait for HALF_SEPARATOR;

      -- Load all 3s into WI3 data register and all 2s into WI2 data register	
      -- Load all 0s into WI4 and WI1 scan registers to stop writing to their data registers
		-- Shift in "010" + 0x33333333 + "11" + 64 zeroes + 0x22222223 to retain the scanpath
		shift_data("010"&all_zeroes(32)&"11"&X"33333333"&X"22222223"&all_zeroes(32));

		wait for HALF_SEPARATOR;
		
      -- Load all 0s into WI3 and WI2 scan registers to stop writing to their data registers
		-- Shift in "010" + 32 zeroes + "11" + 96 zeroes to retain the scanpath
		shift_data("010"&all_zeroes(32)&"11"&all_zeroes(32)&all_zeroes(32)&all_zeroes(32));
		
		wait for HALF_SEPARATOR;
		
      -- Load all 0s into WI4, WI3, WI2 and WI1 scan registers
		-- Shift in "010" + 32 zeroes + "11" + 96 zeroes to retain the scanpath
		shift_data("010"&all_zeroes(32)&"11"&all_zeroes(32)&all_zeroes(32)&all_zeroes(32));
				
		-- Check that written content of WI1, WI2, WI3 and WI4 data registers has appeared from SO
		report "TEST No.5 is finished";
      data_check("010"&X"44444445"&"11"&X"33333333"&X"22222223"&X"11111111","report");
		
		-- END of TEST No.5 -----------------------------------------------------------------
	
      wait;
   end process;

END;
