-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 08.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------
--         1.1 | 21.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TreeFlat is
 Generic ( regSize : positive := 8);
    Port ( -- iJTAG interface
	        SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort	
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC;
			  -- Instruments interface
			  INSTR_CLK : in STD_LOGIC; -- Instruments Clock
			  INSTR_RST : in STD_LOGIC); -- Instruments Reset
end TreeFlat;

architecture TreeFlat_arch of TreeFlat is

signal sel_SIB_main : std_logic; --LogicSignal sel_SIM_main
signal sel_WI0 : std_logic; --LogicSignal sel_WI0
signal sel_WI1 : std_logic; --LogicSignal sel_WI1
signal sel_WI2 : std_logic; --LogicSignal sel_WI2
signal sel_WI3 : std_logic; --LogicSignal sel_WI3
signal sel_WI4 : std_logic; --LogicSignal sel_WI4
signal sel_WI5 : std_logic; --LogicSignal sel_WI5
signal sel_WI6 : std_logic; --LogicSignal sel_WI6
signal sel_WI7 : std_logic; --LogicSignal sel_WI7
signal sel_WI8 : std_logic; --LogicSignal sel_WI8
signal sel_WI9 : std_logic; --LogicSignal sel_WI9
signal sel_WI10 : std_logic; --LogicSignal sel_WI10

component WrappedInstr is
 Generic ( Size : positive);
    Port ( -- Scan Interface scan_client ----------
	        SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
			  -- Instruments interface
			  INSTR_CLK : in STD_LOGIC; -- Instruments Clock
			  INSTR_RST : in STD_LOGIC); -- Instruments Reset
end component;

signal WI_0_so, WI_1_so, WI_2_so, WI_3_so, WI_4_so, WI_5_so, WI_6_so, WI_7_so, WI_8_so, WI_9_so, WI_10_so  : std_logic;

component SCB is
    Port ( -- Scan Interface  client --------------
	        SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
           ----------------------------------------
			  toSEL : out STD_LOGIC; -- ToSelectPort
			  DO : out STD_LOGIC_VECTOR (0 downto 0)); -- DataOutPort
end component;

signal SCB1_so : std_logic;
signal SCB1_toSEL : std_logic;
signal SCB1_do : std_logic_vector(0 downto 0);

component ScanMux is
 Generic (ControlSize : positive);
    Port ( ScanMux_in : in STD_LOGIC_VECTOR((2**ControlSize)-1 downto 0);
           SelectedBy : in STD_LOGIC_VECTOR(ControlSize-1 downto 0);
           ScanMux_out : out STD_LOGIC);
end component;

signal sMuxSO_out, sMux0_out, sMux1_out, sMux2_out, sMux3_out, sMux4_out, sMux5_out, sMux6_out, sMux7_out, sMux8_out, sMux9_out, sMux10_out : std_logic; 

component SIB_mux_pre is
    Port ( -- Scan Interface  client --------------
	       SI : in  STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
		   -- Scan Interface  host ----------------
           fromSO : in  STD_LOGIC; -- ScanInPort
           toCE : out  STD_LOGIC; -- ToCaptureEnPort
           toSE : out  STD_LOGIC; -- ToShiftEnPort
           toUE : out  STD_LOGIC; -- ToUpdateEnPort
           toSEL : out  STD_LOGIC; -- ToSelectPort
           toRST : out  STD_LOGIC; -- ToResetPort
           toTCK : out  STD_LOGIC; -- ToTCKPort
           toSI : out  STD_LOGIC); -- ScanOutPort
end component;

signal SIB_main_so, SIB0_so, SIB1_so, SIB2_so, SIB3_so, SIB4_so, SIB5_so, SIB6_so, SIB7_so, SIB8_so, SIB9_so, SIB10_so : std_logic;
signal SIB_main_toCE, SIB0_toCE, SIB1_toCE, SIB2_toCE, SIB3_toCE, SIB4_toCE, SIB5_toCE, SIB6_toCE, SIB7_toCE, SIB8_toCE, SIB9_toCE, SIB10_toCE : std_logic;
signal SIB_main_toSE, SIB0_toSE, SIB1_toSE, SIB2_toSE, SIB3_toSE, SIB4_toSE, SIB5_toSE, SIB6_toSE, SIB7_toSE, SIB8_toSE, SIB9_toSE, SIB10_toSE : std_logic;
signal SIB_main_toUE, SIB0_toUE, SIB1_toUE, SIB2_toUE, SIB3_toUE, SIB4_toUE, SIB5_toUE, SIB6_toUE, SIB7_toUE, SIB8_toUE, SIB9_toUE, SIB10_toUE : std_logic;
signal SIB_main_toSEL, SIB0_toSEL, SIB1_toSEL, SIB2_toSEL, SIB3_toSEL, SIB4_toSEL, SIB5_toSEL, SIB6_toSEL, SIB7_toSEL, SIB8_toSEL, SIB9_toSEL, SIB10_toSEL : std_logic;
signal SIB_main_toRST, SIB0_toRST, SIB1_toRST, SIB2_toRST, SIB3_toRST, SIB4_toRST, SIB5_toRST, SIB6_toRST, SIB7_toRST, SIB8_toRST, SIB9_toRST, SIB10_toRST : std_logic;
signal SIB_main_toTCK, SIB0_toTCK, SIB1_toTCK, SIB2_toTCK, SIB3_toTCK, SIB4_toTCK, SIB5_toTCK, SIB6_toTCK, SIB7_toTCK, SIB8_toTCK, SIB9_toTCK, SIB10_toTCK : std_logic;
signal SIB_main_toSI, SIB0_toSI, SIB1_toSI, SIB2_toSI, SIB3_toSI, SIB4_toSI, SIB5_toSI, SIB6_toSI, SIB7_toSI, SIB8_toSI, SIB9_toSI, SIB10_toSI : std_logic;

begin

SO <= SCB1_so; -- Source SCB1.SO

SIB_main : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	            SI => SI, --InputPort SI = SI
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => sel_SIB_main,
               RST => RST,
               TCK => TCK,
               SO => SIB_main_so,
		         -- Scan Interface  host ----------------
               fromSO => SIB10_so, --InputPort fromSO = SIB10.SO
               toCE => SIB_main_toCE,
               toSE => SIB_main_toSE,
               toUE => SIB_main_toUE,
               toSEL => SIB_main_toSEL,
               toRST => SIB_main_toRST,
               toTCK => SIB_main_toTCK,
               toSI => SIB_main_toSI);
					
SCB1 : SCB
    Port map ( -- Scan Interface  client --------------
	            SI => sMuxSO_out, -- InputPort SI = sMuxSO
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL,
               RST => RST,
               TCK => TCK,
               SO => SCB1_so,
               ----------------------------------------
			      toSEL => SCB1_toSEL,
			      DO => SCB1_do);
					
sMuxSO : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB_main_so, -- 1'b0 : SIB_main.SO
	            ScanMux_in(1) => WI_10_so, -- 1'b1 : WI_10.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMuxSO_out);

sel_SIB_main <= not SCB1_do(0) and SEL; --LogicSignal sel_SIB_main
sel_WI0 <= (SCB1_do(0) and SEL) or SIB0_toSEL; --LogicSignal sel_WI0
sel_WI1 <= (SCB1_do(0) and SEL) or SIB1_toSEL; --LogicSignal sel_WI1
sel_WI2 <= (SCB1_do(0) and SEL) or SIB2_toSEL; --LogicSignal sel_WI2
sel_WI3 <= (SCB1_do(0) and SEL) or SIB3_toSEL; --LogicSignal sel_WI3
sel_WI4 <= (SCB1_do(0) and SEL) or SIB4_toSEL; --LogicSignal sel_WI4
sel_WI5 <= (SCB1_do(0) and SEL) or SIB5_toSEL; --LogicSignal sel_WI5
sel_WI6 <= (SCB1_do(0) and SEL) or SIB6_toSEL; --LogicSignal sel_WI6
sel_WI7 <= (SCB1_do(0) and SEL) or SIB7_toSEL; --LogicSignal sel_WI7
sel_WI8 <= (SCB1_do(0) and SEL) or SIB8_toSEL; --LogicSignal sel_WI8
sel_WI9 <= (SCB1_do(0) and SEL) or SIB9_toSEL; --LogicSignal sel_WI9
sel_WI10 <= (SCB1_do(0) and SEL) or SIB10_toSEL; --LogicSignal sel_WI10

-- 0 bit
SIB0 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	            SI => SIB_main_toSI, --InputPort SI = SIB_main.toSI
               CE => SIB_main_toCE,
               SE => SIB_main_toSE,
               UE => SIB_main_toUE,
               SEL => SIB_main_toSEL,
               RST => SIB_main_toRST,
               TCK => SIB_main_toTCK,
               SO => SIB0_so,
		         -- Scan Interface host ----------------
               fromSO => WI_0_so, --InputPort fromSO = WI_0.SO
               toCE => SIB0_toCE,
               toSE => SIB0_toSE,
               toUE => SIB0_toUE,
               toSEL => SIB0_toSEL,
               toRST => SIB0_toRST,
               toTCK => SIB0_toTCK,
               toSI => SIB0_toSI);

WI_0 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux0_out, --InputPort SI = sMux0
               SO => WI_0_so,
               SEL => sel_WI0, -- InputPort SEL = sel_WI0
               ----------------------------------------		
               SE => SIB0_toSE,
               CE => SIB0_toCE,
               UE => SIB0_toUE,
               RST => SIB0_toRST,
               TCK => SIB0_toTCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
					
sMux0 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB0_toSI, -- 1'b0 : SIB0.toSI
	            ScanMux_in(1) => SI, -- 1'b1 : SI
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux0_out);
					
-- 1 bit
SIB1 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	            SI => SIB0_so, --InputPort SI = SIB0.SO
               CE => SIB_main_toCE,
               SE => SIB_main_toSE,
               UE => SIB_main_toUE,
               SEL => SIB_main_toSEL,
               RST => SIB_main_toRST,
               TCK => SIB_main_toTCK,
               SO => SIB1_so,
		         -- Scan Interface host ----------------
               fromSO => WI_1_so, --InputPort fromSO = WI_1.SO
               toCE => SIB1_toCE,
               toSE => SIB1_toSE,
               toUE => SIB1_toUE,
               toSEL => SIB1_toSEL,
               toRST => SIB1_toRST,
               toTCK => SIB1_toTCK,
               toSI => SIB1_toSI);

WI_1 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux1_out, --InputPort SI = sMux1
               SO => WI_1_so,
               SEL => sel_WI1, -- InputPort SEL = sel_WI1
               ----------------------------------------		
               SE => SIB1_toSE,
               CE => SIB1_toCE,
               UE => SIB1_toUE,
               RST => SIB1_toRST,
               TCK => SIB1_toTCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
					
sMux1 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB1_toSI, -- 1'b0 : SIB1.toSI
	            ScanMux_in(1) => WI_0_so, -- 1'b1 : WI_0.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux1_out);
					
-- 2 bit
SIB2 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	            SI => SIB1_so, --InputPort SI = SIB1.SO
               CE => SIB_main_toCE,
               SE => SIB_main_toSE,
               UE => SIB_main_toUE,
               SEL => SIB_main_toSEL,
               RST => SIB_main_toRST,
               TCK => SIB_main_toTCK,
               SO => SIB2_so,
		         -- Scan Interface host ----------------
               fromSO => WI_2_so, --InputPort fromSO = WI_2.SO
               toCE => SIB2_toCE,
               toSE => SIB2_toSE,
               toUE => SIB2_toUE,
               toSEL => SIB2_toSEL,
               toRST => SIB2_toRST,
               toTCK => SIB2_toTCK,
               toSI => SIB2_toSI);

WI_2 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux2_out, --InputPort SI = sMux2
               SO => WI_2_so,
               SEL => sel_WI2, -- InputPort SEL = sel_WI2
               ----------------------------------------		
               SE => SIB2_toSE,
               CE => SIB2_toCE,
               UE => SIB2_toUE,
               RST => SIB2_toRST,
               TCK => SIB2_toTCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
					
sMux2 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB2_toSI, -- 1'b0 : SIB2.toSI
	            ScanMux_in(1) => WI_1_so, -- 1'b1 : WI_1.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux2_out);

-- 3 bit
SIB3 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	            SI => SIB2_so, --InputPort SI = SIB2.SO
               CE => SIB_main_toCE,
               SE => SIB_main_toSE,
               UE => SIB_main_toUE,
               SEL => SIB_main_toSEL,
               RST => SIB_main_toRST,
               TCK => SIB_main_toTCK,
               SO => SIB3_so,
		         -- Scan Interface host ----------------
               fromSO => WI_3_so, --InputPort fromSO = WI_3.SO
               toCE => SIB3_toCE,
               toSE => SIB3_toSE,
               toUE => SIB3_toUE,
               toSEL => SIB3_toSEL,
               toRST => SIB3_toRST,
               toTCK => SIB3_toTCK,
               toSI => SIB3_toSI);

WI_3 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux3_out, --InputPort SI = sMux3
               SO => WI_3_so,
               SEL => sel_WI3, -- InputPort SEL = sel_WI3
               ----------------------------------------		
               SE => SIB3_toSE,
               CE => SIB3_toCE,
               UE => SIB3_toUE,
               RST => SIB3_toRST,
               TCK => SIB3_toTCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
					
sMux3 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB3_toSI, -- 1'b0 : SIB3.toSI
	            ScanMux_in(1) => WI_2_so, -- 1'b1 : WI_2.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux3_out);
					
-- 4 bit
SIB4 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	            SI => SIB3_so, --InputPort SI = SIB3.SO
               CE => SIB_main_toCE,
               SE => SIB_main_toSE,
               UE => SIB_main_toUE,
               SEL => SIB_main_toSEL,
               RST => SIB_main_toRST,
               TCK => SIB_main_toTCK,
               SO => SIB4_so,
		         -- Scan Interface host ----------------
               fromSO => WI_4_so, --InputPort fromSO = WI_4.SO
               toCE => SIB4_toCE,
               toSE => SIB4_toSE,
               toUE => SIB4_toUE,
               toSEL => SIB4_toSEL,
               toRST => SIB4_toRST,
               toTCK => SIB4_toTCK,
               toSI => SIB4_toSI);

WI_4 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux4_out, --InputPort SI = sMux4
               SO => WI_4_so,
               SEL => sel_WI4, -- InputPort SEL = sel_WI4
               ----------------------------------------		
               SE => SIB4_toSE,
               CE => SIB4_toCE,
               UE => SIB4_toUE,
               RST => SIB4_toRST,
               TCK => SIB4_toTCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
					
sMux4 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB4_toSI, -- 1'b0 : SIB4.toSI
	            ScanMux_in(1) => WI_3_so, -- 1'b1 : WI_3.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux4_out);
					
-- 5 bit
SIB5 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	            SI => SIB4_so, --InputPort SI = SIB4.SO
               CE => SIB_main_toCE,
               SE => SIB_main_toSE,
               UE => SIB_main_toUE,
               SEL => SIB_main_toSEL,
               RST => SIB_main_toRST,
               TCK => SIB_main_toTCK,
               SO => SIB5_so,
		         -- Scan Interface host ----------------
               fromSO => WI_5_so, --InputPort fromSO = WI_5.SO
               toCE => SIB5_toCE,
               toSE => SIB5_toSE,
               toUE => SIB5_toUE,
               toSEL => SIB5_toSEL,
               toRST => SIB5_toRST,
               toTCK => SIB5_toTCK,
               toSI => SIB5_toSI);

WI_5 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux5_out, --InputPort SI = sMux5
               SO => WI_5_so,
               SEL => sel_WI5, -- InputPort SEL = sel_WI5
               ----------------------------------------		
               SE => SIB5_toSE,
               CE => SIB5_toCE,
               UE => SIB5_toUE,
               RST => SIB5_toRST,
               TCK => SIB5_toTCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
					
sMux5 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB5_toSI, -- 1'b0 : SIB5.toSI
	            ScanMux_in(1) => WI_4_so, -- 1'b1 : WI_4.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux5_out);
					
-- 6 bit
SIB6 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	            SI => SIB5_so, --InputPort SI = SIB5.SO
               CE => SIB_main_toCE,
               SE => SIB_main_toSE,
               UE => SIB_main_toUE,
               SEL => SIB_main_toSEL,
               RST => SIB_main_toRST,
               TCK => SIB_main_toTCK,
               SO => SIB6_so,
		         -- Scan Interface host ----------------
               fromSO => WI_6_so, --InputPort fromSO = WI_6.SO
               toCE => SIB6_toCE,
               toSE => SIB6_toSE,
               toUE => SIB6_toUE,
               toSEL => SIB6_toSEL,
               toRST => SIB6_toRST,
               toTCK => SIB6_toTCK,
               toSI => SIB6_toSI);

WI_6 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux6_out, --InputPort SI = sMux6
               SO => WI_6_so,
               SEL => sel_WI6, -- InputPort SEL = sel_WI6
               ----------------------------------------		
               SE => SIB6_toSE,
               CE => SIB6_toCE,
               UE => SIB6_toUE,
               RST => SIB6_toRST,
               TCK => SIB6_toTCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
					
sMux6 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB6_toSI, -- 1'b0 : SIB6.toSI
	            ScanMux_in(1) => WI_5_so, -- 1'b1 : WI_5.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux6_out);

-- 7 bit
SIB7 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	            SI => SIB6_so, --InputPort SI = SIB6.SO
               CE => SIB_main_toCE,
               SE => SIB_main_toSE,
               UE => SIB_main_toUE,
               SEL => SIB_main_toSEL,
               RST => SIB_main_toRST,
               TCK => SIB_main_toTCK,
               SO => SIB7_so,
		         -- Scan Interface host ----------------
               fromSO => WI_7_so, --InputPort fromSO = WI_7.SO
               toCE => SIB7_toCE,
               toSE => SIB7_toSE,
               toUE => SIB7_toUE,
               toSEL => SIB7_toSEL,
               toRST => SIB7_toRST,
               toTCK => SIB7_toTCK,
               toSI => SIB7_toSI);

WI_7 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux7_out, --InputPort SI = sMux7
               SO => WI_7_so,
               SEL => sel_WI7, -- InputPort SEL = sel_WI7
               ----------------------------------------		
               SE => SIB7_toSE,
               CE => SIB7_toCE,
               UE => SIB7_toUE,
               RST => SIB7_toRST,
               TCK => SIB7_toTCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
					
sMux7 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB7_toSI, -- 1'b0 : SIB7.toSI
	            ScanMux_in(1) => WI_6_so, -- 1'b1 : WI_6.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux7_out);
					
-- 8 bit
SIB8 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	            SI => SIB7_so, --InputPort SI = SIB7.SO
               CE => SIB_main_toCE,
               SE => SIB_main_toSE,
               UE => SIB_main_toUE,
               SEL => SIB_main_toSEL,
               RST => SIB_main_toRST,
               TCK => SIB_main_toTCK,
               SO => SIB8_so,
		         -- Scan Interface host ----------------
               fromSO => WI_8_so, --InputPort fromSO = WI_8.SO
               toCE => SIB8_toCE,
               toSE => SIB8_toSE,
               toUE => SIB8_toUE,
               toSEL => SIB8_toSEL,
               toRST => SIB8_toRST,
               toTCK => SIB8_toTCK,
               toSI => SIB8_toSI);

WI_8 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux8_out, --InputPort SI = sMux8
               SO => WI_8_so,
               SEL => sel_WI8, -- InputPort SEL = sel_WI8
               ----------------------------------------		
               SE => SIB8_toSE,
               CE => SIB8_toCE,
               UE => SIB8_toUE,
               RST => SIB8_toRST,
               TCK => SIB8_toTCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
					
sMux8 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB8_toSI, -- 1'b0 : SIB8.toSI
	            ScanMux_in(1) => WI_7_so, -- 1'b1 : WI_7.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux8_out);
					
-- 9 bit
SIB9 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	            SI => SIB8_so, --InputPort SI = SIB8.SO
               CE => SIB_main_toCE,
               SE => SIB_main_toSE,
               UE => SIB_main_toUE,
               SEL => SIB_main_toSEL,
               RST => SIB_main_toRST,
               TCK => SIB_main_toTCK,
               SO => SIB9_so,
		         -- Scan Interface host ----------------
               fromSO => WI_9_so, --InputPort fromSO = WI_9.SO
               toCE => SIB9_toCE,
               toSE => SIB9_toSE,
               toUE => SIB9_toUE,
               toSEL => SIB9_toSEL,
               toRST => SIB9_toRST,
               toTCK => SIB9_toTCK,
               toSI => SIB9_toSI);

WI_9 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux9_out, --InputPort SI = sMux9
               SO => WI_9_so,
               SEL => sel_WI9, -- InputPort SEL = sel_WI9
               ----------------------------------------		
               SE => SIB9_toSE,
               CE => SIB9_toCE,
               UE => SIB9_toUE,
               RST => SIB9_toRST,
               TCK => SIB9_toTCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
					
sMux9 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB9_toSI, -- 1'b0 : SIB9.toSI
	            ScanMux_in(1) => WI_8_so, -- 1'b1 : WI_8.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux9_out);
					
-- 10 bit
SIB10 : SIB_mux_pre
    Port map ( -- Scan Interface client --------------
	            SI => SIB9_so, --InputPort SI = SIB9.SO
               CE => SIB_main_toCE,
               SE => SIB_main_toSE,
               UE => SIB_main_toUE,
               SEL => SIB_main_toSEL,
               RST => SIB_main_toRST,
               TCK => SIB_main_toTCK,
               SO => SIB10_so,
		         -- Scan Interface host ----------------
               fromSO => WI_10_so, --InputPort fromSO = WI_10.SO
               toCE => SIB10_toCE,
               toSE => SIB10_toSE,
               toUE => SIB10_toUE,
               toSEL => SIB10_toSEL,
               toRST => SIB10_toRST,
               toTCK => SIB10_toTCK,
               toSI => SIB10_toSI);

WI_10 : WrappedInstr
 Generic map ( Size => regSize) -- Parameter Size = $regSize
    Port map ( -- Scan Interface scan_client ----------
	            SI => sMux10_out, --InputPort SI = sMux10
               SO => WI_10_so,
               SEL => sel_WI10, -- InputPort SEL = sel_WI10
               ----------------------------------------		
               SE => SIB10_toSE,
               CE => SIB10_toCE,
               UE => SIB10_toUE,
               RST => SIB10_toRST,
               TCK => SIB10_toTCK,
			      INSTR_CLK => INSTR_CLK,
			      INSTR_RST => INSTR_RST);
					
					
sMux10 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB10_toSI, -- 1'b0 : SIB10.toSI
	            ScanMux_in(1) => WI_9_so, -- 1'b1 : WI_9.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux10_out);

end TreeFlat_arch;