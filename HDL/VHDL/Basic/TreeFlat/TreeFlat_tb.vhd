-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 08.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY TreeFlat_tb IS
END TreeFlat_tb;
 
ARCHITECTURE behavior OF TreeFlat_tb IS 
 
    -- Declaration of TreeFlat iJTAG benchmark
    COMPONENT TreeFlat
    PORT(
         SI : IN  std_logic;
         CE : IN  std_logic;
         SE : IN  std_logic;
         UE : IN  std_logic;
         SEL : IN  std_logic;
         RST : IN  std_logic;
         TCK : IN  std_logic;
         SO : OUT  std_logic;
			INSTR_CLK : IN  std_logic;
			INSTR_RST : IN  std_logic
        );
    END COMPONENT;

   -- iJTAG I/O signals
   signal SI : std_logic := '0';
   signal CE : std_logic := '0';
   signal SE : std_logic := '0';
   signal UE : std_logic := '0';
   signal SEL : std_logic := '0';
   signal RST : std_logic := '0';
   signal TCK : std_logic := '0';
   signal INSTR_RST : std_logic := '0';
   signal INSTR_CLK : std_logic := '0';	
   signal SO : std_logic;
	
	-- Testbench constants
	constant TCK_period : time := 20 ns;
	constant CLK_period : time := 20 ns;
	
	constant HALF_SEPARATOR : time := 2*TCK_period;
	constant FULL_SEPARATOR : time := 8*TCK_period;	
	
	constant MAX_SCANCHAIN_LENGTH : positive := 11*8+11+2;
	
	-- Testbench Functions
	
	-- Returns all zeroes std_logic_vector of specified size
   function all_zeroes (number_of_zeroes : in positive) return std_logic_vector is
	  variable zero_array : std_logic_vector(0 to number_of_zeroes-1);
   begin
	  for i in zero_array'range loop
       zero_array(i) := '0';
	  end loop;
	  return zero_array;
   end function all_zeroes;	
	
	-- Converts STD_LOGIC_VECTOR to STRING
   function slv_to_string (slv_data : in std_logic_vector) return string is
	  variable string_out : string(1 to slv_data'length);
   begin
	  for i in string_out'range loop
       string_out(i) := std_logic'image(slv_data(i-1))(2);
	  end loop;
	  return string_out;
   end function slv_to_string;	

   -- Testbench signals   
	
   signal so_value_history : std_logic_vector(0 to MAX_SCANCHAIN_LENGTH-1);
	
BEGIN
 
   INSTR_CLK <= not INSTR_CLK after CLK_period/2;
   
	-- Instantiate the Unit Under Test (UUT)
   uut: TreeFlat PORT MAP (
          SI => SI,
          CE => CE,
          SE => SE,
          UE => UE,
          SEL => SEL,
          RST => RST,
          TCK => TCK,
          SO => SO,
			 INSTR_CLK => INSTR_CLK,
			 INSTR_RST => INSTR_RST
        );
		  
   -- Scanchain SO value history	
	so_value_history <= so_value_history(1 to MAX_SCANCHAIN_LENGTH-1)&SO when (falling_edge(TCK) and SE = '1');

   -- Stimulus process
   stim_proc: process
	
	 -- Generate a number of TCK ticks
    procedure clock_tick (number_of_tick : in positive) is
    begin
	     for i in 1 to number_of_tick loop
	       TCK <= '1';
		    wait for TCK_period/2;
		    TCK <= '0';
		    wait for TCK_period/2;
		  end loop;
    end procedure clock_tick;
	 
	 -- Shifts in specified data (Capture -> Shift -> Update)
    procedure shift_data (data : in std_logic_vector) is
    begin
	     -- Capture phase
		  CE <= '1';
	     clock_tick(1);
		  CE <= '0';
		  -- Shift phase
        SE <= '1';
	     for i in data'range loop
			 SI <= data(i);
	       clock_tick(1);
		  end loop;
        SE <= '0';
		  -- Update phase
		  UE <= '1';
	     clock_tick(1);
		  UE <= '0';
    end procedure shift_data;
	 
	 -- Checks data from SO against provided expected data
    procedure data_check (expected_data : in std_logic_vector; report_type : string) is
	   variable so_data : std_logic_vector(0 to expected_data'length-1);
    begin
	   so_data := so_value_history(MAX_SCANCHAIN_LENGTH-expected_data'length to MAX_SCANCHAIN_LENGTH-1);
		if report_type = "report" then
		  if so_data = expected_data then
          report "TEST PASSED";
		  else
		    report "TEST FAILED"
		    severity ERROR;
		  end if;
		  report "Received SO = "&slv_to_string(so_data)&" (<-- SO)";	
		  report "Expected SO = "&slv_to_string(expected_data)&" (<-- SO)";	
		elsif report_type = "assert" then
		  assert (so_data = expected_data)
		  report "assert check FAILED"&LF&"Received SO = "&slv_to_string(so_data)&" (<-- SO)"&LF&"Expected SO = "&slv_to_string(expected_data)&" (<-- SO)"
		  severity ERROR;
		end if;
    end procedure data_check;	

	 variable TESTBYTE : std_logic_vector(0 to 7);	 
	 
   begin		
	   
		-- Set TESTBYTE value ---------------------------------------------------------------
		TESTBYTE := X"B3";
		
		-- Reset iJTAG chain and Instruments
	   RST <= '1';
      INSTR_RST <= '1';
		wait for TCK_period/2;
	   RST <= '0';
      INSTR_RST <= '0';
		-------------------------------------------------------------------------------------
		
		wait for FULL_SEPARATOR;
		
      -- TEST No.0 ------------------------------------------------------------------------
		-- By default SIBmain is closed and SCB1 is equal to 0 (Path 0 is selected for sMuxSO)
		-- Scanchain is 2 bits long 
		-- SO <- SCB1 <- SIB_main <- SI
		-- Check that default scanchain does not work when SEL is deasserted
		
		-- Deassert SEL
		SEL <= '0';

		-- Shift in TESTBYTE	+ 2 zeroes to retain the scanpath
		shift_data(TESTBYTE&all_zeroes(2));
		
		-- Check that UNINITIALIZED has appeared from SO
		report "TEST No.0 is finished";
      data_check("UUUUUUUU","report");
		
		-- Assert SEL for other tests
		SEL <= '1';
	
		-- END of TEST No.0 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

      -- TEST No.1 ------------------------------------------------------------------------
		-- SIBmain is closed and SCB1 is equal to 0 (Path 0 is selected for sMuxSO)
		-- Scanchain is 2 bits long 
		-- SO <- SCB1 <- SIB_main <- SI
		-- Check thet default scanchain

		-- Shift in TESTBYTE	+ 2 zeroes to retain the scanpath
		shift_data(TESTBYTE&all_zeroes(2));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.1 is finished";
      data_check(TESTBYTE,"report");
	
		-- END of TEST No.1 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

      -- TEST No.2 ------------------------------------------------------------------------
		-- SIBmain is closed and SCB1 is equal to 0 (Path 0 is selected for sMuxSO)
		-- Scanchain is 2 bits long 
		-- SO <- SCB1 <- SIB_main <- SI
		-- Reconfigure scanpath to select all instruments at once bypassing respective SIBs	
		
      -- Set SCB1 to value 1 (select Path 1 for sMuxSO)
		shift_data("10");
		
		wait for HALF_SEPARATOR;
		
		-- All Instruments are now selected (SIBs are bypassed)
		-- Scanchain is 89 bits long 
		-- SO <- SCB1 <- WI_10 <- WI_9 <- WI_8 <- WI_7 <- WI_6 <- WI_5 <- WI_4 <- WI_3 <- WI_2 <- WI_1 <- WI_0 <- SI	
		
		-- Shift in TESTBYTE	+ "1" + 88 zeroes to retain the scanpath
		shift_data(TESTBYTE&'1'&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.2 is finished";
      data_check(TESTBYTE,"report");
		
		-- END of TEST No.2 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;

      -- TEST No.3 ------------------------------------------------------------------------
		-- All Instruments are selected (SIBs are bypassed)
		-- Scanchain is 89 bits long 
		-- SO <- SCB1 <- WI_10 <- WI_9 <- WI_8 <- WI_7 <- WI_6 <- WI_5 <- WI_4 <- WI_3 <- WI_2 <- WI_1 <- WI_0 <- SI	
--		-- Reconfigure scanchain to the longest length 
		
      -- Set SCB1 to value 0 (switch back to Path 0 for sMuxSO)
		shift_data('0'&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8)&all_zeroes(8));
		
		wait for HALF_SEPARATOR;
		
		-- SIB_main is closed and SCB1 is now equal to 0 (Path 0 is selected for sMuxSO)
		-- Scanchain is 2 bits long 
		-- SO <- SCB1 <- SIB_main <- SI		
		
		-- Shift in TESTBYTE	+ 2 zeroes to retain the scanpath	
		shift_data(TESTBYTE&all_zeroes(2));
		
		-- Check that TESTBYTE has appeared from SO
      data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
      -- Open SIB_main (set SIB_main to value 1)
		shift_data("01");
		
		wait for HALF_SEPARATOR;

		-- SIB_main is now opened
		-- Scanchain is 13 bits long 
		-- SO <- SCB1 <- SIB_main <- SIB10 <- SIB9 <- SIB8 <- SIB7 <- SIB6 <- SIB5 <- SIB4 <- SIB3 <- SIB2 <- SIB1 <- SIB0 <- SI		
		
		-- Shift in TESTBYTE	+ "0000000000001" to retain the scanpath	
		shift_data(TESTBYTE&"0100000000000");
		
		-- Check that TESTBYTE has appeared from SO
      data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

      -- Open all remaining SIBs to include Instruments int scanchain
		shift_data("0111111111111");
		
		wait for HALF_SEPARATOR;

		-- All SIBs are now opened
		-- Scanchain is 101 bits long 
		-- SO <- SCB1 <- SIB_main <- SIB10 <- WI_10 <- SIB9 <- WI_9 <- SIB8 <- WI_8 <- SIB7 <- WI_7 <- SIB6 <- WI_6 <- 
		-- <- SIB5 <- WI_5 <- SIB4 <- WI_4 <- SIB3 <- WI_3 <- SIB2 <- WI_2 <- SIB1 <- WI_1 <- SIB0 <- WI_0 <- SI		
		
		-- Shift in TESTBYTE	+ "011" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + 
		-- + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 to retain the scanpath	
		shift_data(TESTBYTE&"011"&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00");
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.3 is finished";
      data_check(TESTBYTE,"report");
		
		-- END of TEST No.3 -----------------------------------------------------------------

		wait for FULL_SEPARATOR;

      -- TEST No.4 ------------------------------------------------------------------------
		-- The longest scanchain is selected
		-- Scanchain is 101 bits long 
		-- SO <- SCB1 <- SIB_main <- SIB10 <- WI_10 <- SIB9 <- WI_9 <- SIB8 <- WI_8 <- SIB7 <- WI_7 <- SIB6 <- WI_6 <- 
		-- <- SIB5 <- WI_5 <- SIB4 <- WI_4 <- SIB3 <- WI_3 <- SIB2 <- WI_2 <- SIB1 <- WI_1 <- SIB0 <- WI_0 <- SI		
		-- Write data to all instruments and read it back 

      -- Load all As into WI_10 data register, load all 8s into WI_8 data register,
      -- Load all 6s into WI_6 data register, load all 4s into WI_4 data register,
      -- Load all 2s into WI_2 data register, load all 0s into WI_0 data register

		-- Shift in "011" + 0xAB + "1" + 0x00 + "1" + 0x89 + "1" + 0x00 + "1" + 0x67 + "1" + 0x00 + 
		-- + "1" + 0x45 + "1" + 0x00 + "1" + 0x23 + "1" + 0x00 + "1" + 0x01 to retain the scanpath
		shift_data("011"&X"AB"&'1'&X"00"&'1'&X"89"&'1'&X"00"&'1'&X"67"&'1'&X"00"&'1'&X"45"&'1'&X"00"&'1'&X"23"&'1'&X"00"&'1'&X"01");

		wait for HALF_SEPARATOR;

      -- Load all 0s into scan registers of even instruments to stop writing to their data registers
      -- Load all 9s into WI_9 data register, load all 7s into WI_7 data register,
      -- Load all 5s into WI_5 data register, load all 3s into WI_3 data register,
      -- Load all 1s into WI_1 data register
		
		-- Shift in "011" + 0x00 + "1" + 0x99 + "1" + 0x00 + "1" + 0x77 + "1" + 0x00 + "1" + 0x55 + 
		-- + "1" + 0x00 + "1" + 0x33 + "1" + 0x00 + "1" + 0x11 + "1" + 0x00 to retain the scanpath
		shift_data("011"&X"00"&'1'&X"99"&'1'&X"00"&'1'&X"77"&'1'&X"00"&'1'&X"55"&'1'&X"00"&'1'&X"33"&'1'&X"00"&'1'&X"11"&'1'&X"00");

		wait for HALF_SEPARATOR;
		
      -- Load all 0s into scan registers of odd instruments to stop writing to their data registers
		
		-- Shift in "011" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + 
		-- + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 to retain the scanpath	
		shift_data("011"&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00");
		
		wait for HALF_SEPARATOR;
		
      -- Load all 0s into scan registers of all instruments
		
		-- Shift in "011" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + 
		-- + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 + "1" + 0x00 to retain the scanpath	
		shift_data("011"&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00"&'1'&X"00");
				
		-- Check that written content has appeared from SO
		report "TEST No.4 is finished";
      data_check("011"&X"AB"&'1'&X"99"&'1'&X"89"&'1'&X"77"&'1'&X"67"&'1'&X"55"&'1'&X"45"&'1'&X"33"&'1'&X"23"&'1'&X"11"&'1'&X"01","report");
		
		-- END of TEST No.4 -----------------------------------------------------------------
	
      wait;
   end process;

END;
