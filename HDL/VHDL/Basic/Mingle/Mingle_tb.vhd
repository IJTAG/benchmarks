-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 08.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY Mingle_tb IS
END Mingle_tb;
 
ARCHITECTURE behavior OF Mingle_tb IS 
 
    -- Declaration of Mingle iJTAG benchmark
    COMPONENT Mingle
    PORT(
         SI : IN  std_logic;
         CE : IN  std_logic;
         SE : IN  std_logic;
         UE : IN  std_logic;
         SEL : IN  std_logic;
         RST : IN  std_logic;
         TCK : IN  std_logic;
         SO : OUT  std_logic;
		 INSTR_CLK : IN  std_logic;
		 INSTR_RST : IN  std_logic
        );
    END COMPONENT;

   -- iJTAG I/O signals
   signal SI : std_logic := '0';
   signal CE : std_logic := '0';
   signal SE : std_logic := '0';
   signal UE : std_logic := '0';
   signal SEL : std_logic := '0';
   signal RST : std_logic := '0';
   signal TCK : std_logic := '0';
   signal INSTR_RST : std_logic := '0';
   signal INSTR_CLK : std_logic := '0';	
   signal SO : std_logic;
	
   -- Testbench constants
   constant TCK_period : time := 20 ns;
   constant CLK_period : time := 20 ns;
	
   constant HALF_SEPARATOR : time := 2*TCK_period;
   constant FULL_SEPARATOR : time := 8*TCK_period;	
	
   constant MAX_SCANCHAIN_LENGTH : positive := 2+3+2*32+6+3*32;
	
   -- Testbench Functions
	
   -- Returns all zeroes std_logic_vector of specified size
   function all_zeroes (number_of_zeroes : in positive) return std_logic_vector is
	  variable zero_array : std_logic_vector(0 to number_of_zeroes-1);
   begin
	  for i in zero_array'range loop
       zero_array(i) := '0';
	  end loop;
	  return zero_array;
   end function all_zeroes;	
	
   -- Converts STD_LOGIC_VECTOR to STRING
   function slv_to_string (slv_data : in std_logic_vector) return string is
	  variable string_out : string(1 to slv_data'length);
   begin
	  for i in string_out'range loop
       string_out(i) := std_logic'image(slv_data(i-1))(2);
	  end loop;
	  return string_out;
   end function slv_to_string;	

   -- Testbench signals   
   signal so_value_history : std_logic_vector(0 to MAX_SCANCHAIN_LENGTH-1);
   signal halt_simulation : std_logic:='0';
	
BEGIN
 
   INSTR_CLK <= (not INSTR_CLK and not halt_simulation) after CLK_period/2;
   
	-- Instantiate the Unit Under Test (UUT)
   uut: Mingle PORT MAP (
          SI => SI,
          CE => CE,
          SE => SE,
          UE => UE,
          SEL => SEL,
          RST => RST,
          TCK => TCK,
          SO => SO,
		  INSTR_CLK => INSTR_CLK,
		  INSTR_RST => INSTR_RST
        );

   -- Scanchain SO value history	
   so_value_history <= so_value_history(1 to MAX_SCANCHAIN_LENGTH-1)&SO when (falling_edge(TCK) and SE = '1');

   -- Stimulus process
   stim_proc: process
	
   -- Generate a number of TCK ticks
   procedure clock_tick (number_of_tick : in positive) is
   begin
	     for i in 1 to number_of_tick loop
	       TCK <= '1';
		    wait for TCK_period/2;
		    TCK <= '0';
		    wait for TCK_period/2;
		 end loop;
   end procedure clock_tick;
	 
   -- Shifts in specified data (Capture -> Shift -> Update)
   procedure shift_data (data : in std_logic_vector) is
   begin
	     -- Capture phase
		 CE <= '1';
	     clock_tick(1);
		 CE <= '0';
		 -- Shift phase
         SE <= '1';
	     for i in data'range loop
			SI <= data(i);
	        clock_tick(1);
		 end loop;
         SE <= '0';
		 -- Update phase
		 UE <= '1';
	     clock_tick(1);
		 UE <= '0';
   end procedure shift_data;
	 
   -- Checks data from SO against provided expected data
   procedure data_check (expected_data : in std_logic_vector; report_type : string) is
	   variable so_data : std_logic_vector(0 to expected_data'length-1);
   begin
	    so_data := so_value_history(MAX_SCANCHAIN_LENGTH-expected_data'length to MAX_SCANCHAIN_LENGTH-1);
		if report_type = "report" then
		  if so_data = expected_data then
            report "TEST PASSED";
		  else
		    report "TEST FAILED"
		    severity ERROR;
		  end if;
		  report "Received SO = "&slv_to_string(so_data)&" (<-- SO)"&LF&"      Expected SO = "&slv_to_string(expected_data)&" (<-- SO)";	
--		  report "Expected SO = "&slv_to_string(expected_data)&" (<-- SO)";	
		elsif report_type = "assert" then
		  assert (so_data = expected_data)
		  report "assert check FAILED"&LF&"Received SO = "&slv_to_string(so_data)&" (<-- SO)"&LF&"Expected SO = "&slv_to_string(expected_data)&" (<-- SO)"
		  severity ERROR;
		end if;
   end procedure data_check;	

   variable TESTBYTE : std_logic_vector(0 to 7);	 
	 
   begin		
	   
		-- Set TESTBYTE value ---------------------------------------------------------------
		TESTBYTE := X"B3";
		
		-- Reset iJTAG chain and Instruments
	    RST <= '1';
        INSTR_RST <= '1';
		wait for TCK_period/2;
	    RST <= '0';
        INSTR_RST <= '0';
		-------------------------------------------------------------------------------------
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.0 ------------------------------------------------------------------------
		-- By default SIB1 and SIB2 are closed
		-- Scanchain is 2 bits long 
		-- SO <- SIB2 <- SIB1 <- SI
		-- Check that default scanchain does not work when SEL is deasserted
		
		-- Deassert SEL
		SEL <= '0';

		-- Shift in TESTBYTE + 2 zeroes to retain the scanchain
		shift_data(TESTBYTE&all_zeroes(2));
		
		-- Check that UNINITIALIZED has appeared from SO
		report "TEST No.0 is finished";
        data_check("UUUUUUUU","report");
		
		-- Assert SEL for other tests
		SEL <= '1';
	
		-- END of TEST No.0 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

        -- TEST No.1 ------------------------------------------------------------------------
		-- SIB1 and SIB2 are closed
		-- Scanchain is 2 bits long 
		-- SO <- SIB2 <- SIB1 <- SI
		-- Check the default scanchain

		-- Shift in TESTBYTE + 2 zeroes to retain the scanchain
		shift_data(TESTBYTE&all_zeroes(2));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.1 is finished";
        data_check(TESTBYTE,"report");
	
		-- END of TEST No.1 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

        -- TEST No.2 ------------------------------------------------------------------------
		-- SIB1 and SIB2 are closed
		-- Scanchain is 2 bits long 
		-- SO <- SIB2 <- SIB1 <- SI
		-- Select branch B1 (instruments WI5 and WI6)	
		
        -- Open SIB1 (set SIB1 to value 1)
		shift_data("01");
		
		wait for HALF_SEPARATOR;
		
		-- SIB5 and SCB3 are included in the scanchain
		-- Scanchain is 4 bits long 
		-- SO <- SIB2 <- SIB1 <- SCB3 <- SIB5 <- SI	
		
		-- Shift in TESTBYTE + "0100" to retain the scanchain
		shift_data(TESTBYTE&"0100");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

        -- Open SIB5 (set SIB5 to value 1)
		shift_data("0101");
		
		wait for HALF_SEPARATOR;	

		-- WI5 and SIB6 are included in the scanchain
		-- Scanchain is 37 bits long 
		-- SO <- SIB2 <- SIB1 <- SCB3 <- SIB5 <- SIB6 <- WI5 <- SI	
		
		-- Shift in TESTBYTE + "01010" + 32 zeroes to retain the scanchain
		shift_data(TESTBYTE&"01010"&all_zeroes(32));
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;		
		
        -- Open SIB6 (set SIB6 to value 1)
		shift_data("01011"&all_zeroes(32));
		
		wait for HALF_SEPARATOR;	

		-- WI6 is included in the scanchain
		-- Scanchain is 69 bits long 
		-- SO <- SIB2 <- SIB1 <- SCB3 <- SIB5 <- SIB6 <- WI6 <- WI5 <- SI	
		
		-- Shift in TESTBYTE + "01011" + 64 zeroes to retain the scanchain
		shift_data(TESTBYTE&"01011"&all_zeroes(32)&all_zeroes(32));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.2 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.2 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.3 ------------------------------------------------------------------------
		-- Branch B1 is selected
		-- Scanchain is 69 bits long 
		-- SO <- SIB2 <- SIB1 <- SCB3 <- SIB5 <- SIB6 <- WI6 <- WI5 <- SI 	
		-- Write data to all instruments in branch B1 and read it back 

        -- Load 0x12345678 into WI6 data register 

		-- Shift in "01011" + 0x12345679 + 32 zeroes to retain the scanchain
		shift_data("01011"&X"12345679"&all_zeroes(32));

		wait for HALF_SEPARATOR;

        -- Load all 0s into scan register of WI6 to stop writing to its data registers
        -- Load 0x87654321 into WI5 data register

		-- Shift in "01011" + 32 zeroes + 0x87654321 to retain the scanchain
		shift_data("01011"&all_zeroes(32)&X"87654321");

		wait for HALF_SEPARATOR;

        -- Load all 0s into scan register of WI6 to stop writing to its data registers

		-- Shift in "01011" + 64 zeroes to retain the scanchain
		shift_data("01011"&all_zeroes(32)&all_zeroes(32));
			
		-- Check that written content has appeared from SO
		report "TEST No.3 is finished";
        data_check("01011"&X"12345679"&X"87654321","report");
		
		-- END of TEST No.3 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.4 ------------------------------------------------------------------------
		-- Branch B1 is selected
		-- Scanchain is 69 bits long 
		-- SO <- SIB2 <- SIB1 <- SCB3 <- SIB5 <- SIB6 <- WI6 <- WI5 <- SI 	
		-- Select branch B2 (instruments WI7 and WI8)	
		
        -- Set SCB3 to 1 (select Path 1 for sMux3)
		shift_data("01111"&all_zeroes(32)&all_zeroes(32));
		
		wait for HALF_SEPARATOR;

		-- SIB5, WI5, SIB6 and WI6 are excluded from the scanchain
		-- SIBpost3 is included in the scanchain
		-- Scanchain is 4 bits long 
		-- SO <- SIB2 <- SIB1 <- SCB3 <- SIBpost3 <- SI	
		
		-- Shift in TESTBYTE + "0110" to retain the scanchain
		shift_data(TESTBYTE&"0110");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

        -- Open SIBpost3 (set SIBpost3 to value 1)
		shift_data("0111");
		
		wait for HALF_SEPARATOR;	

		-- WI7 and SIB7 are included in the scanchain
		-- Scanchain is 37 bits long 
		-- SO <- SIB2 <- SIB1 <- SCB3 <- SIB7 <- WI7 <- SIBpost3 <- SI	
		
		-- Shift in TESTBYTE + "0110" + 32 zeroes + "1" to retain the scanchain
		shift_data(TESTBYTE&"0110"&all_zeroes(32)&'1');
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;		
		
        -- Open SIB7 (set SIB7 to value 1)
		shift_data("0111"&all_zeroes(32)&'1');
		
		wait for HALF_SEPARATOR;	

		-- WI8 is included in the scanchain
		-- Scanchain is 69 bits long 
		-- SO <- SIB2 <- SIB1 <- SCB3 <- SIB7 <- WI8 <- WI7 <- SIBpost3 <- SI		
		
		-- Shift in TESTBYTE + "01011" + 64 zeroes + "1" to retain the scanchain
		shift_data(TESTBYTE&"0111"&all_zeroes(32)&all_zeroes(32)&'1');
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.4 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.4 -----------------------------------------------------------------	
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.5 ------------------------------------------------------------------------
		-- Branch B2 is selected
		-- Scanchain is 69 bits long 
		-- SO <- SIB2 <- SIB1 <- SCB3 <- SIB7 <- WI8 <- WI7 <- SIBpost3 <- SI	
		-- Write data to all instruments in branch B2 and read it back 

        -- Load 0x12345678 into WI8 data register 

		-- Shift in "0111" + 0x12345679 + 32 zeroes + "1" to retain the scanchain
		shift_data("0111"&X"12345679"&all_zeroes(32)&'1');

		wait for HALF_SEPARATOR;

        -- Load all 0s into scan register of WI8 to stop writing to its data registers
        -- Load 0x87654321 into WI7 data register

		-- Shift in "0111" + 32 zeroes + 0x87654321 + "1" to retain the scanchain
		shift_data("0111"&all_zeroes(32)&X"87654321"&'1');

		wait for HALF_SEPARATOR;

        -- Load all 0s into scan register of WI7 to stop writing to its data registers

		-- Shift in "0111" + 64 zeroes + "1" to retain the scanchain
		shift_data("0111"&all_zeroes(32)&all_zeroes(32)&'1');
			
		-- Check that written content has appeared from SO
		report "TEST No.5 is finished";
        data_check("0111"&X"12345679"&X"87654321"&'1',"report");
		
		-- END of TEST No.5 -----------------------------------------------------------------

		wait for FULL_SEPARATOR;
		
        -- TEST No.6 ------------------------------------------------------------------------
		-- Branch B2 is selected
		-- Scanchain is 69 bits long 
		-- SO <- SIB2 <- SIB1 <- SCB3 <- SIB7 <- WI8 <- WI7 <- SIBpost3 <- SI	
		-- Close branch B2, open branch A1 (instrument WI1)	
		
        -- Set SIB1 to 0 and SIB2 to 1
		shift_data("1011"&all_zeroes(32)&all_zeroes(32)&'1');
		
		wait for HALF_SEPARATOR;

		-- Branch B2 is excluded from the scanchain
		-- Void1 and SCB2 are included in the scanchain
		-- Scanchain is 4 bits long 
		-- SO <- SIB2 <- SCB2 <- Void1 <- SIB1 <- SI	
		
		-- Shift in TESTBYTE + "1000" to retain the scanchain
		shift_data(TESTBYTE&"1000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

        -- Set SCB2 to 1 (select Path 1 for sMux2) to include SCB1 in scanchain
		shift_data("1100");
		
		wait for HALF_SEPARATOR;	

		-- Void1 is excluded from the scanchain		
		-- SCB1, SIB3 and SIB4 are included in the scanchain
		-- Scanchain is 6 bits long 
		-- SO <- SIB2 <- SCB2 <- SIB4 <- SIB3 <- SCB1 <- SIB1 <- SI
		
		-- Shift in TESTBYTE + "110000" to retain the scanchain
		shift_data(TESTBYTE&"110000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;		
		
        -- Set SCB1 to 1 and SCB2 to 0 (select Path 1 for sMux1 and Path 0 for sMux2) to include WI1 in the scanchain
		shift_data("100010");
		
		wait for HALF_SEPARATOR;	

		-- SCB1, SIB3 and SIB4 are excluded from the scanchain		
		-- WI1 is included in the scanchain (branch A1)
		-- Scanchain is 35 bits long 
		-- SO <- SIB2 <- SCB2 <- WI1 <- SIB1 <- SI	
		
		-- Shift in TESTBYTE + "10" + 32 zeroes + "0" to retain the scanchain
		shift_data(TESTBYTE&"10"&all_zeroes(32)&'0');
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.6 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.6 -----------------------------------------------------------------	
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.7 ------------------------------------------------------------------------
		-- Branch A1 is selected
		-- Scanchain is 35 bits long 
		-- SO <- SIB2 <- SCB2 <- WI1 <- SIB1 <- SI	
		-- Write data to instrument WI1 in branch A1 and read it back 

        -- Load 0x87654321 into WI1 data register 

		-- Shift in "10" + 0x87654321 + "0" to retain the scanchain
		shift_data("10"&X"87654321"&'0');

		wait for HALF_SEPARATOR;

        -- Load all 0s into scan register of WI1 to stop writing to its data registers

		-- Shift in "10" + 32 zeroes + "0" to retain the scanchain
		shift_data("10"&all_zeroes(32)&'0');

		wait for HALF_SEPARATOR;

        -- Load all 0s into scan register of WI1

		-- Shift in "10" + 32 zeroes + "0" to retain the scanchain
		shift_data("10"&all_zeroes(32)&'0');
			
		-- Check that written content has appeared from SO
		report "TEST No.7 is finished";
        data_check("10"&X"87654321"&'0',"report");
		
		-- END of TEST No.7 -----------------------------------------------------------------

		wait for FULL_SEPARATOR;
		
        -- TEST No.8 ------------------------------------------------------------------------
		-- Branch A1 is selected
		-- Scanchain is 35 bits long 
		-- SO <- SIB2 <- SCB2 <- WI1 <- SIB1 <- SI		
		-- Close branch A1, open branch A2 (instruments WI2, WI3 and WI4)	
		
        -- Set SCB2 to 1 (select Path 1 for sMux2) to include SCB1, SIB3 and SIB4 in scanchain
        shift_data("11"&all_zeroes(32)&'0');
		
		wait for HALF_SEPARATOR;

		-- Branch A1 is excluded from the scanchain
		-- SCB1, SIB3 and SIB4 are included in the scanchain
		-- Scanchain is 6 bits long 
		-- SO <- SIB2 <- SCB2 <- SIB4 <- SIB3 <- SCB1 <- SIB1 <- SI	
		
		-- Shift in TESTBYTE + "110000" to retain the scanchain
		shift_data(TESTBYTE&"110000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

        -- Open SIB3 and SIB4 (set SIB3 and SIB4 to 1) to include SIBpost1, SIBpost2 and WI2 in scanchain
		shift_data("111100");
		
		wait for HALF_SEPARATOR;	
	
		-- SIBpost1, SIBpost2 and WI2 are included in the scanchain
		-- Scanchain is 40 bits long 
		-- SO <- SIB2 <- SCB2 <- SIB4 <- WI2 <- SIB3 <- SIBpost2 <- SIBpost1 <- SCB1 <- SIB1 <- SI	
		
		-- Shift in TESTBYTE + "111" + 32 zeroes + "10000" to retain the scanchain
		shift_data(TESTBYTE&"111"&all_zeroes(32)&"10000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;		
		
        -- Open SIBpost1 and SIBpost2 (set SIBpost1 and SIBpost2 to 1) to include WI3 and WI4 in scanchain
		shift_data("111"&all_zeroes(32)&"11100");
		
		wait for HALF_SEPARATOR;	
	
		-- Branch A2 is selected
		-- Scanchain is 104 bits long 
		-- SO <- SIB2 <- SCB2 <- SIB4 <- WI2 <- SIB3 <- WI4 <- SIBpost2 <- WI3 <- SIBpost1 <- SCB1 <- SIB1 <- SI	
		
		-- Shift in TESTBYTE + "111" + 32 zeroes + "1" + 32 zeroes + "1" + 32 zeroes + "100" to retain the scanchain
		shift_data(TESTBYTE&"111"&all_zeroes(32)&'1'&all_zeroes(32)&'1'&all_zeroes(32)&"100");
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.8 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.8 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.9 ------------------------------------------------------------------------
		-- Branch A2 is selected
		-- Scanchain is 104 bits long 
		-- SO <- SIB2 <- SCB2 <- SIB4 <- WI2 <- SIB3 <- WI4 <- SIBpost2 <- WI3 <- SIBpost1 <- SCB1 <- SIB1 <- SI	
		-- Write data to all instruments in branch A2 and read it back 

        -- Load 0x12345678 into WI2 and WI4 data register 

		-- Shift in "111" + 0x12345679 + "1" + 0x12345679 + "1" + 32 zeroes + "100" to retain the scanchain
		shift_data("111"&X"12345679"&'1'&X"12345679"&'1'&all_zeroes(32)&"100");

		wait for HALF_SEPARATOR;

        -- Load all 0s into scan register of WI2 and WI4 to stop writing to their data registers
        -- Load 0x87654321 into WI3 data register

		-- Shift in "111" + 32 zeroes + "1" + 32 zeroes + "1" + 0x87654321 + "100" to retain the scanchain
		shift_data("111"&all_zeroes(32)&'1'&all_zeroes(32)&'1'&X"87654321"&"100");

		wait for HALF_SEPARATOR;

        -- Load all 0s into scan register of WI3 to stop writing to its data registers

		-- Shift in "111" + 32 zeroes + "1" + 32 zeroes + "1" + 32 zeroes + "100" to retain the scanchain
		shift_data("111"&all_zeroes(32)&'1'&all_zeroes(32)&'1'&all_zeroes(32)&"100");
			
		-- Check that written content has appeared from SO
		report "TEST No.9 is finished";
        data_check("111"&X"12345679"&'1'&X"12345679"&'1'&X"87654321"&"100","report");
		
		-- END of TEST No.9 -----------------------------------------------------------------

		wait for FULL_SEPARATOR;
		
		-- STOP SIMULATION ------------------------------------------------------------------
	    halt_simulation <= '1';
        wait;
        
   end process;

END;
