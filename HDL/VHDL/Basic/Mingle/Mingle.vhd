-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 08.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Mingle is
 Generic ( regSize : positive := 32;
           regSize1 : positive := regSize;
		   regSize2 : positive := regSize;
		   regSize3 : positive := regSize;
		   regSize4 : positive := regSize;
		   regSize5 : positive := regSize;
		   regSize6 : positive := regSize;
		   regSize7 : positive := regSize;
		   regSize8 : positive := regSize);
    Port ( -- iJTAG interface
	       SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort	
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC;
		   -- Instruments interface
		   INSTR_CLK : in STD_LOGIC; -- Instruments Clock
		   INSTR_RST : in STD_LOGIC); -- Instruments Reset
end Mingle;

architecture Mingle_arch of Mingle is

signal sel_Void1 : std_logic; --LogicSignal sel_Void1
signal sel_WI1 : std_logic; --LogicSignal sel_WI1
signal sel_SIB3 : std_logic; --LogicSignal sel_SIB3
signal sel_SIB4 : std_logic; --LogicSignal sel_SIB4
signal sel_SCB1 : std_logic; --LogicSignal sel_SCB1
signal sel_SIB5 : std_logic; --LogicSignal sel_SIB5
signal sel_SIBpost3 : std_logic; --LogicSignal sel_SIBpost3

component WrappedInstr is
 Generic ( Size : positive);
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   -- Instruments interface
		   INSTR_CLK : in STD_LOGIC; -- Instruments Clock
		   INSTR_RST : in STD_LOGIC); -- Instruments Reset
end component;

signal WI1_so, WI2_so, WI3_so, WI4_so, WI5_so, WI6_so, WI7_so, WI8_so  : std_logic;

component SCB is
    Port ( -- Scan Interface  client --------------
	        SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
           ----------------------------------------
			  toSEL : out STD_LOGIC; -- ToSelectPort
			  DO : out STD_LOGIC_VECTOR (0 downto 0)); -- DataOutPort
end component;

signal SCB1_so, SCB2_so, SCB3_so : std_logic;
signal SCB1_toSEL, SCB2_toSEL, SCB3_toSEL : std_logic;
signal SCB1_do, SCB2_do, SCB3_do : std_logic_vector(0 downto 0);

component BypassReg is
    Port ( -- Scan Interface  client --------------
	       SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC); -- ScanOutPort
           ----------------------------------------
end component;

signal Void1_so : std_logic; 

component ScanMux is
 Generic (ControlSize : positive);
    Port ( ScanMux_in : in STD_LOGIC_VECTOR((2**ControlSize)-1 downto 0);
           SelectedBy : in STD_LOGIC_VECTOR(ControlSize-1 downto 0);
           ScanMux_out : out STD_LOGIC);
end component;

signal sMux1_out, sMux2_out, sMux3_out : std_logic; 

component SIB_mux_pre is
    Port ( -- Scan Interface  client --------------
	       SI : in  STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
		   -- Scan Interface  host ----------------
           fromSO : in  STD_LOGIC; -- ScanInPort
           toCE : out  STD_LOGIC; -- ToCaptureEnPort
           toSE : out  STD_LOGIC; -- ToShiftEnPort
           toUE : out  STD_LOGIC; -- ToUpdateEnPort
           toSEL : out  STD_LOGIC; -- ToSelectPort
           toRST : out  STD_LOGIC; -- ToResetPort
           toTCK : out  STD_LOGIC; -- ToTCKPort
           toSI : out  STD_LOGIC); -- ScanOutPort
end component;

signal SIB1_so, SIB2_so, SIB3_so, SIB4_so, SIB5_so, SIB6_so, SIB7_so : std_logic;
signal SIB1_toCE, SIB2_toCE, SIB3_toCE, SIB4_toCE, SIB5_toCE, SIB6_toCE, SIB7_toCE : std_logic;
signal SIB1_toSE, SIB2_toSE, SIB3_toSE, SIB4_toSE, SIB5_toSE, SIB6_toSE, SIB7_toSE : std_logic;
signal SIB1_toUE, SIB2_toUE, SIB3_toUE, SIB4_toUE, SIB5_toUE, SIB6_toUE, SIB7_toUE : std_logic;
signal SIB1_toSEL, SIB2_toSEL, SIB3_toSEL, SIB4_toSEL, SIB5_toSEL, SIB6_toSEL, SIB7_toSEL : std_logic;
signal SIB1_toRST, SIB2_toRST, SIB3_toRST, SIB4_toRST, SIB5_toRST, SIB6_toRST, SIB7_toRST : std_logic;
signal SIB1_toTCK, SIB2_toTCK, SIB3_toTCK, SIB4_toTCK, SIB5_toTCK, SIB6_toTCK, SIB7_toTCK : std_logic;
signal SIB1_toSI, SIB2_toSI, SIB3_toSI, SIB4_toSI, SIB5_toSI, SIB6_toSI, SIB7_toSI : std_logic;

component SIB_mux_post is
    Port ( -- Scan Interface  client --------------
	       SI : in  STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
		   -- Scan Interface  host ----------------
           fromSO : in  STD_LOGIC; -- ScanInPort
           toCE : out  STD_LOGIC; -- ToCaptureEnPort
           toSE : out  STD_LOGIC; -- ToShiftEnPort
           toUE : out  STD_LOGIC; -- ToUpdateEnPort
           toSEL : out  STD_LOGIC; -- ToSelectPort
           toRST : out  STD_LOGIC; -- ToResetPort
           toTCK : out  STD_LOGIC; -- ToTCKPort
           toSI : out  STD_LOGIC); -- ScanOutPort
end component;

signal SIBpost1_so, SIBpost2_so, SIBpost3_so : std_logic;
signal SIBpost1_toCE, SIBpost2_toCE, SIBpost3_toCE : std_logic;
signal SIBpost1_toSE, SIBpost2_toSE, SIBpost3_toSE : std_logic;
signal SIBpost1_toUE, SIBpost2_toUE, SIBpost3_toUE : std_logic;
signal SIBpost1_toSEL, SIBpost2_toSEL, SIBpost3_toSEL : std_logic;
signal SIBpost1_toRST, SIBpost2_toRST, SIBpost3_toRST : std_logic;
signal SIBpost1_toTCK, SIBpost2_toTCK, SIBpost3_toTCK : std_logic;
signal SIBpost1_toSI, SIBpost2_toSI, SIBpost3_toSI : std_logic;


begin

SO <= SIB2_so; -- Source SIB2.SO

-- Level 1
SIB1 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => SI, --InputPort SI = SI
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL,
               RST => RST,
               TCK => TCK,
               SO => SIB1_so,
		         -- Scan Interface  host ----------------
               fromSO => SCB3_so, --InputPort fromSO = SCB3.SO
               toCE => SIB1_toCE,
               toSE => SIB1_toSE,
               toUE => SIB1_toUE,
               toSEL => SIB1_toSEL,
               toRST => SIB1_toRST,
               toTCK => SIB1_toTCK,
               toSI => SIB1_toSI);
			   
SIB2 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => SIB1_so, --InputPort SI = SIB1.SO
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL,
               RST => RST,
               TCK => TCK,
               SO => SIB2_so,
		         -- Scan Interface  host ----------------
               fromSO => SCB2_so, --InputPort fromSO = SCB2.SO
               toCE => SIB2_toCE,
               toSE => SIB2_toSE,
               toUE => SIB2_toUE,
               toSEL => SIB2_toSEL,
               toRST => SIB2_toRST,
               toTCK => SIB2_toTCK,
               toSI => SIB2_toSI);

-- Branch A		   
sel_Void1 <= SIB2_toSEL and not SCB1_toSEL and not SCB2_toSEL; --LogicSignal sel_Void1
sel_WI1 <= SIB2_toSEL and SCB1_toSEL and not SCB2_toSEL; --LogicSignal sel_WI1
sel_SIB3 <= SIB2_toSEL and SCB2_toSEL; --LogicSignal sel_SIB3
sel_SIB4 <= SIB2_toSEL and SCB2_toSEL; --LogicSignal sel_SIB4
sel_SCB1 <= SIB2_toSEL and SCB2_toSEL; --LogicSignal sel_SCB1

WI1 : WrappedInstr
 Generic map ( Size => regSize1) -- Parameter Size = $regSize1
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB2_toSI, --InputPort SI = SIB2.toSI
               SO => WI1_so,
               SEL => sel_WI1, -- InputPort SEL = sel_WI1
               ----------------------------------------		
               SE => SIB2_toSE,
               CE => SIB2_toCE,
               UE => SIB2_toUE,
               RST => SIB2_toRST,
               TCK => SIB2_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);

Void1 : BypassReg
    Port map ( -- Scan Interface  client --------------
	           SI => SIB2_toSI, -- InputPort SI = SIB2.toSI 
               CE => SIB2_toCE,
               SE => SIB2_toSE,
               UE => SIB2_toUE,
               SEL => sel_Void1, -- InputPort SEL = sel_Void1
               RST => SIB2_toRST,
               TCK => SIB2_toTCK,
               SO => Void1_so);	

SCB1 : SCB
    Port map ( -- Scan Interface  client --------------
	           SI => SIB2_toSI, -- InputPort SI = SIB2.toSI
               CE => SIB2_toCE,
               SE => SIB2_toSE,
               UE => SIB2_toUE,
               SEL => sel_SCB1, -- InputPort SEL = sel_SCB1
               RST => SIB2_toRST,
               TCK => SIB2_toTCK,
               SO => SCB1_so,
               ----------------------------------------
			   toSEL => SCB1_toSEL,
			   DO => SCB1_do);
					
sMux1 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => Void1_so, -- 1'b0 : Void1.SO
	           ScanMux_in(1) => WI1_so, -- 1'b1 : WI1.SO
               SelectedBy => SCB1_do, --SelectedBy SCB1.DO
               ScanMux_out => sMux1_out);
			   
SIB3 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => SCB1_so, --InputPort SI = SCB1.SO
               CE => SIB2_toCE,
               SE => SIB2_toSE,
               UE => SIB2_toUE,
               SEL => sel_SIB3, --InputPort SEL = sel_SIB3
               RST => SIB2_toRST,
               TCK => SIB2_toTCK,
               SO => SIB3_so,
		         -- Scan Interface  host ----------------
               fromSO => SIBpost2_so, --InputPort fromSO = SIBpost2.SO
               toCE => SIB3_toCE,
               toSE => SIB3_toSE,
               toUE => SIB3_toUE,
               toSEL => SIB3_toSEL,
               toRST => SIB3_toRST,
               toTCK => SIB3_toTCK,
               toSI => SIB3_toSI);
			   
SIBpost1 : SIB_mux_post
    Port map ( -- Scan Interface  client --------------
	           SI => SIB3_toSI, --InputPort SI = SIB3.toSI
               CE => SIB3_toCE,
               SE => SIB3_toSE,
               UE => SIB3_toUE,
               SEL => SIB3_toSEL,
               RST => SIB3_toRST,
               TCK => SIB3_toTCK,
               SO => SIBpost1_so,
		         -- Scan Interface  host ----------------
               fromSO => WI3_so, --InputPort fromSO = WI3.SO
               toCE => SIBpost1_toCE,
               toSE => SIBpost1_toSE,
               toUE => SIBpost1_toUE,
               toSEL => SIBpost1_toSEL,
               toRST => SIBpost1_toRST,
               toTCK => SIBpost1_toTCK,
               toSI => SIBpost1_toSI);

WI3 : WrappedInstr
 Generic map ( Size => regSize3) -- Parameter Size = $regSize3
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIBpost1_toSI, --InputPort SI = SIBpost1.toSI
               SO => WI3_so,
               SEL => SIBpost1_toSEL,
               ----------------------------------------		
               SE => SIBpost1_toSE,
               CE => SIBpost1_toCE,
               UE => SIBpost1_toUE,
               RST => SIBpost1_toRST,
               TCK => SIBpost1_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);	

SIBpost2 : SIB_mux_post
    Port map ( -- Scan Interface  client --------------
	           SI => SIBpost1_so, --InputPort SI = SIBpost1.SO
               CE => SIB3_toCE,
               SE => SIB3_toSE,
               UE => SIB3_toUE,
               SEL => SIB3_toSEL,
               RST => SIB3_toRST,
               TCK => SIB3_toTCK,
               SO => SIBpost2_so,
		         -- Scan Interface  host ----------------
               fromSO => WI4_so, --InputPort fromSO = WI4.SO
               toCE => SIBpost2_toCE,
               toSE => SIBpost2_toSE,
               toUE => SIBpost2_toUE,
               toSEL => SIBpost2_toSEL,
               toRST => SIBpost2_toRST,
               toTCK => SIBpost2_toTCK,
               toSI => SIBpost2_toSI);	

WI4 : WrappedInstr
 Generic map ( Size => regSize4) -- Parameter Size = $regSize4
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIBpost2_toSI, --InputPort SI = SIBpost2.toSI
               SO => WI4_so,
               SEL => SIBpost2_toSEL,
               ----------------------------------------		
               SE => SIBpost2_toSE,
               CE => SIBpost2_toCE,
               UE => SIBpost2_toUE,
               RST => SIBpost2_toRST,
               TCK => SIBpost2_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);	

SIB4 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => SIB3_so, --InputPort SI = SIB3.SO
               CE => SIB2_toCE,
               SE => SIB2_toSE,
               UE => SIB2_toUE,
               SEL => sel_SIB4, --InputPort SEL = sel_SIB4
               RST => SIB2_toRST,
               TCK => SIB2_toTCK,
               SO => SIB4_so,
		         -- Scan Interface  host ----------------
               fromSO => WI2_so, --InputPort fromSO = WI2.SO
               toCE => SIB4_toCE,
               toSE => SIB4_toSE,
               toUE => SIB4_toUE,
               toSEL => SIB4_toSEL,
               toRST => SIB4_toRST,
               toTCK => SIB4_toTCK,
               toSI => SIB4_toSI);			   

WI2 : WrappedInstr
 Generic map ( Size => regSize2) -- Parameter Size = $regSize2
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB4_toSI, --InputPort SI = SIB4.toSI
               SO => WI2_so,
               SEL => SIB4_toSEL,
               ----------------------------------------		
               SE => SIB4_toSE,
               CE => SIB4_toCE,
               UE => SIB4_toUE,
               RST => SIB4_toRST,
               TCK => SIB4_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);				   

sMux2 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => sMux1_out, -- 1'b0 : sMux1
	           ScanMux_in(1) => SIB4_so, -- 1'b1 : SIB4.SO
               SelectedBy => SCB2_do, --SelectedBy SCB2.DO
               ScanMux_out => sMux2_out);
			   
SCB2 : SCB
    Port map ( -- Scan Interface  client --------------
	           SI => sMux2_out, -- InputPort SI = sMux2
               CE => SIB2_toCE,
               SE => SIB2_toSE,
               UE => SIB2_toUE,
               SEL => SIB2_toSEL,
               RST => SIB2_toRST,
               TCK => SIB2_toTCK,
               SO => SCB2_so,
               ----------------------------------------
			   toSEL => SCB2_toSEL,
			   DO => SCB2_do);

-- Branch B
sel_SIB5 <= SIB1_toSEL and not SCB3_toSEL; --LogicSignal sel_SIB5
sel_SIBpost3 <= SIB1_toSEL and SCB3_toSEL; --LogicSignal sel_SIBpost3		   
	
SIB5 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => SIB1_toSI, --InputPort SI = SIB1.toSI
               CE => SIB1_toCE,
               SE => SIB1_toSE,
               UE => SIB1_toUE,
               SEL => sel_SIB5, --InputPort SEL = sel_SIB5
               RST => SIB1_toRST,
               TCK => SIB1_toTCK,
               SO => SIB5_so,
		         -- Scan Interface  host ----------------
               fromSO => SIB6_so, --InputPort fromSO = SIB6.SO
               toCE => SIB5_toCE,
               toSE => SIB5_toSE,
               toUE => SIB5_toUE,
               toSEL => SIB5_toSEL,
               toRST => SIB5_toRST,
               toTCK => SIB5_toTCK,
               toSI => SIB5_toSI);	
			   
WI5 : WrappedInstr
 Generic map ( Size => regSize5) -- Parameter Size = $regSize5
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB5_toSI, --InputPort SI = SIB5.toSI
               SO => WI5_so,
               SEL => SIB5_toSEL,
               ----------------------------------------		
               SE => SIB5_toSE,
               CE => SIB5_toCE,
               UE => SIB5_toUE,
               RST => SIB5_toRST,
               TCK => SIB5_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);	
			   
SIB6 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => WI5_so, --InputPort SI = WI5.SO
               CE => SIB5_toCE,
               SE => SIB5_toSE,
               UE => SIB5_toUE,
               SEL => SIB5_toSEL,
               RST => SIB5_toRST,
               TCK => SIB5_toTCK,
               SO => SIB6_so,
		         -- Scan Interface  host ----------------
               fromSO => WI6_so, --InputPort fromSO = WI6.SO
               toCE => SIB6_toCE,
               toSE => SIB6_toSE,
               toUE => SIB6_toUE,
               toSEL => SIB6_toSEL,
               toRST => SIB6_toRST,
               toTCK => SIB6_toTCK,
               toSI => SIB6_toSI);
			   
WI6 : WrappedInstr
 Generic map ( Size => regSize6) -- Parameter Size = $regSize6
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB6_toSI, --InputPort SI = SIB6.toSI
               SO => WI6_so,
               SEL => SIB6_toSEL,
               ----------------------------------------		
               SE => SIB6_toSE,
               CE => SIB6_toCE,
               UE => SIB6_toUE,
               RST => SIB6_toRST,
               TCK => SIB6_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);	
			   
SIBpost3 : SIB_mux_post
    Port map ( -- Scan Interface  client --------------
	           SI => SIB1_toSI, --InputPort SI = SIB1.toSI
               CE => SIB1_toCE,
               SE => SIB1_toSE,
               UE => SIB1_toUE,
               SEL => sel_SIBpost3, --InputPort SEL = sel_SIBpost3
               RST => SIB1_toRST,
               TCK => SIB1_toTCK,
               SO => SIBpost3_so,
		         -- Scan Interface  host ----------------
               fromSO => SIB7_so, --InputPort fromSO = SIB7.SO
               toCE => SIBpost3_toCE,
               toSE => SIBpost3_toSE,
               toUE => SIBpost3_toUE,
               toSEL => SIBpost3_toSEL,
               toRST => SIBpost3_toRST,
               toTCK => SIBpost3_toTCK,
               toSI => SIBpost3_toSI);	
			   
WI7 : WrappedInstr
 Generic map ( Size => regSize7) -- Parameter Size = $regSize7
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIBpost3_toSI, --InputPort SI = SIBpost3.toSI
               SO => WI7_so,
               SEL => SIBpost3_toSEL,
               ----------------------------------------		
               SE => SIBpost3_toSE,
               CE => SIBpost3_toCE,
               UE => SIBpost3_toUE,
               RST => SIBpost3_toRST,
               TCK => SIBpost3_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);	
			   
SIB7 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => WI7_so, --InputPort SI = WI7.SO
               CE => SIBpost3_toCE,
               SE => SIBpost3_toSE,
               UE => SIBpost3_toUE,
               SEL => SIBpost3_toSEL,
               RST => SIBpost3_toRST,
               TCK => SIBpost3_toTCK,
               SO => SIB7_so,
		         -- Scan Interface  host ----------------
               fromSO => WI8_so, --InputPort fromSO = WI8.SO
               toCE => SIB7_toCE,
               toSE => SIB7_toSE,
               toUE => SIB7_toUE,
               toSEL => SIB7_toSEL,
               toRST => SIB7_toRST,
               toTCK => SIB7_toTCK,
               toSI => SIB7_toSI);
			   
WI8 : WrappedInstr
 Generic map ( Size => regSize8) -- Parameter Size = $regSize8
    Port map ( -- Scan Interface scan_client ----------
	           SI => SIB7_toSI, --InputPort SI = SIB7.toSI
               SO => WI8_so,
               SEL => SIB7_toSEL,
               ----------------------------------------		
               SE => SIB7_toSE,
               CE => SIB7_toCE,
               UE => SIB7_toUE,
               RST => SIB7_toRST,
               TCK => SIB7_toTCK,
			   INSTR_CLK => INSTR_CLK,
			   INSTR_RST => INSTR_RST);	
			   
sMux3 : ScanMux
 Generic map ( ControlSize => 1)
    Port map ( ScanMux_in(0) => SIB5_so, -- 1'b0 : SIB5.SO
	           ScanMux_in(1) => SIBpost3_so, -- 1'b1 : SIBpost3.SO
               SelectedBy => SCB3_do, --SelectedBy SCB3.DO
               ScanMux_out => sMux3_out);

SCB3 : SCB
    Port map ( -- Scan Interface  client --------------
	           SI => sMux3_out, -- InputPort SI = sMux3
               CE => SIB1_toCE,
               SE => SIB1_toSE,
               UE => SIB1_toUE,
               SEL => SIB1_toSEL,
               RST => SIB1_toRST,
               TCK => SIB1_toTCK,
               SO => SCB3_so,
               ----------------------------------------
			   toSEL => SCB3_toSEL,
			   DO => SCB3_do);
			   
end Mingle_arch;