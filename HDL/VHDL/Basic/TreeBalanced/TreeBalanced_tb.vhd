-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 21.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------
--         1.1 | 28.04.2017 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY TreeBalanced_tb IS
END TreeBalanced_tb;
 
ARCHITECTURE behavior OF TreeBalanced_tb IS 
 
    -- Declaration of TreeBalanced iJTAG benchmark
    COMPONENT TreeBalanced
    PORT(
         SI : IN  std_logic;
         CE : IN  std_logic;
         SE : IN  std_logic;
         UE : IN  std_logic;
         SEL : IN  std_logic;
         RST : IN  std_logic;
         TCK : IN  std_logic;
         SO : OUT  std_logic
        );
    END COMPONENT;

   -- iJTAG I/O signals
   signal SI : std_logic := '0';
   signal CE : std_logic := '0';
   signal SE : std_logic := '0';
   signal UE : std_logic := '0';
   signal SEL : std_logic := '0';
   signal RST : std_logic := '0';
   signal TCK : std_logic := '0';
   signal INSTR_RST : std_logic := '0';
   signal INSTR_CLK : std_logic := '0';	
   signal SO : std_logic;
	
	-- Testbench constants
	constant TCK_period : time := 20 ns;
	constant CLK_period : time := 20 ns;
	
	constant HALF_SEPARATOR : time := 2*TCK_period;
	constant FULL_SEPARATOR : time := 8*TCK_period;	

    -- Sets maximum history size for SO output	
	constant MAX_SCANCHAIN_LENGTH : positive := 8; -- Only TESTBYTE is going to be checked
	
	-- Testbench Functions
	
	-- Returns all zeroes std_logic_vector of specified size
   function all_zeroes (number_of_zeroes : in positive) return std_logic_vector is
	  variable zero_array : std_logic_vector(0 to number_of_zeroes-1);
   begin
	  for i in zero_array'range loop
       zero_array(i) := '0';
	  end loop;
	  return zero_array;
   end function all_zeroes;	
	
	-- Converts STD_LOGIC_VECTOR to STRING
   function slv_to_string (slv_data : in std_logic_vector) return string is
	  variable string_out : string(1 to slv_data'length);
   begin
	  for i in string_out'range loop
       string_out(i) := std_logic'image(slv_data(i-1))(2);
	  end loop;
	  return string_out;
   end function slv_to_string;	

   -- Testbench signals   
   signal so_value_history : std_logic_vector(0 to MAX_SCANCHAIN_LENGTH-1);
   signal halt_simulation : std_logic:='0';
	
BEGIN
 
   INSTR_CLK <= (not INSTR_CLK and not halt_simulation) after CLK_period/2;
   
	-- Instantiate the Unit Under Test (UUT)
   uut: TreeBalanced PORT MAP (
          SI => SI,
          CE => CE,
          SE => SE,
          UE => UE,
          SEL => SEL,
          RST => RST,
          TCK => TCK,
          SO => SO
        );
		  
   -- Scanchain SO value history	
	so_value_history <= so_value_history(1 to MAX_SCANCHAIN_LENGTH-1)&SO when (falling_edge(TCK) and SE = '1');

   -- Stimulus process
   stim_proc: process
	
	 -- Generate a number of TCK ticks
    procedure clock_tick (number_of_tick : in positive) is
    begin
	     for i in 1 to number_of_tick loop
	       TCK <= '1';
		    wait for TCK_period/2;
		    TCK <= '0';
		    wait for TCK_period/2;
		  end loop;
    end procedure clock_tick;
	 
	 -- Shifts in specified data (Capture -> Shift -> Update)
    procedure shift_data (data : in std_logic_vector) is
    begin
	     -- Capture phase
		  CE <= '1';
	     clock_tick(1);
		  CE <= '0';
		  -- Shift phase
        SE <= '1';
	     for i in data'range loop
			 SI <= data(i);
	       clock_tick(1);
		  end loop;
        SE <= '0';
		  -- Update phase
		  UE <= '1';
	     clock_tick(1);
		  UE <= '0';
    end procedure shift_data;
	 
	 -- Checks data from SO against provided expected data
    procedure data_check (expected_data : in std_logic_vector; report_type : string) is
	   variable so_data : std_logic_vector(0 to expected_data'length-1);
    begin
	   so_data := so_value_history(MAX_SCANCHAIN_LENGTH-expected_data'length to MAX_SCANCHAIN_LENGTH-1);
		if report_type = "report" then
		  if so_data = expected_data then
          report "TEST PASSED";
		  else
		    report "TEST FAILED"
		    severity ERROR;
		  end if;
		  report "Received SO = "&slv_to_string(so_data)&" (<-- SO)";	
		  report "Expected SO = "&slv_to_string(expected_data)&" (<-- SO)";	
		elsif report_type = "assert" then
		  assert (so_data = expected_data)
		  report "assert check FAILED"&LF&"Received SO = "&slv_to_string(so_data)&" (<-- SO)"&LF&"Expected SO = "&slv_to_string(expected_data)&" (<-- SO)"
		  severity ERROR;
		end if;
    end procedure data_check;	

	 variable TESTBYTE : std_logic_vector(0 to 7);	 
	 
   begin		
	   
		-- Set TESTBYTE value ---------------------------------------------------------------
		TESTBYTE := X"B3";
		
		-- Reset iJTAG chain and Instruments
	    RST <= '1';
        INSTR_RST <= '1';
		wait for TCK_period/2;
	    RST <= '0';
        INSTR_RST <= '0';
		-------------------------------------------------------------------------------------
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.0 ------------------------------------------------------------------------
		-- By default sib0 is closed
		-- Scanchain is 1 bits long 
		-- SO <- sib0 <- SI
		-- Check that default scanchain does not work when SEL is deasserted
		
		-- Deassert SEL
		SEL <= '0';

		-- Shift in TESTBYTE + 1 zeroes to retain the scanchain
		shift_data(TESTBYTE&all_zeroes(1));
		
		-- Check that UNINITIALIZED has appeared from SO
		report "TEST No.0 is finished";
        data_check("UUUUUUUU","report");
		
		-- Assert SEL for other tests
		SEL <= '1';
	
		-- END of TEST No.0 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

        -- TEST No.1 ------------------------------------------------------------------------
		-- sib0 is closed
		-- Scanchain is 1 bits long 
		-- SO <- sib0 <- SI
		-- Check the default scanchain

		-- Shift in TESTBYTE + 1 zeroes to retain the scanchain
		shift_data(TESTBYTE&all_zeroes(1));
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.1 is finished";
        data_check(TESTBYTE,"report");
	
		-- END of TEST No.1 -----------------------------------------------------------------
		
		wait for FULL_SEPARATOR;

        -- TEST No.2 ------------------------------------------------------------------------
		-- sib0 is closed
		-- Scanchain is 1 bits long 
		-- SO <- sib0 <- SI
		-- Reconfigure scanchain to include each scan register in m5 finishing with SR1	
		
        -- Open sib0 (set sib0 to value 1)
		shift_data("1");
		
		wait for HALF_SEPARATOR;
		
		-- sib1 and sib2 are now included in the scanchain
		-- Scanchain is 3 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- SI	
		
		-- Shift in TESTBYTE + "101" to open sib1 (set sib1 to value 1)
		shift_data(TESTBYTE&"101");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

		-- m6|sibTop and sib3 are included in the scanchain
		-- Scanchain is 5 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m6|sibTop <- SI	
		
		-- Shift in TESTBYTE + "10110" to open sib3 (set sib3 to value 1)
		shift_data(TESTBYTE&"10110");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

		-- sib4 and m2|sibTop are included in the scanchain
		-- Scanchain is 7 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m2|sibTop <- sib4 <- m6|sibTop <- SI
		
		-- Shift in TESTBYTE + "1011010" to open sib4 (set sib4 to value 1)
		shift_data(TESTBYTE&"1011010");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- sib5 and m5|sibTop are included in the scanchain
		-- Scanchain is 9 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m2|sibTop <- sib4 <- m5|sibTop <- sib5 <- m6|sibTop <- SI	
		
		-- Shift in TESTBYTE + "101101100" to open m5|sibTop (set m5|sibTop to value 1)
		shift_data(TESTBYTE&"101101100");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m5|sibWrp and m5|sib1 are included in the scanchain
		-- Scanchain is 11 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m2|sibTop <- sib4 <- m5|sibTop <- m5|sib1 <- m5|sibWrp <- sib5 <- m6|sibTop <- SI	
		
		-- Shift in TESTBYTE + "10110111000" to open m5|sib1 (set m5|sib1 to value 1)
		shift_data(TESTBYTE&"10110111000");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	
		
		-- m5|SCREG is included in the scanchain
		-- Scanchain is 14 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m2|sibTop <- sib4 <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|sibWrp <- 
		-- <- sib5 <- m6|sibTop <- SI	
		
		-- Shift in TESTBYTE + needed data to set m5|SCREG to "101" (select path "101" for m5|sMux)
		shift_data(TESTBYTE&
		           "101101"&
				   "11"&"101"&"0"&
				   "00");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m5|SR4 is included in the scanchain
		-- Scanchain is 135 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m2|sibTop <- sib4 <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR4 <- m5|sibWrp <- 
		-- <- sib5 <- m6|sibTop <- SI	
		
		-- Shift in TESTBYTE + needed data to set m5|SCREG to "011" (select path "011" for m5|sMux)
		shift_data(TESTBYTE&
		           "101101"&
				   "11"&"110"&all_zeroes(121)&"0"&
				   "00");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	
		
		-- m5|SR3 is included in the scanchain
		-- Scanchain is 135 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m2|sibTop <- sib4 <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR3 <- m5|sibWrp <- 
		-- <- sib5 <- m6|sibTop <- SI	
		
		-- Shift in TESTBYTE + needed data to set m5|SCREG to "010" (select path "010" for m5|sMux)
		shift_data(TESTBYTE&
		           "101101"&
				   "11"&"010"&all_zeroes(121)&"0"&
				   "00");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	
		
		-- m5|SR2 is included in the scanchain
		-- Scanchain is 135 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m2|sibTop <- sib4 <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR2 <- m5|sibWrp <- 
		-- <- sib5 <- m6|sibTop <- SI	
		
		-- Shift in TESTBYTE + needed data to set m5|SCREG to "001" (select path "001" for m5|sMux)
		shift_data(TESTBYTE&
		           "101101"&
				   "11"&"100"&all_zeroes(121)&"0"&
				   "00");
		
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	
		
		-- m5|SR1 is included in the scanchain
		-- Scanchain is 135 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m2|sibTop <- sib4 <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR1 <- m5|sibWrp <- 
		-- <- sib5 <- m6|sibTop <- SI	
		
		-- Shift in TESTBYTE + needed data to close m5|sibTop and m5|sib1 (set m5|sibTop and m5|sib1 to value 0)
		shift_data(TESTBYTE&
		           "101101"&
				   "00"&"100"&all_zeroes(120)&"0"&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m5 is excluded from the scanchain (only m5|sibTop remains)
		-- Scanchain is 9 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m2|sibTop <- sib4 <- 
		-- <- m5|sibTop <- 
		-- <- sib5 <- m6|sibTop <- SI	
		-- Open the longest path in all modules

		-- Shift in TESTBYTE + needed data to retain the scanchain
		shift_data(TESTBYTE&
		           "101101"&
				   '0'&
				   "00");
		
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.2 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.2 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.3 ------------------------------------------------------------------------
		-- Scanchain is 9 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m2|sibTop <- sib4 <- 
		-- <- m5|sibTop <- 
		-- <- sib5 <- m6|sibTop <- SI	
		-- Open the longest path in all modules

		-- Shift in TESTBYTE + needed data to open m2|sibTop (set m2|sibTop to value 1)
		shift_data(TESTBYTE&
		           "101111"&
				   '0'&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
				   
		-- m2|sib1 and m2|sibWrp are now adder to the scanchain
		-- Scanchain is 11 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- 
		-- <- m2|sibTop <- m2|sib1 <- m2|sibWrp <- 
		-- <- sib4 <- 
		-- <- m5|sibTop <- 
		-- <- sib5 <- m6|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open m2|sib1 (set m2|sib1 to value 1)
		shift_data(TESTBYTE&
		           "1011"&
				   "110"&
				   '1'&
				   '0'&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;				   
				   
		-- m2|SCREG is now added to the scanchain
		-- Scanchain is 13 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- 
		-- <- m2|sibTop <- m2|sib1 <- m2|SCREG <- m2|sibWrp <- 
		-- <- sib4 <- 
		-- <- m5|sibTop <- 
		-- <- sib5 <- m6|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to set m2|SCREG to "11" (to open path 1 for sMux1 and path 1 for sMux2)
		shift_data(TESTBYTE&
		           "1011"&
				   "11"&"11"&'0'&
				   '1'&
				   '0'&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;					   
				   
		-- m2|SR1 and m2|SR2 are now added to the scanchain
		-- Scanchain is 667 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- 
		-- <- m2|sibTop <- m2|sib1 <- m2|SCREG <- m2|(SR2+SR1) <- m2|sibWrp <- 
		-- <- sib4 <- 
		-- <- m5|sibTop <- 
		-- <- sib5 <- m6|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to close m2|sibTop and m2|sib1 (set m2|sibTop and m2|sib1 to value 0)
		shift_data(TESTBYTE&
		           "1011"&
				   "00"&"11"&all_zeroes(327)&all_zeroes(327)&'0'&
				   '1'&
				   '0'&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

		-- m2 is excluded from the scanchain (only m2|sibTop remains)
		-- Scanchain is 9 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- 
		-- <- m2|sibTop <- 
		-- <- sib4 <- 
		-- <- m5|sibTop <- 
		-- <- sib5 <- m6|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open sib5 (set sib5 to value 1)
		shift_data(TESTBYTE&
		           "1011"&
				   '0'&
				   '1'&
				   '0'&
				   "10");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

		-- m3|sibTop and sibM7 are now added to the scanchain
		-- Scanchain is 11 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- 
		-- <- m2|sibTop <- 
		-- <- sib4 <- 
		-- <- m5|sibTop <- 
		-- <- sib5 <- sibM7 <- m3|sibTop <- m6|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to close sib3 (set sib3 to value 0)
		shift_data(TESTBYTE&
		           "1010"&
				   '0'&
				   '1'&
				   '0'&
				   "1000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;		
		
		-- m2|sibTop, sib4, m5|sibTop, sib5, sibM7 and m3|sibTop are now excluded from the scanchain
		-- Scanchain is 5 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m6|sibTop <- SI	

		-- Shift in TESTBYTE + needed data to open m6|sibTop (set m6|sibTop to value 1)
		shift_data(TESTBYTE&"10101");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m6|sib1 and m6|sibWrp are now added to the scanchain
		-- Scanchain is 7 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- 
		-- <- m6|sibTop <- m6|sib1 <- m6|sibWrp <- 
		-- <- SI

		-- Shift in TESTBYTE + needed data to open m6|sib1 (set m6|sib1 to value 1)
		shift_data(TESTBYTE&
		           "1010"&
				   "110");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;		
		
		-- m6|sib2 and m6|SR1 are now added to the scanchain
		-- Scanchain is 192 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- 
		-- <- m6|sibTop <- m6|sib1 <- m6|sib2 <- m6|SR1 <- m6|sibWrp <- 
		-- <- SI

		-- Shift in TESTBYTE + needed data to open m6|sib2 (set m6|sib2 to value 1)
		shift_data(TESTBYTE&
		           "1010"&
				   "111"&all_zeroes(184)&'0');
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;			
		
		-- m6|sib3 and m6|SR2 are now added to the scanchain
		-- Scanchain is 377 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- 
		-- <- m6|sibTop <- m6|sib1 <- m6|sib2 <- m6|sib3 <- m6|(SR2+SR1) <- m6|sibWrp <- 
		-- <- SI

		-- Shift in TESTBYTE + needed data to open m6|sib3 (set m6|sib3 to value 1)
		shift_data(TESTBYTE&
		           "1010"&
				   "1111"&all_zeroes(184)&all_zeroes(184)&'0');
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;		
		
		-- m6|sib4 and m6|SR3 are now added to the scanchain
		-- Scanchain is 562 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- 
		-- <- m6|sibTop <- m6|sib1 <- m6|sib2 <- m6|sib3 <- m6|sib4 <- m6|(SR3+SR2+SR1) <- m6|sibWrp <- 
		-- <- SI

		-- Shift in TESTBYTE + needed data to open m6|sib4 (set m6|sib4 to value 1)
		shift_data(TESTBYTE&
		           "1010"&
				   "11111"&all_zeroes(184)&all_zeroes(184)&all_zeroes(184)&'0');
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;

		-- m6|SR4 is now added to the scanchain
		-- Scanchain is 747 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- 
		-- <- m6|sibTop <- m6|sib1 <- m6|sib2 <- m6|sib3 <- m6|sib4 <- m6|(SR4+SR3+SR2+SR1) <- m6|sibWrp <- 
		-- <- SI

		-- Shift in TESTBYTE + needed data to close m6|sibTop and m6|sib1 (set m6|sibTop and m6|sib1 to value 0)
		shift_data(TESTBYTE&
		           "1010"&
				   "00111"&all_zeroes(184)&all_zeroes(184)&all_zeroes(184)&all_zeroes(185)&'0');
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- m6|sib1, m6|sib2, m6|sib3, m6|sib4, m6|SR4, m6|SR3, m6|SR2, m6|SR1) and m6|sibWrp are now excluded from the scanchain
		-- Scanchain is 5 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- sib3 <- m6|sibTop <- SI

		-- Shift in TESTBYTE + needed data to close sib1 (set sib1 to value 0)
		shift_data(TESTBYTE&"10000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;
		
		-- sib3 and m6|sibTop are now excluded from the scanchain
		-- Scanchain is 3 bits long 
		-- SO <- sib0 <- sib2 <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open sib2 (set sib2 to value 1)
		shift_data(TESTBYTE&"110");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;		
		
		-- m8|sibTop, m4|sibTop and m1|sibTop are now added to the scanchain
		-- Scanchain is 6 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- m4|sibTop <- m1|sibTop <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m1|sibTop (set m1|sibTop to value 1)
		shift_data(TESTBYTE&"110010");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	
		
		-- m1|sib1 and m1|sibWrp are now added to the scanchain
		-- Scanchain is 8 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- m4|sibTop <- 
		-- <- m1|sibTop <- m1|sib1 <- m1|sibWrp <- 
		-- <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m1|sib1 (set m1|sib1 to value 1)
		shift_data(TESTBYTE&
		           "1100"&
		           "110"&
				   '0');
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;			
		
		-- m1|SR1 and m1|sib2 are now added to the scanchain
		-- Scanchain is 357 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- m4|sibTop <- 
		-- <- m1|sibTop <- m1|sib2 <- m1|SR1 <- m2|sib1 <- m1|sibWrp <- 
		-- <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m1|sib2 (set m1|sib2 to value 1)
		shift_data(TESTBYTE&
		           "1100"&
		           "11"&all_zeroes(348)&"10"&
				   '0');
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;			
		
		-- m1|SR2 and m1|sib3 are now added to the scanchain
		-- Scanchain is 706 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- m4|sibTop <- 
		-- <- m1|sibTop <- m1|sib3 <- m1|SR2 <- m1|sib2 <- m1|SR1 <- m2|sib1 <- m1|sibWrp <- 
		-- <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m1|sib3 (set m1|sib3 to value 1)
		shift_data(TESTBYTE&
		           "1100"&
		           "11"&all_zeroes(348)&'1'&all_zeroes(348)&"10"&
				   '0');
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;			
		
		-- m1|SR3 and m1|sib4 are now added to the scanchain
		-- Scanchain is 1055 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- m4|sibTop <- 
		-- <- m1|sibTop <- m1|sib4 <- m1|SR3 <- m1|sib3 <- m1|SR2 <- m1|sib2 <- m1|SR1 <- m2|sib1 <- m1|sibWrp <- 
		-- <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m1|sib4 (set m1|sib4 to value 1)
		shift_data(TESTBYTE&
		           "1100"&
		           "11"&all_zeroes(348)&'1'&all_zeroes(348)&'1'&all_zeroes(348)&"10"&
				   '0');
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m1|SR4 is now added to the scanchain
		-- Scanchain is 1140 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- m4|sibTop <- 
		-- <- m1|sibTop <- m1|SR4 <- m1|sib4 <- m1|SR3 <- m1|sib3 <- m1|SR2 <- m1|sib2 <- m1|SR1 <- m2|sib1 <- m1|sibWrp <- 
		-- <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to close m1|sibTop and m1|sib1 (set m1|sibTop and m1|sib1 to value 0)
		shift_data(TESTBYTE&
		           "1100"&
		           '0'&all_zeroes(85)&'1'&all_zeroes(348)&'1'&all_zeroes(348)&'1'&all_zeroes(348)&"00"&
				   '0');
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m2|sib1, m1|sib2, m1|sib3, m1|sib4, m1|SR4, m1|SR3, m1|SR2, m1|SR1 and m1|sibWrp are now excluded from the scanchain
		-- Scanchain is 6 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- m4|sibTop <- m1|sibTop <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m4|sibTop (set m4|sibTop to value 1)
		shift_data(TESTBYTE&"110100");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m4|sib1 and m4|sibWrp are now added to the scanchain
		-- Scanchain is 8 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sibWrp <- 
		-- <- m1|sibTop <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m4|sib1 (set m4|sib1 to value 1)
		shift_data(TESTBYTE&
		           "110"&
		           "110"&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;		
		
		-- m4|SR1 and m4|sib2 are now added to the scanchain
		-- Scanchain is 30 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|SR1 <- m4|sibWrp <- 
		-- <- m1|sibTop <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m4|sib2 (set m4|sib2 to value 1)
		shift_data(TESTBYTE&
		           "110"&
		           "111"&all_zeroes(21)&'0'&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");	

		wait for HALF_SEPARATOR;		
		
		-- m4|SR2 and m4|sib3 are now added to the scanchain
		-- Scanchain is 52 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|sib3 <- m4|(SR2+SR1) <- m4|sibWrp <- 
		-- <- m1|sibTop <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m4|sib3 (set m4|sib3 to value 1)
		shift_data(TESTBYTE&
		           "110"&
		           "1111"&all_zeroes(21)&all_zeroes(21)&'0'&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");	

		wait for HALF_SEPARATOR;		
		
		-- m4|SR3 and m4|sib4 are now added to the scanchain
		-- Scanchain is 74 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|sib3 <- m4|sib4 <- m4|(SR3+SR2+SR1) <- m4|sibWrp <- 
		-- <- m1|sibTop <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m4|sib4 (set m4|sib4 to value 1)
		shift_data(TESTBYTE&
		           "110"&
		           "11111"&all_zeroes(21)&all_zeroes(21)&all_zeroes(21)&'0'&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");	

		wait for HALF_SEPARATOR;		
		
		-- m4|SR4 is now added to the scanchain
		-- Scanchain is 95 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|sib3 <- m4|sib4 <- m4|(SR4+SR3+SR2+SR1) <- m4|sibWrp <- 
		-- <- m1|sibTop <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to close m4|sibTop and m4|sib1 (set m4|sibTop and m4|sib1 to value 0)
		shift_data(TESTBYTE&
		           "110"&
		           "00111"&all_zeroes(21)&all_zeroes(21)&all_zeroes(21)&all_zeroes(21)&'0'&
				   "00");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");		

		wait for HALF_SEPARATOR;		
		
		-- m4|sib1, m4|sib2, m4|sib3, m4|sib4, m4|SR4, m4|SR3, m4|SR2, m4|SR1 and m4|sibWrp are now excluded from the scanchain
		-- Scanchain is 6 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- m4|sibTop <- m1|sibTop <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m8|sibTop (set m8|sibTop to value 1)
		shift_data(TESTBYTE&"111000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m8|sib5, m8|sib1 and m8|sibWrp are now added to the scanchain
		-- Scanchain is 9 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib1 <- m8|sibWrp <- 
		-- <- m4|sibTop <- m1|sibTop <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m8|sib1 (set m8|sib1 to value 1)
		shift_data(TESTBYTE&
		           "11"&
		           "1010"&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m8|SR1 and m8|sib2 are now added to the scanchain
		-- Scanchain is 198 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib1 <- m8|sib2 <- m8|SR1 <- m8|sibWrp <- 
		-- <- m4|sibTop <- m1|sibTop <- sib1 <- SI		

		-- Shift in TESTBYTE + needed data to open m8|sib2 (set m8|sib2 to value 1)
		shift_data(TESTBYTE&
		           "11"&
		           "1011"&all_zeroes(188)&'0'&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");	

		wait for HALF_SEPARATOR;	

		-- m8|SR2 and m8|sib3 are now added to the scanchain
		-- Scanchain is 387 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib1 <- m8|sib2 <- m8|sib3 <- m8|(SR2+SR1) <- m8|sibWrp <- 
		-- <- m4|sibTop <- m1|sibTop <- sib1 <- SI		

		-- Shift in TESTBYTE + needed data to open m8|sib3 (set m8|sib3 to value 1)
		shift_data(TESTBYTE&
		           "11"&
		           "10111"&all_zeroes(188)&all_zeroes(188)&'0'&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");	

		wait for HALF_SEPARATOR;	

		-- m8|SR3 and m8|sib4 are now added to the scanchain
		-- Scanchain is 576 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib1 <- m8|sib2 <- m8|sib3 <- m8|sib4 <- m8|(SR3+SR2+SR1) <- m8|sibWrp <- 
		-- <- m4|sibTop <- m1|sibTop <- sib1 <- SI		

		-- Shift in TESTBYTE + needed data to open m8|sib4 (set m8|sib4 to value 1)
		shift_data(TESTBYTE&
		           "11"&
		           "101111"&all_zeroes(188)&all_zeroes(188)&all_zeroes(188)&'0'&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");	

		wait for HALF_SEPARATOR;	

		-- m8|SR4 is now added to the scanchain
		-- Scanchain is 764 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib1 <- m8|sib2 <- m8|sib3 <- m8|sib4 <- m8|(SR4+SR3+SR2+SR1) <- m8|sibWrp <- 
		-- <- m4|sibTop <- m1|sibTop <- sib1 <- SI		

		-- Shift in TESTBYTE + needed data to close m8|sib1 (set m8|sib1 to value 0)
		shift_data(TESTBYTE&
		           "11"&
		           "100111"&all_zeroes(188)&all_zeroes(188)&all_zeroes(188)&all_zeroes(188)&'0'&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");	

		wait for HALF_SEPARATOR;
		
		-- m8|sib2, m8|sib3, m8|sib4, m8|SR4, m8|SR3, m8|SR2  and m8|SR1 are now excluded from the scanchain
		-- Scanchain is 9 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib1 <- m8|sibWrp <- 
		-- <- m4|sibTop <- m1|sibTop <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open m8|sib5 (set m8|sib5 to value 1)
		shift_data(TESTBYTE&
		           "11"&
		           "1100"&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;			
		
		-- m8|SR5 and m8|sib6 are now added to the scanchain
		-- Scanchain is 198 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib6 <- m8|SR5 <- m8|sib1 <- m8|sibWrp <- 
		-- <- m4|sibTop <- m1|sibTop <- sib1 <- SI		

		-- Shift in TESTBYTE + needed data to open m8|sib6 (set m8|sib6 to value 1)
		shift_data(TESTBYTE&
		           "11"&
		           "111"&all_zeroes(188)&"00"&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");	

		wait for HALF_SEPARATOR;			
		
		-- m8|SR6 and m8|sib7 are now added to the scanchain
		-- Scanchain is 388 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib6 <- m8|sib7 <- m8|(SR6+SR5) <- m8|sib1 <- m8|sibWrp <- 
		-- <- m4|sibTop <- m1|sibTop <- sib1 <- SI		

		-- Shift in TESTBYTE + needed data to open m8|sib7 (set m8|sib7 to value 1)
		shift_data(TESTBYTE&
		           "11"&
		           "1111"&all_zeroes(189)&all_zeroes(188)&"00"&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");	

		wait for HALF_SEPARATOR;			
		
		-- m8|SR7 and m8|sib8 are now added to the scanchain
		-- Scanchain is 578 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib6 <- m8|sib7 <- m8|sib8 <- m8|(SR7+SR6+SR5) <- m8|sib1 <- m8|sibWrp <- 
		-- <- m4|sibTop <- m1|sibTop <- sib1 <- SI		

		-- Shift in TESTBYTE + needed data to open m8|sib8 (set m8|sib8 to value 1)
		shift_data(TESTBYTE&
		           "11"&
		           "11111"&all_zeroes(189)&all_zeroes(189)&all_zeroes(188)&"00"&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");	

		wait for HALF_SEPARATOR;			
		
		-- m8|SR8 is now added to the scanchain
		-- Scanchain is 767 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib6 <- m8|sib7 <- m8|sib8 <- m8|(SR8+SR7+SR6+SR5) <- m8|sib1 <- m8|sibWrp <- 
		-- <- m4|sibTop <- m1|sibTop <- sib1 <- SI		

		-- Shift in TESTBYTE + needed data to close m8|sibTop and m8|sib5 (set m8|sibTop and m8|sib5 to value 0)
		shift_data(TESTBYTE&
		           "11"&
		           "00111"&all_zeroes(189)&all_zeroes(189)&all_zeroes(189)&all_zeroes(188)&"00"&
				   "000");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");	

		wait for HALF_SEPARATOR;		

		-- m8|sib5, m8|sib6, m8|sib7, m8|sib8, m8|SR8, m8|SR7, m8|SR6, m8|SR5, m8|sib1 and m8|sibWrp are now excluded from the scanchain
		-- Scanchain is 6 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- m4|sibTop <- m1|sibTop <- sib1 <- SI

		-- Shift in TESTBYTE + needed data to open sib1 (set sib1 to value 1)
		shift_data(TESTBYTE&"110001");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	
		
		-- m6|sibTop and sib3 are now added to the scanchain
		-- Scanchain is 8 bits long 
		-- SO <- sib0 <- sib2 <- m8|sibTop <- m4|sibTop <- m1|sibTop <- sib1 <- sib3 <- m6|sibTop <- SI

		-- Shift in TESTBYTE + needed data to open sib3 (set sib3 to value 1)
		shift_data(TESTBYTE&"11000110");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;			

		-- m3|sibTop, sibM7, sib5, m5|sibTop, sib4 and m2|sibTop are now added to the scanchain
		-- Scanchain is 14 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- 
		-- <- m4|sibTop <- 
		-- <- m1|sibTop <- 
		-- <- sib1 <- sib3 <- 
		-- <- m2|sibTop <- 
		-- <- sib4 <- 
		-- <- m5|sibTop <- 
		-- <- sib5 <- 
		-- <- sibM7 <- 
		-- <- m3|sibTop <- 
		-- <- m6|sibTop <- SI

		-- Shift in TESTBYTE + needed data to open sibTop-s for all modules (set sibTop-s to value 1)
		shift_data(TESTBYTE&
		           "11"&
				   '1'&
				   '1'&
				   '1'&
				   "11"&
				   '1'&
				   '1'&
				   '1'&
				   '1'&
				   '0'&
				   '1'&
				   '1');
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m6|sib1, m6|sibWrp, m6|SR2, m6|SR1, m3|sibWrp, m5|sib1, m5|sibWrp, m2|sib1, m2|sibWrp, 
		-- m1|sib1, m1|sibWrp, m4|sib1, m4|sibWrp, m8|sib1 and m8|sibWrp are now added to the scanchain
		-- Scanchain is 89 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib1 <- m8|sibWrp <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sibWrp <- 
		-- <- m1|sibTop <- m1|sib1 <- m1|sibWrp <- 
		-- <- sib1 <- sib3 <- 
		-- <- m2|sibTop <- m2|sib1 <- m2|sibWrp <- 
		-- <- sib4 <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|sibWrp <- 
		-- <- sib5 <- 
		-- <- sibM7 <- 
		-- <- m3|sibTop <- m6|(SR2+SR1) <- m3|sibWrp <-
		-- <- m6|sibTop <- m6|sib1 <- m6|sibWrp <- SI

		-- Shift in TESTBYTE + needed data to open remaining sibs (set remaining sibs to value 1)
		shift_data(TESTBYTE&
		           "11"&
				   "1111"&
				   "111"&
				   "111"&
				   "11"&
				   "111"&
				   '1'&
				   "111"&
				   '1'&
				   '1'&
				   '1'&all_zeroes(32)&all_zeroes(31)&'1'&
				   "111");
				   
		-- Check that TESTBYTE has appeared from SO
        data_check(TESTBYTE,"assert");
		
		wait for HALF_SEPARATOR;	

		-- m6|wrpOut, m6|wrpIn, m6|sib2, m6|sib3, m6|sib4, m6|SR4, m6|SR3, m6|SR2, m6|SR1, m3|wrpOut, m3|wrpIn, m7|wrpOut, m7|wrpIn,
		-- m5|wrpOut, m5|wrpIn, m5|SCREG, m5|SR1, m2|wrpOut, m2|wrpIn, m2|SCREG, m2|SR2, m2|SR1, 
		-- m1|wrpOut, m1|wrpIn, m1|sib2, m1|sib3, m1|sib4, m1|SR4, m1|SR3, m1|SR2, m1|SR1,
		-- m4|wrpOut, m4|wrpIn, m4|sib2, m4|sib3, m4|sib4, m4|SR4, m4|SR3, m4|SR2, m4|SR1,
		-- m8|wrpOut, m8|wrpIn, m8|sib2, m8|sib3, m8|sib4, m8|SR4, m8|SR3, m8|SR2, m8|SR1, 
		-- m8|sib6, m8|sib7, m8|sib8, m8|SR8, m8|SR7, m8|SR6, m8|SR5 are now added to the scanchain
		-- Scanchain is 5218 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib6 <- m8|sib7 <- m8|sib8 <- m8|(SR8+SR7+SR6+SR5) <-  
		-- <- m8|sib1 <- m8|sib2 <- m8|sib3 <- m8|sib4 <- m8|(SR4+SR3+SR2+SR1) <- m8|sibWrp <- m8|wrpOut <- m8|wrpIn <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|sib3 <- m4|sib4 <- m4|(SR4+SR3+SR2+SR1) <- m4|sibWrp <- m4|wrpOut <- m4|wrpIn <- 
		-- <- m1|sibTop <- m1|SR4 <- m1|sib4 <- m1|SR3 <- m1|sib3 <- m1|SR2 <- m1|sib2 <- m1|SR1 <- m2|sib1 <- m1|sibWrp <- m1|wrpOut <- m1|wrpIn <- 
		-- <- sib1 <- sib3 <- 
		-- <- m2|sibTop <- m2|sib1 <- m2|SCREG <- m2|(SR2+SR1) <- m2|sibWrp <- m2|wrpOut <- m2|wrpIn <- 
		-- <- sib4 <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR1 <- m5|sibWrp <- m5|wrpOut <- m5|wrpIn <- 
		-- <- sib5 <- 
		-- <- sibM7 <- m7|wrpOut <- m7|wrpIn <- 
		-- <- m3|sibTop <- m6|(SR2+SR1) <- m3|sibWrp <- m3|wrpOut <- m3|wrpIn <-
		-- <- m6|sibTop <- m6|sib1 <- m6|sib2 <- m6|sib3 <- m6|sib4 <- m6|(SR4+SR3+SR2+SR1) <- m6|sibWrp <- m6|wrpOut <- m6|wrpIn <- SI

		-- Shift in TESTBYTE + needed data to retain the scanchain
		shift_data(TESTBYTE&
		           "11"&
				   "11111"&all_zeroes(189)&all_zeroes(189)&all_zeroes(189)&all_zeroes(188)&
				   "1111"&all_zeroes(188)&all_zeroes(188)&all_zeroes(188)&all_zeroes(188)&'1'&all_zeroes(69)&all_zeroes(35)&
				   "11111"&all_zeroes(21)&all_zeroes(21)&all_zeroes(21)&all_zeroes(21)&'1'&all_zeroes(67)&all_zeroes(88)&
				   '1'&all_zeroes(85)&'1'&all_zeroes(348)&'1'&all_zeroes(348)&'1'&all_zeroes(348)&"11"&all_zeroes(152)&all_zeroes(112)&
				   "11"&
				   "11"&"11"&all_zeroes(327)&all_zeroes(327)&'1'&all_zeroes(89)&all_zeroes(68)&
				   '1'&
				   "11"&"100"&all_zeroes(120)&'1'&all_zeroes(13)&all_zeroes(19)&
				   '1'&
				   '1'&all_zeroes(32)&all_zeroes(80)&
				   '1'&all_zeroes(32)&all_zeroes(31)&'1'&all_zeroes(17)&all_zeroes(9)&
				   "11111"&all_zeroes(185)&all_zeroes(184)&all_zeroes(184)&all_zeroes(184)&'1'&all_zeroes(11)&all_zeroes(15));
				   				
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.3 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.3 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
        -- TEST No.4 ------------------------------------------------------------------------
		-- Scanchain is 5218 bits long 
		-- SO <- sib0 <- sib2 <- 
		-- <- m8|sibTop <- m8|sib5 <- m8|sib6 <- m8|sib7 <- m8|sib8 <- m8|(SR8+SR7+SR6+SR5) <-  
		-- <- m8|sib1 <- m8|sib2 <- m8|sib3 <- m8|sib4 <- m8|(SR4+SR3+SR2+SR1) <- m8|sibWrp <- m8|wrpOut <- m8|wrpIn <- 
		-- <- m4|sibTop <- m4|sib1 <- m4|sib2 <- m4|sib3 <- m4|sib4 <- m4|(SR4+SR3+SR2+SR1) <- m4|sibWrp <- m4|wrpOut <- m4|wrpIn <- 
		-- <- m1|sibTop <- m1|SR4 <- m1|sib4 <- m1|SR3 <- m1|sib3 <- m1|SR2 <- m1|sib2 <- m1|SR1 <- m2|sib1 <- m1|sibWrp <- m1|wrpOut <- m1|wrpIn <- 
		-- <- sib1 <- sib3 <- 
		-- <- m2|sibTop <- m2|sib1 <- m2|SCREG <- m2|(SR2+SR1) <- m2|sibWrp <- m2|wrpOut <- m2|wrpIn <- 
		-- <- sib4 <- 
		-- <- m5|sibTop <- m5|sib1 <- m5|SCREG <- m5|SR1 <- m5|sibWrp <- m5|wrpOut <- m5|wrpIn <- 
		-- <- sib5 <- 
		-- <- sibM7 <- m7|wrpOut <- m7|wrpIn <- 
		-- <- m3|sibTop <- m6|(SR2+SR1) <- m3|sibWrp <- m3|wrpOut <- m3|wrpIn <-
		-- <- m6|sibTop <- m6|sib1 <- m6|sib2 <- m6|sib3 <- m6|sib4 <- m6|(SR4+SR3+SR2+SR1) <- m6|sibWrp <- m6|wrpOut <- m6|wrpIn <- SI
		-- Repeat the last shift in order to check that scanchain remained the same

		-- Shift in TESTBYTE + needed data to retain the scanchain
		shift_data(TESTBYTE&
		           "11"&
				   "11111"&all_zeroes(189)&all_zeroes(189)&all_zeroes(189)&all_zeroes(188)&
				   "1111"&all_zeroes(188)&all_zeroes(188)&all_zeroes(188)&all_zeroes(188)&'1'&all_zeroes(69)&all_zeroes(35)&
				   "11111"&all_zeroes(21)&all_zeroes(21)&all_zeroes(21)&all_zeroes(21)&'1'&all_zeroes(67)&all_zeroes(88)&
				   '1'&all_zeroes(85)&'1'&all_zeroes(348)&'1'&all_zeroes(348)&'1'&all_zeroes(348)&"11"&all_zeroes(152)&all_zeroes(112)&
				   "11"&
				   "11"&"11"&all_zeroes(327)&all_zeroes(327)&'1'&all_zeroes(89)&all_zeroes(68)&
				   '1'&
				   "11"&"100"&all_zeroes(120)&'1'&all_zeroes(13)&all_zeroes(19)&
				   '1'&
				   '1'&all_zeroes(32)&all_zeroes(80)&
				   '1'&all_zeroes(32)&all_zeroes(31)&'1'&all_zeroes(17)&all_zeroes(9)&
				   "11111"&all_zeroes(185)&all_zeroes(184)&all_zeroes(184)&all_zeroes(184)&'1'&all_zeroes(11)&all_zeroes(15));
				   				
		-- Check that TESTBYTE has appeared from SO
		report "TEST No.4 is finished";
        data_check(TESTBYTE,"report");
		
		-- END of TEST No.4 -----------------------------------------------------------------			
		
		wait for FULL_SEPARATOR;
		
		-- STOP SIMULATION ------------------------------------------------------------------
        halt_simulation <= '1';
        wait;
        
   end process;

END;
