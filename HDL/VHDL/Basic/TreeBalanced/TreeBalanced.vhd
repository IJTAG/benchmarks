-- Copyright Testonica Lab (C) 2016

-- History:
---------------+------------+-------------------------+------------------------
--  Version    |  Date      | Author                  | Organization
---------------+------------+-------------------------+------------------------
--         1.0 | 28.11.2016 | Dmitri Mihhailov        | Testonica Lab
---------------+------------+-------------------------+------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TreeBalanced is
    Port ( -- iJTAG interface
	       SI : in STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort	
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC); -- ScanOutPort
end TreeBalanced;

architecture TreeBalanced_arch of TreeBalanced is

component SIB_mux_pre is
    Port ( -- Scan Interface  client --------------
	       SI : in  STD_LOGIC; -- ScanInPort 
           CE : in STD_LOGIC; -- CaptureEnPort
           SE : in STD_LOGIC; -- ShiftEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           SEL : in STD_LOGIC; -- SelectPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
           SO : out STD_LOGIC; -- ScanOutPort
		   -- Scan Interface  host ----------------
           fromSO : in  STD_LOGIC; -- ScanInPort
           toCE : out  STD_LOGIC; -- ToCaptureEnPort
           toSE : out  STD_LOGIC; -- ToShiftEnPort
           toUE : out  STD_LOGIC; -- ToUpdateEnPort
           toSEL : out  STD_LOGIC; -- ToSelectPort
           toRST : out  STD_LOGIC; -- ToResetPort
           toTCK : out  STD_LOGIC; -- ToTCKPort
           toSI : out  STD_LOGIC); -- ScanOutPort
end component;

signal sib0_so, sib1_so, sib2_so, sib3_so, sib4_so, sib5_so, sibM7_so : std_logic;
signal sib0_toCE, sib1_toCE, sib2_toCE, sib3_toCE, sib4_toCE, sib5_toCE, sibM7_toCE : std_logic;
signal sib0_toSE, sib1_toSE, sib2_toSE, sib3_toSE, sib4_toSE, sib5_toSE, sibM7_toSE : std_logic;
signal sib0_toUE, sib1_toUE, sib2_toUE, sib3_toUE, sib4_toUE, sib5_toUE, sibM7_toUE : std_logic;
signal sib0_toSEL, sib1_toSEL, sib2_toSEL, sib3_toSEL, sib4_toSEL, sib5_toSEL, sibM7_toSEL : std_logic;
signal sib0_toRST, sib1_toRST, sib2_toRST, sib3_toRST, sib4_toRST, sib5_toRST, sibM7_toRST : std_logic;
signal sib0_toTCK, sib1_toTCK, sib2_toTCK, sib3_toTCK, sib4_toTCK, sib5_toTCK, sibM7_toTCK : std_logic;
signal sib0_toSI, sib1_toSI, sib2_toSI, sib3_toSI, sib4_toSI, sib5_toSI, sibM7_toSI : std_logic;

component H953_Basic_M1 is
 Generic ( inputs : positive := 112; -- Parameter inputs = 112
           outputs : positive := 152); -- Parameter outputs = 152
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m1_so : STD_LOGIC;

component H953_Basic_M2 is
 Generic ( inputs : positive := 68; -- Parameter inputs = 68
           outputs : positive := 89); -- Parameter outputs = 89
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m2_so : STD_LOGIC;

component H953_Basic_M3 is
 Generic ( inputs : positive := 9; -- Parameter inputs = 9
           outputs : positive := 17); -- Parameter outputs = 17
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m3_so : STD_LOGIC;

component H953_Basic_M4 is
 Generic ( inputs : positive := 88; -- Parameter inputs = 88
           outputs : positive := 67); -- Parameter outputs = 67
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m4_so : STD_LOGIC;

component H953_Basic_M5 is
 Generic ( inputs : positive := 19; -- Parameter inputs = 19
           outputs : positive := 13); -- Parameter outputs = 13
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m5_so : STD_LOGIC;

component H953_Basic_M6 is
 Generic ( inputs : positive := 15; -- Parameter inputs = 15
           outputs : positive := 11); -- Parameter outputs = 11
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m6_so : STD_LOGIC;

component H953_Basic_M8 is
 Generic ( inputs : positive := 35; -- Parameter inputs = 35
           outputs : positive := 69); -- Parameter outputs = 69
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC; -- TCKPort
		   DI : in STD_LOGIC_VECTOR(inputs-1 downto 0); --DataInPort
		   toDO : in STD_LOGIC_VECTOR(outputs-1 downto 0); --DataInPort
		   DO : out STD_LOGIC_VECTOR(outputs-1 downto 0)); --DataOutPort
end component;

signal m8_so : STD_LOGIC;


component EmptyModule_NoBidirs is
 Generic ( inputs : positive := 1; -- Parameter inputs = 1
           outputs : positive := 1;-- Parameter outputs = 1
		   size : positive := inputs + outputs); -- Parameter inputs = $inputs + $outputs
    Port ( -- Scan Interface scan_client ----------
	       SI : in STD_LOGIC; -- ScanInPort 
           SO : out STD_LOGIC; -- ScanOutPort
           SEL : in STD_LOGIC; -- SelectPort
           ----------------------------------------		
           SE : in STD_LOGIC; -- ShiftEnPort
           CE : in STD_LOGIC; -- CaptureEnPort
           UE : in STD_LOGIC; -- UpdateEnPort
           RST : in STD_LOGIC; -- ResetPort
           TCK : in STD_LOGIC); -- TCKPort
end component;

signal m7_so : STD_LOGIC;

begin

SO <= sib0_so; -- Source sib0.SO

-- Level 1
sib0 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => SI, --InputPort SI = SI
               CE => CE,
               SE => SE,
               UE => UE,
               SEL => SEL,
               RST => RST,
               TCK => TCK,
               SO => sib0_so,
		         -- Scan Interface  host ----------------
               fromSO => sib2_so, --InputPort fromSO = sib2.SO
               toCE => sib0_toCE,
               toSE => sib0_toSE,
               toUE => sib0_toUE,
               toSEL => sib0_toSEL,
               toRST => sib0_toRST,
               toTCK => sib0_toTCK,
               toSI => sib0_toSI);

-- Level 2   
sib1 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => sib0_toSI, --InputPort SI = sib0.toSI
               CE => sib0_toCE,
               SE => sib0_toSE,
               UE => sib0_toUE,
               SEL => sib0_toSEL,
               RST => sib0_toRST,
               TCK => sib0_toTCK,
               SO => sib1_so,
		         -- Scan Interface  host ----------------
               fromSO => sib3_so, --InputPort fromSO = sib3.SO
               toCE => sib1_toCE,
               toSE => sib1_toSE,
               toUE => sib1_toUE,
               toSEL => sib1_toSEL,
               toRST => sib1_toRST,
               toTCK => sib1_toTCK,
               toSI => sib1_toSI);
			   
sib2 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => sib1_so, --InputPort SI = sib1.SO
               CE => sib0_toCE,
               SE => sib0_toSE,
               UE => sib0_toUE,
               SEL => sib0_toSEL,
               RST => sib0_toRST,
               TCK => sib0_toTCK,
               SO => sib2_so,
		         -- Scan Interface  host ----------------
               fromSO => m8_so, --InputPort fromSO = m8.SO
               toCE => sib2_toCE,
               toSE => sib2_toSE,
               toUE => sib2_toUE,
               toSEL => sib2_toSEL,
               toRST => sib2_toRST,
               toTCK => sib2_toTCK,
               toSI => sib2_toSI);
			   
-- Level 3
m6 : H953_Basic_M6
    Port map ( -- Scan Interface scan_client ----------
	           SI => sib1_toSI, -- InputPort SI = sib1.toSI
               SO => m6_so,
               SEL => sib1_toSEL,
               ----------------------------------------		
               SE => sib1_toSE,
               CE => sib1_toCE,
               UE => sib1_toUE,
               RST => sib1_toRST,
               TCK => sib1_toTCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);
			   	   
sib3 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => m6_so, --InputPort SI = m6.SO
               CE => sib1_toCE,
               SE => sib1_toSE,
               UE => sib1_toUE,
               SEL => sib1_toSEL,
               RST => sib1_toRST,
               TCK => sib1_toTCK,
               SO => sib3_so,
		         -- Scan Interface  host ----------------
               fromSO => m2_so, --InputPort fromSO = m2.SO
               toCE => sib3_toCE,
               toSE => sib3_toSE,
               toUE => sib3_toUE,
               toSEL => sib3_toSEL,
               toRST => sib3_toRST,
               toTCK => sib3_toTCK,
               toSI => sib3_toSI);	

m1 : H953_Basic_M1
    Port map ( -- Scan Interface scan_client ----------
	           SI => sib2_toSI, -- InputPort SI = sib2.toSI
               SO => m1_so,
               SEL => sib2_toSEL,
               ----------------------------------------		
               SE => sib2_toSE,
               CE => sib2_toCE,
               UE => sib2_toUE,
               RST => sib2_toRST,
               TCK => sib2_toTCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);		

m4 : H953_Basic_M4
    Port map ( -- Scan Interface scan_client ----------
	           SI => m1_so, -- InputPort SI = m1.SO
               SO => m4_so,
               SEL => sib2_toSEL,
               ----------------------------------------		
               SE => sib2_toSE,
               CE => sib2_toCE,
               UE => sib2_toUE,
               RST => sib2_toRST,
               TCK => sib2_toTCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);		

m8 : H953_Basic_M8
    Port map ( -- Scan Interface scan_client ----------
	           SI => m4_so, -- InputPort SI = m4.SO
               SO => m8_so,
               SEL => sib2_toSEL,
               ----------------------------------------		
               SE => sib2_toSE,
               CE => sib2_toCE,
               UE => sib2_toUE,
               RST => sib2_toRST,
               TCK => sib2_toTCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	

-- Level 4		   	   
sib4 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => sib3_toSI, --InputPort SI = sib3.toSI
               CE => sib3_toCE,
               SE => sib3_toSE,
               UE => sib3_toUE,
               SEL => sib3_toSEL,
               RST => sib3_toRST,
               TCK => sib3_toTCK,
               SO => sib4_so,
		         -- Scan Interface  host ----------------
               fromSO => m5_so, --InputPort fromSO = m5.SO
               toCE => sib4_toCE,
               toSE => sib4_toSE,
               toUE => sib4_toUE,
               toSEL => sib4_toSEL,
               toRST => sib4_toRST,
               toTCK => sib4_toTCK,
               toSI => sib4_toSI);	

m2 : H953_Basic_M2
    Port map ( -- Scan Interface scan_client ----------
	           SI => sib4_so, -- InputPort SI = sib4.SO
               SO => m2_so,
               SEL => sib3_toSEL,
               ----------------------------------------		
               SE => sib3_toSE,
               CE => sib3_toCE,
               UE => sib3_toUE,
               RST => sib3_toRST,
               TCK => sib3_toTCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	

-- Level 5		   	   
sib5 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => sib4_toSI, --InputPort SI = sib4.toSI
               CE => sib4_toCE,
               SE => sib4_toSE,
               UE => sib4_toUE,
               SEL => sib4_toSEL,
               RST => sib4_toRST,
               TCK => sib4_toTCK,
               SO => sib5_so,
		         -- Scan Interface  host ----------------
               fromSO => sibM7_so, --InputPort fromSO = sibM7.SO
               toCE => sib5_toCE,
               toSE => sib5_toSE,
               toUE => sib5_toUE,
               toSEL => sib5_toSEL,
               toRST => sib5_toRST,
               toTCK => sib5_toTCK,
               toSI => sib5_toSI);	

m5 : H953_Basic_M5
    Port map ( -- Scan Interface scan_client ----------
	           SI => sib5_so, -- InputPort SI = sib5.SO
               SO => m5_so,
               SEL => sib4_toSEL,
               ----------------------------------------		
               SE => sib4_toSE,
               CE => sib4_toCE,
               UE => sib4_toUE,
               RST => sib4_toRST,
               TCK => sib4_toTCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	

-- Level 6 and lower		   	   
m3 : H953_Basic_M3
    Port map ( -- Scan Interface scan_client ----------
	           SI => sib5_toSI, -- InputPort SI = sib5.toSI
               SO => m3_so,
               SEL => sib5_toSEL,
               ----------------------------------------		
               SE => sib5_toSE,
               CE => sib5_toCE,
               UE => sib5_toUE,
               RST => sib5_toRST,
               TCK => sib5_toTCK,
		       DI => (others => '0'),
		       toDO => (others => '0'),
		       DO => open);	

sibM7 : SIB_mux_pre
    Port map ( -- Scan Interface  client --------------
	           SI => m3_so, --InputPort SI = m3.SO
               CE => sib5_toCE,
               SE => sib5_toSE,
               UE => sib5_toUE,
               SEL => sib5_toSEL,
               RST => sib5_toRST,
               TCK => sib5_toTCK,
               SO => sibM7_so,
		         -- Scan Interface  host ----------------
               fromSO => m7_so, --InputPort fromSO = m7.SO
               toCE => sibM7_toCE,
               toSE => sibM7_toSE,
               toUE => sibM7_toUE,
               toSEL => sibM7_toSEL,
               toRST => sibM7_toRST,
               toTCK => sibM7_toTCK,
               toSI => sibM7_toSI);		

m7 : EmptyModule_NoBidirs
 Generic map ( inputs => 80, -- Parameter inputs = 80
               outputs => 32) -- Parameter outputs = 32
    Port map ( -- Scan Interface scan_client ----------
	           SI => sibM7_toSI, -- InputPort SI = sibM7.toSI
               SO => m7_so,
               SEL => sibM7_toSEL,
               ----------------------------------------		
               SE => sibM7_toSE,
               CE => sibM7_toCE,
               UE => sibM7_toUE,
               RST => sibM7_toRST,
               TCK => sibM7_toTCK);	

end TreeBalanced_arch;