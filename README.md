# IEEE 1687 Std. Benchmarks

The Suite of IEEE Std 1687 benchmark networks provides challenging examples for the research community to enable validation, comparison and improvement of various algorithms.